<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route for admin with prefix "admin" declared one time in group*/
Route::group(['prefix' => 'admin',  'middleware' => 'admin'], function() {
	Route::get('/dashboard','Admin\DashboardController@index');

	Route::get('change_username', 'Admin\ChangePasswordController@change_username');
	Route::post('update_username', 'Admin\ChangePasswordController@update_username');

	Route::get('change_password', 'Admin\ChangePasswordController@index');
	Route::post('update_password', 'Admin\ChangePasswordController@store');

	Route::get('/add_frame','Admin\FrameController@add_frame');
	Route::post('/submit_frame','Admin\FrameController@submit_frame');
	Route::post('/update_frame','Admin\FrameController@update_frame');
	Route::get('/edit_frame/{id}','Admin\FrameController@edit_frame');
	Route::get('/frame_list','Admin\FrameController@frame_list');
	Route::get('/change_frame_status','Admin\FrameController@change_frame_status');
	Route::post('/delete_frame','Admin\FrameController@delete_frame');

	Route::get('/add_video','Admin\PramotionController@add_video');
	Route::post('/submit_video','Admin\PramotionController@submit_video');
	Route::post('/update_video','Admin\PramotionController@update_video');
	Route::get('/edit_video/{id}','Admin\PramotionController@edit_video');
	Route::get('/pramotion_video_list','Admin\PramotionController@pramotion_video_list');
	Route::get('/change_video_status','Admin\PramotionController@change_video_status');
	Route::post('/delete_pramote_video','Admin\PramotionController@delete_pramote_video');

	Route::get('/add','Admin\FeedController@add');
	Route::post('/submit_feed','Admin\FeedController@submit_feed');
	Route::post('/update_feed','Admin\FeedController@update_feed');
	Route::get('/edit/{id}','Admin\FeedController@edit');
	Route::get('/feed_list','Admin\FeedController@feed_list');
	Route::get('/change_feed_status','Admin\FeedController@change_feed_status');
	Route::post('/delete_feed','Admin\FeedController@delete_feed');

	Route::get('/image_list/{id}','Admin\UserController@image_list');
	Route::post('/delete_image','Admin\UserController@delete_image');

	Route::get('/video_list/{id}','Admin\UserController@video_list');
	Route::post('/delete_video','Admin\UserController@delete_video');

	Route::get('/add_user','Admin\UserController@add_user');
	Route::post('/submit_user','Admin\UserController@submit_user');
	Route::post('/update_user','Admin\UserController@update_user');
	Route::get('/edit_user/{id}','Admin\UserController@edit_user');
	Route::get('/user_list','Admin\UserController@user_list');
	Route::get('/user_profile/{id}','Admin\UserController@user_profile');
	Route::get('/change_user_status','Admin\UserController@change_user_status');
	Route::post('/delete_user','Admin\UserController@delete_user');
	Route::get('change_user_password/{id}', 'Admin\UserController@change_user_password');
	Route::post('update_user_password', 'Admin\UserController@update_user_password');

	Route::get('/logout','Admin\DashboardController@logout');
});

/* Route for guest */
Route::group(['middleware' => 'guest'], function() {
	Route::get('/admin_login','Admin\LoginController@index');

	Route::post('/submit_login','Admin\LoginController@submit_login');

	Route::get('/forgot_password','Admin\LoginController@forgot_password');
	Route::post('/forgot_password_submit','Admin\LoginController@forgot_password_submit');

	Route::get('/login_one','LoginController@login_one');
	Route::get('/login_two','LoginController@login_two');
	Route::get('/login_three','LoginController@login_three');
	Route::get('/login','LoginController@login');

	Route::post('/register_name','LoginController@register_name');
	Route::post('/register_email','LoginController@register_email');
});

Route::group(['middleware' => 'user'], function() {
	Route::get('/select_frame','HomeController@select_frame');
	Route::get('/logout','HomeController@logout');
	Route::get('/dashboard','HomeController@user_dashboard');
});	


Route::get('/','HomeController@home');
Route::get('/home','HomeController@home')->name('home');

Route::get('/privacy_policy','HomeController@home');
Route::get('/term_of_sale','HomeController@home');

Route::get('/email_verification/{id}', 'LoginController@email_verification');

Route::get('/custom_auth/{id}', 'LoginController@custom_auth');




/* Facebook Integration */
Route::get('auth/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('auth/facebook/callback', 'Auth\LoginController@handleProviderCallback');

/* Google Integration */
Route::get('/redirect', 'SocialAuthGoogleController@redirect');
Route::get('/callback', 'SocialAuthGoogleController@callback');