<!DOCTYPE html>
<!-- saved from url=(0069)http://votivelaravel.in/designer/FrameIt/HTML/order_status_email.html -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Email Template</title>

<link href="{{url('/').'/resources/views/emails/invoice/css'}}" rel="stylesheet">

<style type="text/css">
    
@media(min-width:320px) and (max-width: 767px) 
{
img.logo_im {
    width: auto;
    max-width: 100%;
}

.bill_add span {
    width: 100% !important;
}

.bill_add {
    width: 92% !important;
}
span.fir_str {
    padding-top: 7px;
    border-top: 1px solid #e0e0e0;
    margin-top: 12px;
}

span.Date_s {
    width: 100% !important;
    float: left;
    line-height: 25px;
}

span.Date_s_on {
    width: 100% !important;
    float: left;
}

body.main_b_s {
    margin: 10px auto !important;
}

.main_h_s {
    padding: 0 0px !important;
    max-width: 600px !important; 
    margin: 0 auto !important;
}

.main_h_ss {
    width: 90% !important;
}

}

</style>



</head>
<body class="main_b_s" style="font-family: &#39;open Sans&#39;;font-size: 14px; line-height:20px;">
    <div class="main_h_s" style="padding: 0 10px;max-width: 600px;margin: 0 auto;">
        

        <div class="main_h_ss" style="max-width:600px;width:100%;padding:10px;margin:0 auto 30px;border:1px solid #faaa1b;background: #faaa1b">
        	<div style="background: #fff;padding: 15px;">
                <div style="padding:17px 30px 30px;border:1px solid #fff;background:rgba(243, 243, 243, 0.72);">
                <div style="text-align:center">
                    <h2 style="color: #fff;"><img src="{{url('/').'/resources/front_assets/img/logoColor.png'}}" class="logo_im"></h2>
                </div>
                <div style="border-bottom:1px dashed #8a8a8a94;margin:15px auto 15px;padding:10px;display:block;overflow:hidden;max-width: 400px; text-align: center; color: #fff;">
                    <h4 style="margin:0 0; font-size: 28px; color: #000; font-weight: 500; margin-bottom: 15px;">Hii {{!empty($first_name) ? $first_name : ''}} {{!empty($last_name) ? $last_name : ''}}</h4>
                </div>



                <div style=" display:block; overflow:hidden">
                    <p style=" margin:0 0 15px 0px; color: #faaa1b; text-align: center; font-size: 20px; font-weight: 600;">Your order is {{!empty($status) ? $status : ''}}</p>

                <div style="text-align: center;">
                <span style="text-decoration:none; color:#000; padding-bottom:2px; display:inline-block; text-align: center; margin-right: 10px;"> 
                <strong> Product Order ID : </strong>   <b style="font-weight: 500;"> #{{!empty($order_id) ? $order_id : ''}} </b> </span>
                </div>

                <p style="text-align: center; margin-bottom: 0px;margin-top: 8px;">
                <a style="color: #faaa1b;font-size: 12px;font-weight: 500; color: #848282; text-align: center; text-decoration:none;" href="#">Get More Details</a></p>
                </div>
                <div style="     margin: 0px 0;   text-align: center;margin-bottom: 0px;margin-top: 0px;font-size: 15px;">
                <a href="#" style="text-decoration:none; color:#000; padding-bottom:2px; display:inline-block; text-align: center;">Visit site : - frameit</a>
                </div>
        </div>
            		
        	</div>
        </div>
    </div>


</body></html>