@extends('admin.layout.layout')
@section('title', 'Add Referral Commission')

@section('current_page_css')
<link rel="stylesheet" href="{{url('/')}}/resources/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endsection

@section('current_page_js')
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
   <div class="container-fluid">
    <div class="row mb-2">
     <div class="col-sm-6">
      <h1 class="m-0 text-dark">Referral Commission Settings</h1>
    </div>
    <!-- /.col -->
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
       <li class="breadcrumb-item"><a href="#">Home</a></li>
       <li class="breadcrumb-item active">Add Referral Commission</li>
     </ol>
   </div>
   <!-- /.col -->
 </div>
 <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
 <div class="container-fluid">

   @if ($message = Session::get('message'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($message = Session::get('error'))
  <div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($message = Session::get('warning'))
  <div class="alert alert-warning alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($message = Session::get('info'))
  <div class="alert alert-info alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($errors->any())
  <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 <!-- Small boxes (Stat box) -->
 <form action="{{url('/admin/submit_referral_commission')}}" id="couponForm" method="post" enctype="multipart/form-data">
  <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
  <div class="form-group">
    <label for="exampleFormControlInput1">No Of Orders</label>
    <input type="number" class="form-control" name="no_of_orders" id="exampleFormControlInput1" placeholder="No Of Orders">
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">Order Amount</label>
    <input type="number" class="form-control" name="order_amount" id="exampleFormControlInput1" placeholder="No Of Uses">
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">Referral Amount</label>
    <input type="number" class="form-control" name="referral_amount" id="exampleFormControlInput1" placeholder="Uses Limit">
  </div>

  <div class="row">
    <!-- /.col -->
    <div class="col-4">
      <button class="btn btn-primary" name="submit" type="submit">Submit</button>
    </div>
    <!-- /.col -->
  </div>
</form> 
<!-- /.row -->


</div>
<!-- /.container-fluid -->
</section>
<!-- /.content -->

<script type="text/javascript">
  $('#couponForm').validate({ 
      // initialize the plugin
    rules: {
       no_of_orders: {
        required: true
      },

      order_amount: {
        required: true
      },

      referral_amount: {
        required: true
      }
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
</script>
</div>
@endsection         