@extends('admin.layout.layout')
@section('title', 'Edit CMS')

@section('current_page_css')
<link rel="stylesheet" href="{{url('/')}}/resources/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endsection

@section('current_page_js')
<script src="{{ asset('public/ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace( 'summary-ckeditor' );
</script>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
   <div class="container-fluid">
    <div class="row mb-2">
     <div class="col-sm-6">
      <h1 class="m-0 text-dark">CMS </h1>
    </div>
    <!-- /.col -->
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
       <li class="breadcrumb-item"><a href="#">Home</a></li>
       <li class="breadcrumb-item active">Edit CMS</li>
     </ol>
   </div>
   <!-- /.col -->
 </div>
 <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
 <div class="container-fluid">

  @if ($message = Session::get('message'))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($message = Session::get('error'))
  <div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($message = Session::get('warning'))
  <div class="alert alert-warning alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($message = Session::get('info'))
  <div class="alert alert-info alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($errors->any())
  <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif

 <!-- Small boxes (Stat box) -->
 <form action="{{url('/admin/update_cms_mgmt')}}" id="frameForm" method="post" enctype="multipart/form-data">
  <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
  <input type="hidden" name="cms_id" id="cms_id" value="{{(!empty($cms_info->id) ? $cms_info->id : '')}}" />

  <div class="form-group">
    <label for="exampleFormControlInput1">Page Name</label>
    <input type="text" class="form-control" name="page_name" id="exampleFormControlInput1" value="{{(!empty($cms_info->page_name) ? $cms_info->page_name : '')}}" placeholder="Page Name">
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">Page Title</label>
    <input type="text" class="form-control" name="page_title" id="exampleFormControlInput1" value="{{(!empty($cms_info->title) ? $cms_info->title : '')}}" placeholder="Title">
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">Description</label>
    <textarea type="text" class="form-control" name="description" id="summary-ckeditor">{{(!empty($cms_info->description) ? $cms_info->description : '')}}</textarea>
  </div>

  <div class="row">
    <!-- /.col -->
    <div class="col-4">
      <button class="btn btn-primary" name="submit" type="submit">Submit</button>
    </div>
    <!-- /.col -->
  </div>
</form> 
<!-- /.row -->


</div>
<!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
@endsection         