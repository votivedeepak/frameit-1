@extends('admin.layout.layout')

@section('title', 'Product List')



@section('current_page_css')

<link rel="stylesheet" href="{{url('/')}}/resources/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

<link rel="stylesheet" href="{{url('/')}}/resources/assets/css/bootstrap-toggle.min.css">

@endsection



@section('current_page_js')

<script src="{{url('/')}}/resources/assets/plugins/datatables/jquery.dataTables.js"></script>

<script src="{{url('/')}}/resources/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script src="{{url('/')}}/resources/assets/js/bootstrap-toggle.min.js"></script>

<script type="text/javascript">

  $(function () {

    $('#frame_list').DataTable({

      "paging": true,

      "lengthChange": false,

      "searching": true,

      "ordering": true,

      "info": true,

      "autoWidth": false,

    });

  });

</script>

<script type="text/javascript">

 function delete_frame(frame_id){

  $.ajaxSetup({

    headers: {

      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

  });

  $.ajax({

   type: 'POST',

   url: "<?php echo url('/admin/delete_frame'); ?>",

   enctype: 'multipart/form-data',

   data:{frame_id:frame_id,'_token':'<?php echo csrf_token(); ?>'},

     beforeSend:function(){

       return confirm("Are you sure you want to delete this image?");

     },

     success: function(resultData) { 

       console.log(resultData);

       var obj = JSON.parse(resultData);

       if (obj.status == 'success') {

         setTimeout(function() {

            $('#success_message').fadeOut("slow");

          }, 2000 );

          $("#row" + frame_id).remove();

       }

     },

     error: function(errorData) {

      console.log(errorData);

      alert('Please refresh page and try again!');

    }

  });

}

</script>

<script>

  $('.toggle-class').on("change", function() {

    var status = $(this).prop('checked') == true ? 1 : 0; 

    var frame_id = $(this).data('id'); 

    

    $.ajax({

      type: "GET",

      dataType: "json",

      url: "<?php echo url('/admin/change_frame_status'); ?>",

      data: {'status': status, 'frame_id': frame_id},

      success: function(data){

        $('#success_message').fadeIn().html(data.success);

        setTimeout(function() {

          $('#success_message').fadeOut("slow");

        }, 2000 );

      },

      error: function(errorData) {

        console.log(errorData);

        alert('Please refresh page and try again!');

      }

    });

  })

</script>

@endsection



@section('content')

<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <!-- Content Header (Page header) -->

  <div class="content-header">

   <div class="container-fluid">

    <div class="row mb-2">

     <div class="col-sm-6">

      <h1 class="m-0 text-dark">Product</h1>

    </div>

    <!-- /.col -->

    <div class="col-sm-6">

      <ol class="breadcrumb float-sm-right">

       <li class="breadcrumb-item"><a href="#">Home</a></li>

       <li class="breadcrumb-item active">Product List</li>

     </ol>

   </div>

   <!-- /.col -->

 </div>

 <!-- /.row -->

</div>

<!-- /.container-fluid -->

</div>

<!-- /.content-header -->

<!-- Main content -->

<section class="content">

 <div class="container-fluid">

  <p style="display: none;" id="success_message" class="alert alert-success"></p>

  @if ($errors->any())

  <div class="alert alert-danger">

   <ul>

     @foreach ($errors->all() as $error)

     <li>{{ $error }}</li>

     @endforeach

   </ul>

 </div>

 @endif



 @if(Session::has('message'))

 <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>

 @endif



 @if(Session::has('error'))

 <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>

 @endif



 <a href="{{url('/admin/add_frame')}}" class="btn btn-primary">Add</a>

 <!-- Small boxes (Stat box) -->

 <table id="frame_list" class="table table-bordered table-striped">

  <thead>

    <tr>

      <th>SNo.</th>

      <th>Image</th>
      <!-- <th>Second Image</th> -->

      <th>Title</th>

      <th>Quantity</th>

      <th>Price</th>
      <th>Frame Type</th>
      <th>Frame Category</th>

      <th>Size</th>

      <th>Size Inch</th>

      <th>Face Count</th>

      <th>Status</th>

      <th>Create Date</th>

      <th>Action</th>

    </tr>

  </thead>

  <tbody>

    @if(!$frame_list->isEmpty())

    <?php $i=1; ?>

    @foreach($frame_list as $arr)

    <tr id="row{{$arr->id}}">

      <td>{{$i}}</td>

      <td>

        <img style="width: 150px;" src="{{url('/')}}/public/uploads/frame_images/{{$arr->images}}">

      </td>


      <!-- <td> -->
        @if(!empty($arr->second_image))
        <!-- <img style="width: 150px;" src="{{url('/')}}/public/uploads/frame_images/{{$arr->second_image}}"> -->
        @else


        @endif

     <!--  </td> -->

      <td>{{$arr->title}}</td>

      <td>{{$arr->quantity}}</td>

      <td>{{$arr->price}}</td>

      <td>{{$arr->type}}</td>

      <td>
        @foreach($category as $c)
          @if($arr->category_id == $c->cat_id)
            {{@$c->category_name}}
          @endif  
        @endforeach

      </td>
        
      <td>
        {{$arr->size}}
      </td>

      <td>
        {{$arr->size_inch}}
      </td>

      <td>
        {{$arr->face_count}}
      </td>

      <td>

        <input data-id="{{$arr->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $arr->status ? 'checked' : '' }}>

      </td>

      <td>{{(!empty($arr->created_at) ? date('d-m-Y H:i A',strtotime($arr->created_at)) : 'N/A')}}</td>

      <td>



        <a href="{{url('/admin/edit_frame')}}/{{base64_encode($arr->id)}}"><i class="fa fa-edit" aria-hidden="true" alt="frame" title="frame"></i></a>



        <a href="javascript:void(0)" onclick="delete_frame('<?php echo $arr->id; ?>');"><i class="fa fa-trash" aria-hidden="true" alt="frame" title="frame"></i></a>



      </td>

    </tr>

    <?php $i++; ?>

    @endforeach

    @endif



  </tbody>

</table>   

<!-- /.row -->





</div>

<!-- /.container-fluid -->

</section>

<!-- /.content -->

</div>

@endsection         