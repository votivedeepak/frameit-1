@extends('admin.layout.layout')
@section('title', 'Image Gallery')

@section('current_page_css')
   
@endsection

@section('current_page_js')
   
@endsection

@section('content')
         <!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
               <div class="container-fluid">
                  <div class="row mb-2">
                     <div class="col-sm-6">
                        <h1 class="m-0 text-dark">User Profile</h1>
                     </div>
                     <!-- /.col -->
                     <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                           <li class="breadcrumb-item"><a href="#">Home</a></li>
                           <li class="breadcrumb-item active">User Profile</li>
                        </ol>
                     </div>
                     <!-- /.col -->
                  </div>
                  <!-- /.row -->
               </div>
               <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <section class="content">
               <div class="container-fluid">


                    <div class="row">
                      <div class="col-md-3">

                        <!-- Profile Image -->
                        <div class="card card-primary card-outline">
                          <div class="card-body box-profile">
                            <div class="text-center">
                              @if(!empty($user_info->profile_image))
                                <?php $profile_url = url('/').'/public/uploads/profile_image/'.$user_info->profile_image; ?>
                              @else
                                <?php $profile_url = url('/').'/resources/assets/images/blank_user.jpg'; ?>
                              @endif
                              <img class="profile-user-img img-fluid img-circle" src="{{$profile_url}}" alt="User profile picture">
                              
                            </div>

                            <h3 class="profile-username text-center">{{(!empty($user_info->fullname) ? $user_info->fullname : 'N/A')}}</h3>

                            <p class="text-muted text-center">{{(!empty($user_info->username) ? $user_info->username : 'N/A')}}</p>

                            <ul class="list-group list-group-unbordered mb-3">
                              <li class="list-group-item">
                                <b>Followers</b> <a class="float-right">{{$followers}}</a>
                              </li>
                              <li class="list-group-item">
                                <b>Following</b> <a class="float-right">{{$following}}</a>
                              </li>
                            </ul>

                          </div>
                          <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        <!-- /.card -->
                      </div>
                      <!-- /.col -->
                      <div class="col-md-9">
                        <div class="card">
                            
                            <!-- About Me Box -->
                            <div class="card card-primary">
                              <div class="card-header">
                                <h3 class="card-title">About Me</h3>
                              </div>
                              <!-- /.card-header -->
                              <div class="card-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        <strong><i class="fas fa-book mr-1"></i> Email</strong>

                                        <p class="text-muted">
                                          {{(!empty($user_info->email) ? $user_info->email : 'N/A')}}
                                        </p>

                                        <hr>

                                        <strong><i class="fas fa-map-marker-alt mr-1"></i> Contact Number</strong>

                                        <p class="text-muted">{{(!empty($user_info->user_mob) ? $user_info->user_mob : 'N/A')}}</p>

                                        <hr>

                                        <strong><i class="fas fa-pencil-alt mr-1"></i> Gender</strong>

                                        <p class="text-muted">
                                          {{(!empty($user_info->user_gender) ? $user_info->user_gender : 'N/A')}}
                                        </p>

                                        <hr>

                                        <strong><i class="far fa-file-alt mr-1"></i> City</strong>

                                        <p class="text-muted">{{(!empty($user_info->user_city) ? $user_info->user_city : 'N/A')}}</p>

                                        <hr>

                                        <strong><i class="far fa-file-alt mr-1"></i> Work At</strong>

                                        <p class="text-muted">{{(!empty($user_info->works_at) ? $user_info->works_at : 'N/A')}}</p>

                                        <hr>

                                        
                                    </div>

                                    <div class="col-md-6">
                                        <strong><i class="far fa-file-alt mr-1"></i> Relation Status</strong>

                                        <p class="text-muted">{{(!empty($user_info->relation_status) ? $user_info->relation_status : 'N/A')}}</p>

                                        <hr>

                                        <strong><i class="far fa-file-alt mr-1"></i> Zip Code</strong>

                                        <p class="text-muted">{{(!empty($user_info->user_zipcode) ? $user_info->user_zipcode : 'N/A')}}</p>

                                        <hr>

                                        <strong><i class="far fa-file-alt mr-1"></i> Wallet Amount</strong>

                                        <p class="text-muted">{{(!empty($user_info->wallet_amount) ? $user_info->wallet_amount : 'N/A')}}</p>

                                        <hr>

                                        <strong><i class="far fa-file-alt mr-1"></i> DOB</strong>

                                        <p class="text-muted">{{(!empty($user_info->dob) ? date('d-m-Y',strtotime($user_info->dob)) : 'N/A')}}</p>

                                        <hr>

                                        <strong><i class="far fa-file-alt mr-1"></i> Study At</strong>

                                        <p class="text-muted">{{(!empty($user_info->study_at) ? $user_info->study_at : 'N/A')}}</p>

                                        <hr>
                                    </div>
                                </div>
                                

                                <strong><i class="far fa-file-alt mr-1"></i> Address</strong>

                                <p class="text-muted">{{(!empty($user_info->user_address) ? $user_info->user_address : 'N/A')}}</p>

                                <hr>

                                

                                

                                <strong><i class="far fa-file-alt mr-1"></i> Bio</strong>

                                <p class="text-muted">{{(!empty($user_info->bio) ? $user_info->bio : 'N/A')}}</p>

                                <hr>

                                
                              </div>
                              <!-- /.card-body -->
                            </div>

                        </div>
                        <!-- /.nav-tabs-custom -->
                      </div>
                      <!-- /.col -->
                    </div>

                  
               </div>
               <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
         </div>
@endsection