@extends('admin.layout.layout')
@section('title', 'User List')

@section('current_page_css')
<link rel="stylesheet" href="{{url('/')}}/resources/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="{{url('/')}}/resources/assets/css/bootstrap-toggle.min.css">
@endsection

@section('current_page_js')
<script src="{{url('/')}}/resources/assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{url('/')}}/resources/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="{{url('/')}}/resources/assets/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
  $(function () {
    $('#user_list').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
<script type="text/javascript">
 function delete_user(user_id){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
     type: 'POST',
     url: "<?php echo url('/admin/delete_user'); ?>",
     enctype: 'multipart/form-data',
     data:{user_id:user_id,'_token':'<?php echo csrf_token(); ?>'},
     beforeSend:function(){
       return confirm("Are you sure you want to delete this user?");
     },
     success: function(resultData) { 
       console.log(resultData);
       var obj = JSON.parse(resultData);
       if (obj.status == 'success') {
         setTimeout(function() {
          $('#success_message').fadeOut("slow");
        }, 2000 );
        $("#row" + user_id).remove();
       } 
     },
     error: function(errorData) {
      console.log(errorData);
      alert('Please refresh page and try again!');
    }
  });
}
</script>
<script>
  $('.toggle-class').on('change', function() {
    var status = $(this).prop('checked') == true ? 1 : 0; 
    var user_id = $(this).data('id');
    $.ajax({
      type: "GET",
      dataType: "json",
      url: "<?php echo url('/admin/change_user_status'); ?>",
      data: {'status': status, 'user_id': user_id},
      success: function(data){
        $('#success_message').fadeIn().html(data.success);
        setTimeout(function() {
          $('#success_message').fadeOut("slow");
        }, 2000 );
      },
      error: function(errorData) {
        console.log(errorData);
        alert('Please refresh page and try again!');
      }
    });
  })
</script>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
   <div class="container-fluid">
    <p style="display: none;" id="success_message" class="alert alert-success"></p>
     @if ($errors->any())
     <div class="alert alert-danger">
       <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
       </ul>
     </div>
     @endif

     @if(Session::has('message'))
     <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
     @endif

     @if(Session::has('error'))
     <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
     @endif
     
     <div class="row mb-2">
       <div class="col-sm-6">
        <h1 class="m-0 text-dark">User List</h1>
      </div>
      <!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
         <li class="breadcrumb-item"><a href="#">Home</a></li>
         <li class="breadcrumb-item active">User List</li>
       </ol>
     </div>
     <!-- /.col -->
   </div>
   <!-- /.row -->
 </div>
 <!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
 <div class="container-fluid">

  <a href="{{url('/admin/add_user')}}" class="btn btn-primary">Add</a>
  <!-- Small boxes (Stat box) -->
  <table id="user_list" class="table table-bordered table-striped">
    <thead>
      <tr>
        <th>SNo.</th>
        <th>Name</th>
        <th>Email</th>
        <th>Contact Number</th>
        <!--<th>Wallet Balance</th>-->
        <th>Status</th>
        <!--<th>Create Date</th>-->
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
     @if(!$user_list->isEmpty())
     <?php $i=1; ?>
     @foreach($user_list as $arr)
     <tr id="row{{$arr->id}}">
      <td>{{$i}}</td>
      <td>{{$arr->first_name}} {{$arr->last_name}}</td>
      <td>{{$arr->email}}</td>
      <td>{{$arr->contact_number}}</td>
      <!--<td>{{$arr->wallet_balance}}</td>-->
      <td>
        <input data-id="{{$arr->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $arr->status ? 'checked' : '' }}>
      </td>
      <!--<td>{{(!empty($arr->created_at) ? date('d-m-Y H:i   A',strtotime($arr->created_at)) : 'N/A')}}</td>-->
      <td>
        <a href="{{url('/admin/change_user_password')}}/{{base64_encode($arr->id)}}">Update Password</a>

        <a href="{{url('/admin/edit_user')}}/{{base64_encode($arr->id)}}"><i class="fa fa-edit" aria-hidden="true" alt="user" title="user"></i></a>

        <a href="javascript:void(0)" onclick="delete_user('<?php echo $arr->id; ?>');"><i class="fa fa-trash" aria-hidden="true" alt="user" title="user"></i></a>

        <!-- <a href="{{url('/admin/user_profile')}}/{{base64_encode($arr->id)}}"><i class="fa fa-user" aria-hidden="true" alt="profile" title="profile"></i></a>

       <a href="{{url('/admin/video_list')}}/{{base64_encode($arr->id)}}"><i class="fa fa-file-video" aria-hidden="true" alt="video gallery" title="video gallery"></i></a>

       <a href="{{url('/admin/image_list')}}/{{base64_encode($arr->id)}}"><i class="fa fa-image" aria-hidden="true" alt="image gallery" title="image gallery"></i></a>

       <a href=""><i class="fas fa-user-friends" aria-hidden="true" alt="Followers" title="Followers"></i></a>

       <a href=""><i class="fas fa-users-cog" aria-hidden="true" alt="Following" title="Following"></i></a>

       <a href=""><i class="fas fa-user-slash" aria-hidden="true" alt="Blocked Followers" title="Blocked Followers"></i></a>

       <a href="" ><i class="fa fa-rss" aria-hidden="true" alt="Feeds" title="Feeds"></i></a>-->
     </td>
   </tr>
   <?php $i++; ?>
   @endforeach
   @endif

 </tbody>
</table>  
<!-- /.row -->


</div>
<!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
@endsection         