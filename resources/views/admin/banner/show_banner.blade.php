@extends('admin.layout.layout')
@section('title', 'Banner List')

@section('current_page_css')
<link rel="stylesheet" href="{{url('/')}}/resources/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="{{url('/')}}/resources/assets/css/bootstrap-toggle.min.css">
@endsection

@section('current_page_js')
<script src="{{url('/')}}/resources/assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{url('/')}}/resources/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="{{url('/')}}/resources/assets/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
  $(function () {
    $('#user_list').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
<script type="text/javascript">
 function delete_banner(banner_id){
  
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $.ajax({
   type: 'POST',
   url: "<?php echo url('/admin/deleteBanner'); ?>",
   enctype: 'multipart/form-data',
   data:{banner_id:banner_id,'_token':'<?php echo csrf_token(); ?>'},
     beforeSend:function(){
       return confirm("Are you sure you want to delete this banner details?");
     },
     success: function(resultData) { 
       console.log(resultData);
       
       var obj = JSON.parse(resultData);
       if (obj.status == 'success') {
         setTimeout(function() {
            $('#success_message').fadeOut("slow");
          }, 2000 );
          $("#row" + banner_id).remove();
       }
     },
     error: function(errorData) {
      console.log(errorData);
      alert('Please refresh page and try again!');
    }
  });
}
</script>
<script>
  $('.toggle-class').on('change', function() {
    var status = $(this).prop('checked') == true ? 1 : 0; 
    var banner_id = $(this).data('id');
    $.ajax({
      type: "GET",
      dataType: "json",
      url: "<?php echo url('/admin/change_banner_status'); ?>",
      data: {'status': status, 'banner_id': banner_id},
      success: function(data){
        $('#success_message').fadeIn().html(data.success);
        setTimeout(function() {
          $('#success_message').fadeOut("slow");
        }, 2000 );
      },
      error: function(errorData) {
        console.log(errorData);
        alert('Please refresh page and try again!');
      }
    });
  })
</script>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
   <div class="container-fluid">
    <p style="display: none;" id="success_message" class="alert alert-success"></p>
     @if ($errors->any())
     <div class="alert alert-danger">
       <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
       </ul>
     </div>
     @endif

     @if(Session::has('message'))
     <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
     @endif

     @if(Session::has('error'))
     <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
     @endif
     
     <div class="row mb-2">
       <div class="col-sm-6">
        <h1 class="m-0 text-dark">Banner List</h1>
      </div>
      <!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
         <li class="breadcrumb-item"><a href="#">Home</a></li>
         <li class="breadcrumb-item active">Banner List</li>
       </ol>
     </div>
     <!-- /.col -->
   </div>
   <!-- /.row -->
 </div>
 <!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
 <div class="container-fluid">

  <a href="{{url('/admin/addBanner')}}" class="btn btn-primary">Add</a>
  <!-- Small boxes (Stat box) -->
  <table id="user_list" class="table table-bordered table-striped">
    <thead>
      <tr>
        <th>SNo</th>
        <th>Banner Image</th>
        <th>Mobile Banner Image</th>
        <th>Banner Title</th>
        <th>Banner Description</th>
        <th>Status</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
     
      <?php $i=1; ?>
      @foreach($banner_details as $banner)
        <tr id="row{{$banner->banner_id}}">
          <td>{{$i}}</td>
          <td><img style="width: 150px;" src="{{url('/')}}/public/uploads/banner_images/{{$banner->banner_image}}"></td>
          <td><img style="width: 150px;" src="{{url('/')}}/public/uploads/banner_images/{{$banner->mobile_banner_image}}"></td>
          <td>{{ $banner->banner_title }}</td>
          <td>{{ $banner->banner_description }}</td>
          <td>
            <input data-id="{{$banner->banner_id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $banner->status ? 'checked' : '' }}>
          </td>
          <td>

            <a href="{{url('/admin/editBanner')}}/{{$banner->banner_id}}"><i class="fa fa-edit" aria-hidden="true" alt="feed" title="feed"></i></a>

            <a href="javascript:void(0)" onclick="delete_banner('<?php echo $banner->banner_id; ?>');"><i class="fa fa-trash" aria-hidden="true" alt="feed" title="feed"></i></a>

          </td>
        </tr>
        <?php $i++; ?>
      @endforeach

    </tbody>
</table>  
<!-- /.row -->


</div>
<!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
@endsection         