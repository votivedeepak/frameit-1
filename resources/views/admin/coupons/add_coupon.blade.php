@extends('admin.layout.layout')
@section('title', 'Add Coupon')

@section('current_page_css')
<link rel="stylesheet" href="{{url('/')}}/resources/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="{{url('/')}}/resources/assets/css/bootstrap-datetimepicker.min.css">
@endsection

@section('current_page_js')
<script src="{{url('/')}}/resources/assets/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
   $(function () {
  var bindDatePicker = function () {
    $(".date")
      .datetimepicker({
        format: "YYYY-MM-DD",
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-arrow-up",
          down: "fa fa-arrow-down"
        }
      })
      .find("input:first")
      .on("blur", function () {
        // check if the date is correct. We can accept dd-mm-yyyy and yyyy-mm-dd.
        // update the format if it's yyyy-mm-dd
        var date = parseDate($(this).val());

        if (!isValidDate(date)) {
          //create date based on momentjs (we have that)
          date = moment().format("YYYY-MM-DD");
        }

        $(this).val(date);
      });
  };

  var isValidDate = function (value, format) {
    format = format || false;
    // lets parse the date to the best of our knowledge
    if (format) {
      value = parseDate(value);
    }

    var timestamp = Date.parse(value);

    return isNaN(timestamp) == false;
  };

  var parseDate = function (value) {
    var m = value.match(/^(\d{1,2})(\/|-)?(\d{1,2})(\/|-)?(\d{4})$/);
    if (m)
      value =
        m[5] + "-" + ("00" + m[3]).slice(-2) + "-" + ("00" + m[1]).slice(-2);

    return value;
  };

  bindDatePicker();
});

</script>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
   <div class="container-fluid">
    <div class="row mb-2">
     <div class="col-sm-6">
      <h1 class="m-0 text-dark">Coupon</h1>
    </div>
    <!-- /.col -->
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
       <li class="breadcrumb-item"><a href="#">Home</a></li>
       <li class="breadcrumb-item active">Add Coupon</li>
     </ol>
   </div>
   <!-- /.col -->
 </div>
 <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
 <div class="container-fluid">

   @if ($message = Session::get('message'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($message = Session::get('error'))
  <div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($message = Session::get('warning'))
  <div class="alert alert-warning alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($message = Session::get('info'))
  <div class="alert alert-info alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($errors->any())
  <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 <!-- Small boxes (Stat box) -->
 <form action="{{url('/admin/submit_coupon')}}" id="couponForm" method="post" enctype="multipart/form-data">
  <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
  <div class="form-group">
    <label for="exampleFormControlInput1">Name</label>
    <input type="text" class="form-control" name="name" id="exampleFormControlInput1" placeholder="Name">
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">Coupon Code</label>
    <input type="text" class="form-control" name="code" id="exampleFormControlInput1" placeholder="Code">
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">Type</label>
    <select class="form-control" name="type">
        <option value="percentage">Percentage(%)</option>
        <option value="fixed_amount">Fixed Amount</option>
    </select>
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">Value</label>
    <input type="number" class="form-control" name="value" id="exampleFormControlInput1" placeholder="Value">
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">Reusable</label>
    <select class="form-control" name="no_of_uses">
        <option value="Yes">Yes</option>
        <option value="No">No</option>
    </select>
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">No Of Uses</label>
    <input type="number" class="form-control" name="no_of_uses" id="exampleFormControlInput1" placeholder="No Of Uses">
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">Uses Limit</label>
    <input type="number" class="form-control" name="uses_limit" id="exampleFormControlInput1" placeholder="Uses Limit">
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">For First Signup User Only</label>
    <select class="form-control" name="first_signup_only">
        <option value="Yes">Yes</option>
        <option value="No">No</option>
    </select>
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">Start Date</label>
    <input type="date" class="form-control" name="start_date" id="exampleFormControlInput1" placeholder="Start Date">
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">End Date</label>
    <input type="date" class="form-control" name="end_date" id="exampleFormControlInput1" placeholder="End Date">
  </div>

  <div class="row">
    <!-- /.col -->
    <div class="col-4">
      <button class="btn btn-primary" name="submit" type="submit">Submit</button>
    </div>
    <!-- /.col -->
  </div>
</form> 
<!-- /.row -->


</div>
<!-- /.container-fluid -->
</section>
<!-- /.content -->

<script type="text/javascript">
  $('#couponForm').validate({ 
      // initialize the plugin
    rules: {
       name: {
        required: true
      },

      start_date: {
        required: true
      },

      end_date: {
        required: true
      },

      no_of_uses: {
        required: true
      },

      uses_limit: {
        required: true
      },

      code: {
        required: true
      },

      type: {
        required: true
      },

      value: {
        required: true
      }
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
</script>
</div>
@endsection         