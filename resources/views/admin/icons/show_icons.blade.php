@extends('admin.layout.layout')
@section('title', 'Icon List')

@section('current_page_css')
<link rel="stylesheet" href="{{url('/')}}/resources/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="{{url('/')}}/resources/assets/css/bootstrap-toggle.min.css">
@endsection

@section('current_page_js')
<script src="{{url('/')}}/resources/assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{url('/')}}/resources/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="{{url('/')}}/resources/assets/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
  $(function () {
    $('#user_list').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
<script type="text/javascript">
 function delete_icons(icon_id){
  
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $.ajax({
   type: 'POST',
   url: "<?php echo url('/admin/deleteIcon'); ?>",
   enctype: 'multipart/form-data',
   data:{icon_id:icon_id,'_token':'<?php echo csrf_token(); ?>'},
     beforeSend:function(){
       return confirm("Are you sure you want to delete this Icon details?");
     },
     success: function(resultData) { 
       console.log(resultData);
       var obj = JSON.parse(resultData);
       if (obj.status == 'success') {
         setTimeout(function() {
            $('#success_message').fadeOut("slow");
          }, 2000 );
          $("#row" + icon_id).remove();
       }
     },
     error: function(errorData) {
      console.log(errorData);
      alert('Please refresh page and try again!');
    }
  });
}
</script>
<script>
  $('.toggle-class').on('change', function() {
    var status = $(this).prop('checked') == true ? 1 : 0; 
    var icon_id = $(this).data('id');
    $.ajax({
      type: "GET",
      dataType: "json",
      url: "<?php echo url('/admin/change_icon_status'); ?>",
      data: {'status': status, 'icon_id': icon_id},
      success: function(data){
        $('#success_message').fadeIn().html(data.success);
        setTimeout(function() {
          $('#success_message').fadeOut("slow");
        }, 2000 );
      },
      error: function(errorData) {
        console.log(errorData);
        alert('Please refresh page and try again!');
      }
    });
  })
</script>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
   <div class="container-fluid">
    <p style="display: none;" id="success_message" class="alert alert-success"></p>
     @if ($errors->any())
     <div class="alert alert-danger">
       <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
       </ul>
     </div>
     @endif

     @if(Session::has('message'))
     <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
     @endif

     @if(Session::has('error'))
     <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
     @endif
     
     <div class="row mb-2">
       <div class="col-sm-6">
        <h1 class="m-0 text-dark">Icon List</h1>
      </div>
      <!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
         <li class="breadcrumb-item"><a href="#">Home</a></li>
         <li class="breadcrumb-item active">Icon List</li>
       </ol>
     </div>
     <!-- /.col -->
   </div>
   <!-- /.row -->
 </div>
 <!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
 <div class="container-fluid">

  <a href="{{url('/admin/addIcons')}}" class="btn btn-primary">Add</a>
  <!-- Small boxes (Stat box) -->
  <table id="user_list" class="table table-bordered table-striped">
    <thead>
      <tr>
        <th>SNo</th>
        <th>Image</th>
        <th>Title</th>
        <th>Description</th>
        <th>Status</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
     
      <?php $i=1; ?>
      @foreach($icon_details as $icons)
        <tr id="row{{$icons->icon_id}}">
          <td>{{$i}}</td>
          <td><img style="width: 150px;" src="{{url('/')}}/public/uploads/icons/{{$icons->icon_image}}"></td>
          <td>{{ $icons->icon_title }}</td>
          <td>{{ $icons->icon_description }}</td>
          <td>
            <input data-id="{{$icons->icon_id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $icons->status ? 'checked' : '' }}>
          </td>
          <td>

            <a href="{{url('/admin/editIcons')}}/{{$icons->icon_id}}"><i class="fa fa-edit" aria-hidden="true" alt="feed" title="feed"></i></a>

            <a href="javascript:void(0)" onclick="delete_icons('<?php echo $icons->icon_id; ?>');"><i class="fa fa-trash" aria-hidden="true" alt="feed" title="feed"></i></a>

          </td>
        </tr>
        <?php $i++; ?>
      @endforeach

    </tbody>
</table>  
<!-- /.row -->


</div>
<!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
@endsection         