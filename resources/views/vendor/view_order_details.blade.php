@extends('vendor.layout.layout')

@section('title', 'View Order Details')



@section('current_page_css')

<link rel="stylesheet" href="{{url('/')}}/resources/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

<link rel="stylesheet" href="{{url('/')}}/resources/assets/css/bootstrap-toggle.min.css">

<style type="text/css">

#canvas{

  color: #0cff15; 

  background-image: url("http://localhost/frameit/public/uploads/frame_images/1584604169_white.png");

  -webkit-background-image: url("http://localhost/frameit/public/uploads/frame_images/1584604169_white.png");

  border:1px solid black;

  width:300px;

  height:300px;

}

</style>

@endsection



@section('current_page_js')

<script src="{{url('/')}}/resources/assets/plugins/datatables/jquery.dataTables.js"></script>

<script src="{{url('/')}}/resources/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script src="{{url('/')}}/resources/assets/js/bootstrap-toggle.min.js"></script>

<script type="text/javascript">

  $(function () {

    $('#').DataTable({

      "paging": true,

      "lengthChange": false,

      "searching": true,

      "ordering": true,

      "info": true,

      "autoWidth": false,

    });

  });

</script>


@endsection



@section('content')

<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <!-- Content Header (Page header) -->

  <div class="content-header">

   <div class="container-fluid">

    <div class="row mb-2">

     <div class="col-sm-6">

      <h1 class="m-0 text-dark">Order Details</h1>

    </div>

    <!-- /.col -->

    <div class="col-sm-6">

      <ol class="breadcrumb float-sm-right">

       <li class="breadcrumb-item"><a href="#">Home</a></li>

       <li class="breadcrumb-item active">Order Details</li>

     </ol>

   </div>

   <!-- /.col -->

 </div>

 <!-- /.row -->

</div>

<!-- /.container-fluid -->

</div>

<!-- /.content-header -->





<!-- Order Detail section  -->



<section class="order_Detail_s">

  <div class="container-fluid">

    <div class="row">



      <div class="col-md-6 col-sm-12"> 

        <div class="order_Detail_main_s">

          <div class="portlet yellow-crusta box">

            <div class="portlet-title">

              <div class="caption">

                <i class="fa fa-cogs"></i>Order Details 

              </div>

              <div class="actions">

               <!-- <a href="javascript:;" class="btn btn-default btn-sm">Edit </a>-->

              </div>

            </div>

            <div class="portlet-body">

              <div class="row static-info">

                <div class="col-md-5 name"> Order #: </div>

                <div class="col-md-7 value"> #{{!empty($order_info->id) ? $order_info->id : ""}}

                  <span class="label label-info label-sm"> Email confirmation was sent </span>

                </div>

              </div>

            <div class="row static-info">

              <div class="col-md-5 name"> Order Date &amp; Time: </div>

              <div class="col-md-7 value"> {{(!empty($order_info->created_at) ? date('d  F  Y H:i A',strtotime($order_info->created_at)) : 'N/A')}} </div>

            </div>

            <div class="row static-info">

              <div class="col-md-5 name"> Order Status: </div>

              <div class="col-md-7 value">

                <span class="label label-success"> {{!empty($order_info->order_status) ? $order_info->order_status : ""}} </span>

              </div>

            </div>

              <div class="row static-info">

                <div class="col-md-5 name"> Grand Total: </div>

                <div class="col-md-7 value"> {{!empty($order_info->total_price) ? $order_info->total_price : ""}} {{!empty($order_info->shipping_currency) ? $order_info->shipping_currency : ""}}</div>

              </div>

              <div class="row static-info">

                <div class="col-md-5 name"> Payment Information: </div>

                <div class="col-md-7 value"> Credit Card </div>

              </div>

            </div>

          </div>

        </div>

      </div>



      <div class="col-md-6 col-sm-12"> 

        <div class="portlet blue-hoki box">

          <div class="portlet-title">

            <div class="caption">

               <i class="fa fa-cogs"></i>Customer Information 

            </div>

            <div class="actions">

              <!--<a href="javascript:;" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i> Edit </a>-->

            </div>

          </div>

          <div class="portlet-body">

            <div class="row static-info">

              <div class="col-md-5 name"> Customer Name: </div>

              <div class="col-md-7 value"> {{!empty($order_info->first_name) ? $order_info->first_name : ""}} {{!empty($order_info->last_name) ? $order_info->last_name : ""}}</div>

            </div>

            <div class="row static-info">

              <div class="col-md-5 name"> Email: </div>

              <div class="col-md-7 value"> {{!empty($order_info->email) ? $order_info->email : ""}} </div>

            </div>

            <div class="row static-info">

              <div class="col-md-5 name"> State: </div>

              <div class="col-md-7 value"> {{!empty($order_info->address) ? $order_info->address : "N/A"}} </div>

            </div>

            <div class="row static-info">

              <div class="col-md-5 name"> Phone Number: </div>

              <div class="col-md-7 value"> {{!empty($order_info->contact) ? $order_info->contact : ""}} </div>

            </div>

          </div>

        </div>

      </div>  



      <div class="billing_add_sec">

        <div class="row">

          <div class="col-md-6 col-sm-12"> 

            <div class="portlet green-meadow box">

              <div class="portlet-title">

                <div class="caption">

                  <i class="fa fa-cogs"></i>Billing Address 

                </div>

                  <div class="actions">

                    <!--<a href="javascript:;" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i> Edit </a>-->

                  </div>

              </div>

              <div class="portlet-body">

                <div class="row static-info">

                  <div class="col-md-12 value"> {{!empty($order_info->first_name) ? $order_info->first_name : ""}}  {{!empty($order_info->last_name) ? $order_info->last_name : ""}}

                    <br> {{!empty($order_info->email) ? $order_info->email : ""}}

                    <br>Address : {{!empty($order_info->address) ? $order_info->address : "N/A"}} 

                    <br> T: {{!empty($order_info->contact) ? $order_info->contact : ""}}

                    

                  </div>

                </div>

              </div>

            </div>

          </div>



          <div class="col-md-6 col-sm-12">

            <div class="portlet red-sunglo box">

              <div class="portlet-title">

                <div class="caption">

                  <i class="fa fa-cogs"></i>Shipping Address 

                </div>

                <div class="actions">

                    <!--<a href="javascript:;" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i> Edit </a>-->

                </div>

              </div>

              <div class="portlet-body">

                  <div class="row static-info">

                    <div class="col-md-12 value"> {{!empty($order_info->full_name) ? $order_info->full_name : ""}}

                    <br> {{!empty($order_info->street_address) ? $order_info->street_address : ""}}

                    <br> {{!empty($order_info->city) ? $order_info->city : ""}}

                    <br> {{!empty($order_info->zip_code) ? $order_info->zip_code : ""}}, {{!empty($order_info->state) ? $order_info->state : ""}}

                    <br> {{!empty($order_info->country) ? $order_info->country : ""}}

                    <br> T: {{!empty($order_info->contact_number) ? $order_info->contact_number : ""}}

                    <br> 

                  </div>

                  </div>

              </div>

            </div>

          </div>

        </div>

      </div>



    </div>

  </div>

</section>  







<!-- Main content -->

<section class="content">

 <div class="container-fluid">



  @if ($errors->any())

  <div class="alert alert-danger">

   <ul>

     @foreach ($errors->all() as $error)

     <li>{{ $error }}</li>

     @endforeach

   </ul>

 </div>

 @endif



 @if(Session::has('message'))

 <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>

 @endif



 @if(Session::has('error'))

 <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>

 @endif



 <!-- Small boxes (Stat box) -->

 <table id="" class="table table-bordered table-striped">

  <thead>

    <tr>

      <th>SNo.</th>

      <th>Frame Image</th>

      <th>Tile Image</th>

      <th>Quntity</th>

      <th>Price</th>

      <th>Sub-Total</th>

    </tr>

  </thead>

  <tbody>

    @if(!$order_details->isEmpty())

    <?php $i=1; $sum=0; $grand_total=0;?>

    @foreach($order_details as $arr)

    <?php 

      $sub_total = $arr->price * $arr->quantity;

      $sum += $sub_total;

      $grand_total = $sum + $arr->shipping_price - $arr->coupon_discount - $arr->wallet_discount ; 

     ?>

    <tr>

      <td>{{$i}}</td>

      <td>
        <?php $rotate = $arr->rotation; ?>
      
         <img style="width:100px;transform: rotate(<?php echo $rotate.'deg';?>);" id="btn<?php echo $i; ?>" src="{{url('/')}}/public/uploads/frame_images/{{$arr->images}}">

      </td>

      <td>

          <img style="width:100px;" src="{{url('/')}}/public/uploads/order_images/{{$arr->tile_image}}">

      </td>

      <td>{{$arr->quantity}}</td>

      <td>{{$arr->price}}  {{$arr->currency}}</td>

      <td>{{$sub_total}}  {{$arr->currency}}</td>

    </tr>

    <?php $i++; ?>

    @endforeach

    <tr>

        <td></td><td></td><td></td><td></td><td><b>Total Price</b></td><td><b>{{$sum}}  {{$arr->currency}}</b></td> 

    </tr>

    <tr>

        <td></td><td></td><td></td><td></td><td><b>Shipping Price</b></td><td><b>{{$arr->shipping_price}}  {{$arr->currency}}</b></td> 

    </tr>

    <tr>

        <td></td><td></td><td></td><td></td><td><b>Coupon Amount</b></td><td><b>{{$arr->coupon_discount}}  {{$arr->currency}}</b></td> 

    </tr>

    <tr>

        <td></td><td></td><td></td><td></td><td><b>Wallet Amount</b></td><td><b>{{$arr->wallet_discount}}  {{$arr->currency}}</b></td> 

    </tr>

    <tr>

        <td></td><td></td><td></td><td></td><td><b>Grand Total</b></td><td><b>{{$grand_total}}  {{$arr->currency}}</b></td> 

    </tr>

    @endif



    

  </tbody>

</table>   

<!-- /.row -->





</div>

<!-- /.container-fluid -->

</section>

<!-- /.content -->

</div>

@endsection         