@extends('user.layout.layout')
@section('title', 'User - Profile')

@section('current_page_css')
@endsection

@section('current_page_js')
<script type="text/javascript">
	$.ajaxSetup({
		headers: {'votive':'123456'}
	});
	$(document).ready(function(){
		var formData = new FormData();
		$.ajax({
			type: 'POST',
			url: "<?php echo url('/').'/api/getAllFeedList'; ?>",
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(resultData){ 
				var yourArray = [];
				$.each(resultData.response.feedList, function( i, l ){
					//console.log(resultData.response.feedList[i].image_path+'/'+resultData.response.feedList[i].image);
					var feed ='<div class="clip"><div class="testimonial-item"><div class="social-user"><img class="social-icon" src="{{url('/').'/resources/front_assets'}}/img/instagram-logo.png"> <span>'+resultData.response.feedList[i].title+'</span></div><div class="socialBigImg"><img class="image" src="'+resultData.response.feedList[i].image_path+'/'+resultData.response.feedList[i].image+'"></div><div class="info"><div class="text">'+resultData.response.feedList[i].description+'</div></div></div></div>';
					yourArray.push(feed);
				});
				$("#FeedGallery").html(yourArray);
			}
		});
	});
</script>
@endsection

@section('tag_body')
<body>
	@endsection

	@section('content')                            
	<!--==========================
	    Hero Section
	    ============================-->
	    <section id="" class="section home_page_sec">
	    	<div class="sectionWraper-top">
	    		<div class="hero-container">
	    			<div class="slider-box">
	    			<div class="col-md-5">
	    			<div class="hero-text" data-wow-delay="100ms">
	    				<div class="logoMain wow fadeInLeft"></div>
	    					<h3>{{ $banner_details[0]->banner_title }}</h3>
	    					<p>{{ $banner_details[0]->banner_description }}</p>
	    					<a href="{{ $banner_details[0]->button_url }}" class="get-strt"> {{ $banner_details[0]->button_name }} <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
	    	
	    				<!-- <div class="bannerText wow fadeInLeft"><span>Turn your walls into storytelling memory lane.</span></div> -->
	    				<!-- <div class="photoImg wow fadeInLeft"><img src="{{url('/').'/resources/front_assets'}}/img/photo.png"><span>8"X10"</span></div> -->
	    			</div></div>
	    			<div class="col-md-7">
	    			<div class="hero-banner " >
	    				<img src="{{url('/')}}/public/uploads/banner_images/{{$banner_details[0]->banner_image}}" class="d-no">
	    				<img src="{{url('/')}}/public/uploads/banner_images/{{$banner_details[0]->mobile_banner_image}}" class="d-show">
	    			</div>
	    		</div>
	    	</div>
	    		</div>
	    	</div>
	    </section><!-- #hero -->

	  <!--==========================
	    Get Started Section
	    ============================-->

	    <section id="Explore_sec" class="section">
	    	<div class="sectionWraper mt-op">
	    		<div class="row">
            @foreach($icon_details as $icons)
	    			<div class="col-6">
	    				<div class="bnrBotTex wow fadeInUp" data-wow-delay="100ms">
					<div class="iconBig">
						<!-- <img class="" src="{{url('/').'/resources/front_assets'}}/img/iconBig.jpg"> -->
						<img class="" src="{{url('/')}}/public/uploads/icons/{{$icons->icon_image}}">
					</div>
					<div class="iconText"><h2>{{ $icons->icon_title }}</h2><p>{{ $icons->icon_description }} </p></div>
				</div>
	    			</div>
            @endforeach
	    			<!-- <div class="col-6">
	    				<div class="bnrBotTex wow fadeInUp" data-wow-delay="200ms">
	    					<div class="iconBig"><img class="" src="{{url('/').'/resources/front_assets'}}/img/satisfication.png"></div>
	    					<div class="iconText"><h2>Satisfaction guaranteed</h2><p>Not satisfied? Get a full refund </p></div>
	    				</div>
	    			</div> -->

					<!-- <div class="col-md-6">
	    				<div class="bnrBotTex wow fadeInUp" data-wow-delay="100ms">
	    					<div class="iconBig">
	    						<img class="" src="{{url('/').'/resources/front_assets'}}/img/img_icon_three.jpg">
	    					</div>
	    					<div class="iconText"><h2>High Quality Print</h2><p>Bright & Vivid High Definition Print </p></div>
	    				</div>
	    			</div>
	    			<div class="col-md-6">
	    				<div class="bnrBotTex wow fadeInUp" data-wow-delay="200ms">
	    					<div class="iconBig"><img class="" src="{{url('/').'/resources/front_assets'}}/img/img_icon_four.jpg"></div>
	    					<div class="iconText"><h2>Perfect Size</h2><p>As a gift of precious memory or to display at home or office! </p></div>
	    				</div>
	    			</div> -->

          
      
	    	</div>


	    </section>

	  <!--==========================
	    Screenshots Section
	    ============================-->

<section id="" class="section lastSection">
       <div class="sectionWraper instaSection">
       	 <h3 class="how-work">{{ $video_details[0]->title }} </h3>
        <div class="container">
        	<div class="add_views_sec" style="background-image: url({{url('/')}}/public/uploads/pramotion_videos/{{ $video_details[0]->video_image }});">
        		<div class="rames-text">
                <h3>{{ $video_details[0]->sub_title }}</h3>
        		<h5>{{ $video_details[0]->description }}</h5>
        		<div class="play-btn-box">
        		<a class="watch-video" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-play" aria-hidden="true"></i>{{ $video_details[0]->button_name }}</a>


                    <div class="thumbnail">
                        <div class="help_video">
                            <video id="1" controlsList="nodownload">
                                <source src="~/Content/helpvideos/2%20admission.mp4" type="video/mp4" style="width:100%;">
                            </video>
                    </div>
                </div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <video controls="controls" id="video" controlsList="nodownload" preload="none">
                    <source src="{{url('/')}}/public/uploads/pramotion_videos/{{ $video_details[0]->video }}" type="video/mp4" />
                </video>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="StopVideo()" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
	
	 $(document).ready(function () {
            $('video').on('click', function () {
                var video = $(this).attr('src');
                $('#myModal').on('show.bs.modal', function () {
                    $(".img-responsive").attr("src", video);
                });
            });
        });
        function StopVideo() {
            var vid3 = document.getElementById("video");
            vid3.pause();
        }
</script>
        	</div>
        	</div>

        	</div>

          <div class="section-title text-center wow fadeInUp" data-wow-delay="100ms">
            <h2>{{ $feed_details[0]->feed_heading_name }}</h2>
            <h4>{{ $feed_details[0]->feed_description }}</h4>
          </div>
        </div>

       <div class="owl-carousel owl-theme">
         @foreach($feed_image_details as $arr)
          <div class="item first prev">
              <div class="card border-0 py-3 px-4">
                  <div class="row justify-content-center"> <img src="{{url('/')}}/public/uploads/feed_images/{{$arr->picture}}" class="img-fluid profile-pic mb-4 mt-3"> </div>
                  <h6 class="mb-3 mt-2">{{$arr->title}}</h6>
                  <p class="content mb-5 mx-2">{{$arr->description}}</p>
              </div>
          </div>
        @endforeach
        <!-- <div class="item show">
            <div class="card border-0 py-3 px-4">
                <div class="row justify-content-center"> <img src="https://i.imgur.com/oW8Wpwi.jpg" class="img-fluid profile-pic mb-4 mt-3"> </div>
                <h6 class="mb-3 mt-2">Ximena Vegara</h6>
                <p class="content mb-5 mx-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim.</p>
            </div>
        </div>
        <div class="item next">
            <div class="card border-0 py-3 px-4">
                <div class="row justify-content-center"> <img src="https://i.imgur.com/ndQx2Rg.jpg" class="img-fluid profile-pic mb-4 mt-3"> </div>
                <h6 class="mb-3 mt-2">John Paul</h6>
                <p class="content mb-5 mx-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim.</p>
            </div>
        </div>
        <div class="item last">
            <div class="card border-0 py-3 px-4">
                <div class="row justify-content-center"> <img src="https://i.imgur.com/T5aOhwh.jpg" class="img-fluid profile-pic mb-4 mt-3"> </div>
                <h6 class="mb-3 mt-2">William Doe</h6>
                <p class="content mb-5 mx-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim.</p>
            </div>
        </div> -->
    </div>
        <div id="FeedGallery11" class="slick-slider slider center wow fadeInUp" data-wow-delay="200ms">
          @foreach($feed_image_details as $arr)
          <div class="clip">

            <div class="testimonial-item">
              <!-- <div class="social-user"><img class="social-icon" src="{{url('/').'/resources/front_assets'}}/img/instagram-logo.png"> <span>@HeyItsJenna</span></div> -->
              <div class="socialBigImg"><img class="image" src="{{url('/')}}/public/uploads/feed_images/{{$arr->picture}}"></div>
              <div class="info">                           
                <div class="text">{{$arr->description}}</div>
              </div>
            </div>
          </div>
          @endforeach


        </div>


        <div class="main_bottom_sec">
        	<div class="container">
        		<div class="row">
        			<div class="col-md-3">
        				<div class="btm-log">
        			<img src="http://votivelaravel.in/newframeit/resources/front_assets/img/logopixtwo.png" style="width: 150px;">
        			</div>
        			<p class="get-tuch"> GET THE FREE APP</p>

                     <div class="play-store">
                     	<a href="#">
                     <img src="http://votivelaravel.in/newframeit/resources/front_assets/img/google-play.png" style="width: 100px;">
                 </a>
                 <a href="#">
                     <img src="http://votivelaravel.in/newframeit/resources/front_assets/img/and.png" style="width: 100px;">
                 </a>
                     </div>


        	<!-- <ul>
        		<li><a href="{{url('/')}}/terms_and_condition"> Terms of use </a></li>
        		<li><a href="{{url('/')}}/privacy_policy"> Privacy policy </a></li>
        		<li><a href="mailto:contact@frameit.com.my" target="_blank"> Contact </a></li>
        	</ul> -->
		
		</div>

		<div class="col-md-6 mr-0a">
			<div class="resorne">
       <h3>RESOURCES </h3>
       <ul>
       	<li><a href="{{ url('profile') }}">Manage Account <span>|</span></a>  </li>
       	<li><a href="{{ url('aboutus') }}">About <span>|</span></a> </li>
       	<li><a href="mailto:contact@frameit.com.my" target="_blank">Contact <span>|</span></a> </li>
        <li><a href="{{ url('termsofservice') }}">Terms of Service <span>|</span></a> </li>
         <li><a href="javascript:void(0);" id="faq" data-toggle="modal" data-target="#exampleModalCenterFaq">FAQ / Help <span>|</span></a> </li>

        </ul>

   </div>
		</div>
		<div class="col-md-3">
<div class="resorne-r">
 <h3><img src="http://votivelaravel.in/newframeit/resources/front_assets/img/chat-ioxn.png"> Need some help? </h3>
 <div class="talke-to">
 <p> Talk to someone from our real 
support team, live 24/7 </p>

<a href="#" class="chat-us"> Chat with Us</a>
</div>
		</div>
</div>

<div class="col-md-6 copy-right">
<p> © 2021 Pix2arts, All Rights Reserved.</p>
</div>
<div class="col-md-6">
	<div class="icon-social">
<a href="#"> <img src="http://votivelaravel.in/newframeit/resources/front_assets/img/facebook.png"> </a>
<a href="#"> <img src="http://votivelaravel.in/newframeit/resources/front_assets/img/instagram.png"> </a>
<a href="#"> <img src="http://votivelaravel.in/newframeit/resources/front_assets/img/twitter.png"> </a>

</div>
	</div>


</div>
      </div>
    </section>
		@endsection

		@section('page_footer')
		<footer class="footerSectopm wow fadeInUp">
			<div class="footerBtn">
				<!-- <button>Start Here <i class="fa fa-chevron-right"></i></button> -->
				@if(empty(Auth::user()->id))
			      <!-- <a data-toggle="modal" href="javascript:void(0);" data-target="#loginModal">Start Here  <span class="rig_arr"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a> -->
			      <a href="{{url('/category')}}">Get Started<span class="rig_arr"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
			    @else
			      <a href="{{url('/category')}}">Get Started<span class="rig_arr"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
			    @endif
			</div>
		</footer>
		@endsection