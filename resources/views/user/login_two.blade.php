@extends('user.layout.layout')
@section('title', 'User - Profile')

@section('current_page_css')
@endsection

@section('current_page_js')
@endsection

@section('tag_body')
  <body class="step_main">
@endsection

@section('content')
	<section id="" class="section selectFramStyles">
    <div class="container">
      <div class="selectFrameSec">
        <h2>2/3</h2>        
      </div>
      <div class="stp_one">
        <div class="content">
          <div class="bg-layer">
            <div class="bg-circle"></div>
          </div>

          <div id="displayResBox"></div>
          <form class="log_form" id="second_step_form" method="post" action="#">
            @csrf
            <div class="form-label">Let's get to know you</div>
            <div class="in_firs">
              <input class="FormInput" type="text" name="firstName" placeholder="What's your name?" value="<?php echo (!empty(session('customer_name')) ? session('customer_name') : ''); ?>">
            </div>
            <div class="button_cont">            
              <input class="btn_Submit" type="submit" value="Continue">
            </div>
          </form>
        </div>
      </div>
      <div class="framesTabing">
        <div class="sectionWraper"></div>
      </div>
    </div>
  </section> 
@endsection

@section('page_footer')
	
@endsection