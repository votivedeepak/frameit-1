@extends('user.layout.layout')
@section('title', 'payment')

@section('current_page_css')
@endsection

@section('current_page_js')

<?php $user_id=Auth::user()->id; ?>



@endsection

@section('tag_body')
  <body >
@endsection

@section('content')
   <section id="" class="section bx_mainCrop">
    

<div class="selectFrameSec frm_sssss">

  
  <?php
$amount = 15;
$orderRef=date('YmdHis');
?>


<form name="payForm" id="payForm" method="post" action="https://test.paydollar.com/b2cDemo/eng/payment/payForm.jsp">


<input type="hidden" name="amount" value="<?php echo $amount;?>">
<input type="hidden" name="merchantId" value="85005573">
<input type="hidden" name="orderRef" value="<?php echo $orderRef;?>">
<input type="hidden" name="currCode" value="458" >
<input type="hidden" name="successUrl" value="http://votivelaravel.in/frameit/payment-successful">
<input type="hidden" name="failUrl" value="http://votivelaravel.in/frameit/payment-fail">
<input type="hidden" name="cancelUrl" value="http://votivelaravel.in/frameit/payment-cancel">
<input type="hidden" name="remark" value="test">
<input type="hidden" name="lang" value="E">
<input type="hidden" name="payMethod" value="ALL">
<input type="hidden" name="submit" value="Pay Now">


</form>

<div class="pay_sec_new">
<div class="container">

<div class="inner_secPro_nn">
  <div class="row  wow slideInUp" data-wow-delay=".1s">

  	<div class="pay_sec_one">
	
	<div class="head_pay_p">
	<h3>ipay88</h3>
	<a href="#">ipay88</a>
	</div>
 

<div class="head_pay_txt">
<h3>Make a payment using Credit/Debit card.</h3>
<h4>Payment Type*</h4>

<div class="row">

<div class="col-md-4">
<div class="head_pay_txton">
	
<h6>Credit/Debit Card</h6>

<span class="radio_buiii">
<label class="container12"><img  id="usrImg" width="110px" src="{{url('/')}}/resources/front_assets/img/visa.png">
<input type="radio" name="radio">
<span class="checkmark12"></span>
</label>
</span>



</div>	
</div>

<div class="col-md-4">
<div class="head_pay_txton">
	
<h6>Online Bank Transfe</h6>

<span class="radio_buiii">
<label class="container12"><img  id="usrImg" width="110px" src="{{url('/')}}/resources/front_assets/img/AmBank.png">
<input type="radio" name="radio">
  <span class="checkmark12"></span>
</label>
</span>

<span class="radio_buiii">
<label class="container12"><img  id="usrImg" width="110px" src="{{url('/')}}/resources/front_assets/img/RHB-Bank.png">
<input type="radio" name="radio">
  <span class="checkmark12"></span>
</label>
</span>

<span class="radio_buiii">
<label class="container12"><img  id="usrImg" width="110px" src="{{url('/')}}/resources/front_assets/img/cimbbank.png">
<input type="radio" name="radio">
  <span class="checkmark12"></span>
</label>
</span>


<span class="radio_buiii">
<label class="container12"><img  id="usrImg" width="110px" src="{{url('/')}}/resources/front_assets/img/AmBank.png">
<input type="radio" name="radio">
  <span class="checkmark12"></span>
</label>
</span>



</div>	</div>



<div class="col-md-4">
	
<div class="head_pay_txton">
	
<h6>Other Options</h6>

<span class="radio_buiii">
<label class="container12"><img  id="usrImg" width="110px" src="{{url('/')}}/resources/front_assets/img/paypal.png">
<input type="radio" name="radio">
  <span class="checkmark12"></span>
</label>
</span>
</div>	</div>


<div class="bottom_pay_s">
	<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal123">Place Order</button>
<!-- <a href="#" data-toggle="modal" data-target="#myModal">Place Order</a> -->
</div>

</div>
</div>



</div>


  </div>
  </div>
</div>
</div>




</div>
</section>



<!-- Modal -->
<div id="myModal123" class="modal fade" role="dialog">
  <div class="modal-dialog trans_suoo">

    <!-- Modal content-->
    <div class="modal-content trans_suo">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Transaction successful</h4>
      </div>
      <div class="modal-body trans_su">

      	<img  id="" class="her_ico" width="110px" src="{{url('/')}}/resources/front_assets/img/heart-icon.png">

        <h3>Thank you for your order</h3>
		<strong class="ord_id">Your Order ID:xxxxxx</strong>

		<span class="bot_butt_s"><a href="{{url('/')}}">Back to Homepage</a>
		<a href="{{url('/')}}/edit-profile">Go to Your Orders</a>

		<p>Shared this product and earn credits</p>
		</span>
      </div>
 
    </div>

  </div>
</div>


@endsection

@section('page_footer')
  
@endsection