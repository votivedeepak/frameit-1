@extends('user.layout.layout')
@section('title', 'User - Profile')
@section('current_page_css') 

<style type="text/css">
  .selectFrameSec {
    float: left;
    width: 100%;
    background-color: #f7f7f7 !important;
}

.frmStyl.width img {
    width: 83%;
    max-height: 93px;
}

i.fa.fa-money-bill {
    color: #faaa1b;
    font-size: 20px;
}

  .btn_bot input {

    background-color: #faaa1b;

    border: none;

    border-radius: 5px;

    padding: 7px;

    width: 74%;

    font-weight: 700;

    color: #fff;

    font-size: 28px;

    letter-spacing: 1px;

    margin-top: 12px;

    cursor: pointer;

    position: relative;

  }
  img.cr-image {
    width:100%;
}
.croppie-container {
    /*width: 100%;*/
    height:75% !important;
    /*text-align: center;*/
}
input.jcrop-keymgr {
    display: none !important;
}

.show_modal {
    /*background-size: 215px 270px !important;*/
}

/*.bx_mig a{
       background-size: 100% 100%!important;
        background-position-x: -6px !important;
        background-position-y: 10px !important;
    }*/

span.fsp-summary__action.fsp-summary__action--button {

display: none !important;
  
}
.jcrop-holder {
    background-color: rgb(0 0 0 / 0%) !important;
}

 .socialimg{ 

    color: rgba(255,255,255,.9);
    background-color: #2e68fb;
    font-family: Roboto-regular,Roboto,sans-serif;
    font-size: 13px;
    font-weight: 400;
    height: 38px;
    line-height: 38px;
    padding: 10px 30px;
    border-radius: 4px;
    border-width: 1px;
    border-style: solid;
    border-color: transparent;
    cursor: pointer;   

} 

.fst-sidebar {
    display: none !important;
}

/*a.img_bbb.show_modal {
    overflow: hidden;
    max-height: 290px;
}*/
/*img.set_frame {
    min-height: 290px;
    max-height: 290px;
}*/

.modal .cropboxwidth{ width:auto }

/* modal popup start */

</style>

@endsection


@section('current_page_js')

<?php 

  $qty = 0;  $frame_price = 0; $currency = "";

  if(!empty($tmpImgData)){ 

    foreach ($tmpImgData as  $value) { 

    $qty +=$value->quantity;

    $frame_price = $value->price * $qty;

    $currency = $value->currency;

    }

  }

  $user_id=@Auth::user()->id;

  $wallet_amt=@Auth::user()->wallet_balance; 

  $fprice = $frame_price;

  $sprice = @Auth::user()->shipping_price;

  $scurrency = @Auth::user()->shipping_currency;

?>


<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f322945f84fb08f"></script>


<script type="text/javascript">

   // start add shipping address
    $('#createShipAddress1').validate({ 
      rules: {
        fullname: {
          required: true,
        },
        country:{
          required: true,
        },
        streetAddress: {
          required: true
        },
        city:{
          required: true,
        },
        state:{
          required: true,
        },
        address:{
          required: true,
        },
        contact_number: {
          required: true,
          digits:true,
          minlength : 8,
          maxlength : 13
        },
      },
      submitHandler: function(form) {
         //form.submit();
         addShipAddress1();
       }
     });

    $.ajaxSetup({
      headers: {'votive':'123456'}
    });

    function addShipAddress1() {
      var $this = $('form#createShipAddress1')[0];
      var formData = new FormData($this);

      $.ajax({

          type: 'POST',
          url: "<?php echo url('/').'/createShipAddress'; ?>",
          data: formData,
          enctype: 'multipart/form-data',
          cache:false,
          contentType: false,
          processData: false,
          success: function(resultData) { 
            if (resultData.status) {

              //var result_alert = 'Success!'+;
              $("#signupResBox1").html('<p class="msg1" style="text-align:center;color:green;font-weight:bold;font-size:24px">'+resultData.msg+'</p>');
              // $("#shipping_price").html('<span class="tot_l">Shipping Price</span><span class="tot_r">'+resultData.response.ship_price+' '+resultData.response.ship_currency+'</span>');
              
              var delay = 3000; 
              setTimeout(function(){
                $("#exampleModalCenterOne").modal("hide");
                $("p#msg1").hide();
                var user_id = "<?php echo $user_id ?>";
                var formData = new FormData();

                formData.append('user_id',user_id);

                $.ajax({

                  type: 'POST',

                  url: "<?php echo url('/').'/api/getUserAddressList';?>",

                  data: formData,

                  enctype: 'multipart/form-data',

                  cache:false,

                  contentType: false,

                  processData: false,

                  success: function(resultData){ 

                    //console.log(resultData.response);

                    var yourArray = [];

                    $.each(resultData.response.addressList, function(key, value) { 

                     var addressBook = '<div class="main_pop_s_m_one"><label class="container_on">'+resultData.response.addressList[key].address_type+'<input type="radio" checked="checked" name="address_id" value="'+resultData.response.addressList[key].id+'"><span class="checkmark"></span></label><div class="main_pop_s_m_one_bt"><span><strong>'+resultData.response.addressList[key].fullname+'</strong><a href="javascript:void(0);" onClick="show_editaddress('+resultData.response.addressList[key].id+')" class="editbtn">Edit</a></span><p>'+resultData.response.addressList[key].street_address+','+resultData.response.addressList[key].city+','+resultData.response.addressList[key].zip_code+'</p><p>'+resultData.response.addressList[key].contact_number+'</p></div></div>';

                      yourArray.push(addressBook); 

                    });



                    $(".addressBookList").html(yourArray);



                  }

                }); 
               }, delay);

               

            } else {

              var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
              $("#signupResBox").html(result_alert);

            }

          },
          error: function(errorData) { 
              var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
              $("#signupResBox").html(result_alert);
          }
      });
  }
  // end shipping address 




</script>

<script type="text/javascript">

	 function continue1(cat_id) {  

		var frame_id = $('#frame_id').val();

		if(frame_id==undefined || frame_id==""){

			var frame_id = '3';

		}else{

			var frame_id = frame_id;

		}


	 	var facecount  = $('#face_count').val();

		 $.ajax({
	      type: 'POST',
	      url: "<?php echo url('/').'/getAllSizeFrameListbycategory'; ?>",
	      dataType: "json", 
	      contentType: "application/json; charset=utf-8", 
	      data: JSON.stringify({ cat_id: cat_id,face_count: facecount}),
	      success: function(resultData){ 
	        var yourArray = [];

          //console.log(resultData.response.frameList[0].quantity);

	        $.each(resultData.response.frameList, function( i, l ){ 
	          if(resultData.response.frameList[i].id == frame_id && resultData.response.frameList[i].quantity > 0){
	            var active_class = 'active show';
	            $("#frame_id").val(frame_id);
	          }else{
	            var active_class = '';
	          }

	          if(resultData.response.frameList[i].quantity<=0){
	          		if(resultData.response.frameList[i].type=='square'){
	            		var frame = '<li><a data-toggle="tab" href="#home" class="'+active_class+'"><span class="out_stock">Out of Stock</span><div class="frmStyl width"><div id="msg" style="padding-top:10px;color:red"></div><div class="allframe"><img src="https://votivelaravel.in/newframeit/public/uploads/square.png"></div><span>'+resultData.response.frameList[i].size+'</span></div></a></li>';
	        		}else{
	        			var frame = '<li><a data-toggle="tab" href="#home" class="'+active_class+'"><span class="out_stock">Out of Stock</span><div class="frmStyl width"><div id="msg" style="padding-top:10px;color:red"></div><div class="allframe"><img src="https://votivelaravel.in/newframeit/public/uploads/racangle.png"></div><span>'+resultData.response.frameList[i].size+'</span></div></a></li>';
	        		}
	          }else{
	          	if(resultData.response.frameList[i].type=='square'){
	            		var frame = '<li class="sizeval" data-id="'+resultData.response.frameList[i].size+'" data-catid="'+cat_id+'"><span class="out_stock"></span><a data-toggle="tab" href="#home" class="'+active_class+'"><div class="frmStyl width"><div id="msg" style="padding-top:10px"></div><div class="allframe"><img src="https://votivelaravel.in/newframeit/public/uploads/square.png"></div><span>'+resultData.response.frameList[i].size+'</span></div></a></li>';
	            	}else{
	            		var frame = '<li class="sizeval" data-id="'+resultData.response.frameList[i].size+'" data-catid="'+cat_id+'"><span class="out_stock"></span><a data-toggle="tab" href="#home" class="'+active_class+'"><div class="frmStyl width"><div id="msg" style="padding-top:10px"></div><div class="allframe"><img src="https://votivelaravel.in/newframeit/public/uploads/racangle.png"></div><span>'+resultData.response.frameList[i].size+'</span></div></a></li>';
	            	}
	          }
	          
	          yourArray.push(frame);

	        });

	        $(".frameListsize").html(yourArray);
          // $("#continue1").hide();
          // $("#continue2").show();
	       
	      }
	    });
	 }

	$("#face_count").keyup(function(){

		var val= $("#face_count").val();
		$("#face_count").val(val);

    $("#number_people").val(val);

	});
</script>

<script type="text/javascript">

$( document ).ready(function() { 

  var cat_id = $('#cat_id').val();

       $.ajax({
        type: 'POST',
        url: "<?php echo url('/').'/api/getFrameListData'; ?>",
        dataType: "json", 
        contentType: "application/json; charset=utf-8", 
        data: JSON.stringify({ cat_id: cat_id}),
        success: function(resultData){ 

          alert(resultData); 

        }

      });

  });

   $(document).on('click', ".sizeval", function() { 
      var id = $(this).data("id");
      var cat_id = $(this).data("catid");


    var frame_id = $('#frame_id').val();
    if(frame_id==undefined || frame_id==" "){
      var frame_id = '3';
    }else{
      var frame_id = frame_id;
    }
    var facecount  = $('#face_count').val();
     $.ajax({
        type: 'POST',
        url: "<?php echo url('/').'/api/getAllFrameList'; ?>",
        dataType: "json", 
        contentType: "application/json; charset=utf-8", 
        data: JSON.stringify({ cat_id: cat_id,size: id,face_count: facecount}),
        success: function(resultData){ 
          //console.log("resultData",resultData);
          var yourArray = [];
          $.each(resultData.response.frameList, function( i, l ){
            if(resultData.response.frameList[i].id == frame_id && resultData.response.frameList[i].quantity > 0){
              var active_class = 'active show';
              $("#frame_id").val(frame_id);
            }else{
              var active_class = '';
            }
            if(resultData.response.frameList[i].quantity<=0){
                if(resultData.response.frameList[i].type=='square'){
                  var frame = '<li><a data-toggle="tab" href="#home" class="'+active_class+'"><span class="out_stock">Out of Stock</span><div class="frmStyl width"><div id="msg" style="padding-top:10px;color:red"></div><div class="allframe"><img src="https://votivelaravel.in/newframeit/public/uploads/square.png"></div><span>'+resultData.response.frameList[i].size+'</span></div></a></li>';
              }else{
                var frame = '<li><a data-toggle="tab" href="#home" class="'+active_class+'"><span class="out_stock">Out of Stock</span><div class="frmStyl width"><div id="msg" style="padding-top:10px;color:red"></div><div class="allframe"><img src="https://votivelaravel.in/newframeit/public/uploads/racangle.png"></div><span>'+resultData.response.frameList[i].size+'</span></div></a></li>';
              }
            }else{
              if(resultData.response.frameList[i].type=='square'){
                  var frame = '<li onclick="getFrameById('+resultData.response.frameList[i].id+')" data-id="'+resultData.response.frameList[i].id+'"><span class="out_stock"></span><a data-toggle="tab" href="#home" class="'+active_class+'"><div class="frmStyl width"><div id="msg" style="padding-top:10px"></div><div class="allframe"><img src="'+resultData.response.frameList[i].image_path+'/'+resultData.response.frameList[i].images+'"></div><span>'+resultData.response.frameList[i].title+'</span></div></a></li>';
                }else{
                  var frame = '<li onclick="getFrameById('+resultData.response.frameList[i].id+')" data-id="'+resultData.response.frameList[i].id+'"><span class="out_stock"></span><a data-toggle="tab" href="#home" class="'+active_class+'"><div class="frmStyl width"><div id="msg" style="padding-top:10px"></div><div class="allframe"><img src="'+resultData.response.frameList[i].image_path+'/'+resultData.response.frameList[i].images+'"></div><span>'+resultData.response.frameList[i].title+'</span></div></a></li>';
                }
            }
            
            yourArray.push(frame);

          });
          $(".frameListsize").html(yourArray);
          //$("#continue1").hide();
          //$("#continue2").show();
         
        }
      });
   });

  $("#face_count").keyup(function(){

    var val= $("#face_count").val();
    $("#face_count").val(val);

  });
</script>

<script type="text/javascript">

  //start get ship address list

    function applyrewardorcoupon() {

    //alert($("input[name='rcoupon']:checked").val());
    var user_id = $('#userid').val();
    var total_amount=$('#totalamount').val();
    var coupon_code = $('#code').val();
    var reward_point=$('#ramount').val();
    var type = $("input[name='rcoupon']:checked").val();
    var  ship ="<?php echo $sprice; ?>";
    
    $.ajax({

      type: "POST",

      dataType: "json",

      url: "<?php echo url('/api/getUserPromoRewardCalculation'); ?>",

      data: {'user_id':user_id,'total_amount':total_amount,'coupon_code':coupon_code,'reward_point':reward_point,'type':type},

      success: function(data){ 

        if(data.status==true){ 

        	//var totalamount = parseFloat(data.response.total_amt) - parseFloat(data.response.discount_value);
        	var ttl = (parseFloat(data.response.discount_value) + parseFloat(ship)).toFixed(2);

          $('#mymsgreward').html('<p style="color:green">Apply coupon successfully !</p>'); 
          
           $("#coupon_discount").html('<span class="tot_l">Coupon Discount</span><span class="tot_r">'+data.response.discount_value+' USD</span>');

           $('#wallet_amt').val(reward_point);
           //$('#fprice').val(data.response.discount_value);

           if(coupon_code!==" "){
           	$('#coupon_code').val(coupon_code);
           }
           

           $("#total_price").html('<span class="tot_l">Total</span><span class="tot_r">'+ttl+' USD</span>');

           // $("#coupon_discount").html('<span class="tot_l">Coupon Discount</span><span class="tot_r">'+resultData.response.coupon_value+' '+coupon_type+'</span>');

            var delay = 5000;
          setTimeout(function(){ $('#rewardpopid').model('hide'); }, delay);

        }else{
          $('#mymsgreward').html('<p style="color:red">'+data.msg+'</p>'); 
          // alert();

        }

      }

    });

  }
</script>

<script type="text/javascript">

  //start get ship address list

    function save_frame_mo() {

    var user_id = "<?php echo $user_id ?>";
    var frame_id=$('.frame_id').val();
    var address_id = $('.address_id').val();
    var wallet_amount = $('.wallet_amt').val();
    var coupon_discount = $('.coupon_code').val();
    var total_tile = $('.tttile').val();
    var total_price = $('.ttprice').val();
    //alert(user_id +' '+ frame_id+' '+address_id +" "+wallet_amt+' '+coupon_code+' '+tttile+' '+ttprice);

    $.ajax({

      type: "GET",

      dataType: "json",

      url: "<?php echo url('/save_frame_mo'); ?>",

      data: {'user_id':user_id,'frame_id':frame_id,'address_id':address_id,'address_id':address_id,'wallet_amount':wallet_amount,'coupon_discount':coupon_discount,'total_tile':total_tile,'total_price':total_price},

      success: function(data){ 
        //alert(data.status);
        if(data.status=='success'){ 

        $('#myframesave').modal('show'); 
          
          // location.reload();
          // setTimeout(function(){ window.location.href = "<?php //echo url('/select_frame'); ?>"; }, delay);
          //window.location.href = ""; 
        //window.location.href = "<?php //echo url('/select_frame'); ?>"; 

        }else{

           alert(data.msg);

        }

      }

    });

  }




  $.ajaxSetup({

    headers: {'votive':'123456'}

  });



  var user_id = "<?php echo $user_id ?>";

  

  $(document).ready(function(){

    var formData = new FormData();

    formData.append('user_id',user_id);

    $.ajax({

        type: 'POST',

        url: "<?php echo url('/').'/api/getUserAddressList'; ?>",

        data: formData,

        cache:false,

        contentType: false,

        processData: false,

        success: function(resultData){ 

          //console.log(resultData);

          if(resultData.status){

            $("#contact_number").val(resultData.response.addressList.contact_number);

            //$("#address_id").val(resultData.response.addressList.id);

            $("#fullname").val(resultData.response.addressList.fullname);

            $("#street_address").val(resultData.response.addressList.street_address);

            $("#zip_code").val(resultData.response.addressList.zip_code);

            $("#city").val(resultData.response.addressList.city);

            $("#state").val(resultData.response.addressList.state);

            $("#countrylist option[value="+resultData.response.addressList.country_id+"]").attr('selected', 'selected');

           // $("#shipping_price").html('<span class="tot_l">Shipping Price</span><span class="tot_r">'+resultData.response.addressList.shipping_price+' '+resultData.response.addressList.shipping_currency+'</span>');


          }



        },error: function(errorData) { 



            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';

            $("#err_msg").html(result_alert);



          }

        });

    });

  //end get ship address list



  //start save frame id

  function submit_frame_id(frame_id) {
    //alert(frame_id);
    var user_id = "<?php echo $user_id ?>";

    $.ajax({

      type: "GET",

      dataType: "json",

      url: "<?php echo url('/save_frame'); ?>",

      data: {'user_id':user_id,'frame_id':frame_id},

      success: function(data){
        
      // alert(data.status);
        if(data.status=='success'){
          //location.reload();
        

        }else{

            //$('#msg').html('<span style="color:red">'+data.msg+'</span>');
            //var delay = 5000; 
             //location.reload();
          //setTimeout(function(){  window.location.reload(); }, delay);

        }
      

      }

    });

  }

  //end save frame id

   function save_rotation() {

    var user_id = "<?php echo $user_id ?>";
    var rotate_deg = $('.rotation_deg').val();
    var rotate_id = $('.rotation_id').val();
    //alert(user_id +" "+ rotate_id +" "+ rotate_deg);
    $.ajax({

      type: "GET",

      dataType: "json",

      url: "<?php echo url('/save_rotation'); ?>",

      data: {'user_id':user_id,'rotate_deg':rotate_deg,'rotate_id':rotate_id},

      success: function(data){ 

        if(data.status=='success'){ 

        //$('#myframesave').modal('show');

           var delay = 4000; 

          setTimeout(function(){  window.location.reload(); }, delay);

        //window.location.href = "<?php //echo url('/select_frame'); ?>"; 

        }else{

           alert(data.msg);

        }

      }

    });

  }



 // start add promo code

  $('#addPromoCode').validate({ 

    rules: {

      user_id: {

        required: true,

      },

      coupon_code:{

        required: true,

      },

    },

    submitHandler: function(form) {

       //form.submit();

       addCouponCode();

     }

   });



  $.ajaxSetup({

    headers: {'votive':'123456'}

  });



  var frame_price = "<?php echo $fprice ?>";

  var ship_price = "<?php echo $sprice ?>";

  var ship_currency = "<?php echo $scurrency ?>";

  function addCouponCode() {

    var $this = $('form#addPromoCode')[0];

    var formData = new FormData($this);



    $.ajax({



        type: 'POST',

        url: "<?php echo url('/').'/api/userApplyCouponCode'; ?>",

        data: formData,

        enctype: 'multipart/form-data',

        cache:false,

        contentType: false,

        processData: false,

        success: function(resultData) { 

            //console.log(resultData);

          if (resultData.status) {

            if(resultData.response.coupon_type == 'percentage'){

              var coupon_value = resultData.response.coupon_value;

              var dec = (coupon_value / 100).toFixed(2); //its convert 10 into 0.10

              var mult = frame_price * dec; // gives the value for subtract from main value

              var discont = frame_price - mult;

              var total_price = parseFloat(discont) + parseFloat(ship_price); 

              var coupon_type = "%";

            }else{

              var coupon_type = "USD";

            }



            var result_alert = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> '+resultData.msg+'</div>';

            $("#applycouponBox").html(result_alert);

            $("#coupon_discount").html('<span class="tot_l">Coupon Discount</span><span class="tot_r">'+resultData.response.coupon_value+' '+coupon_type+'</span>');

            $("#coupon_code").val(resultData.response.coupon_code);

            $("#total_price").html('<span class="tot_l"><strong>Total</strong></span><span class="tot_r"><strong>'+total_price+' '+ship_currency+'</strong></span>');

            

            //var delay = 1500; 

            //setTimeout(function(){ window.location.href = "<?php echo url('/select_frame'); ?>"; }, delay);

            //$.cookie('coupon', resultData.response.coupon_value+' '+coupon_type);

            //alert($.cookie('coupon'));

          } else {  

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';

            $("#applycouponBox").html(result_alert);

          }



        },

        error: function(errorData) { 

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';

            $("#applycouponBox").html(result_alert);

        }

    });

  }

  //end promo code





  // start apply wallet amount

  $.ajaxSetup({

    headers: {'votive':'123456'}

  });



  var user_id = "<?php echo $user_id ?>";



  $(document).ready(function(){

    var formData = new FormData();

    formData.append('user_id',user_id);

    $.ajax({

      type: 'POST',

      url: "<?php echo url('/').'/api/getUserWalletAmount'; ?>",

      data: formData,

      enctype: 'multipart/form-data',

      cache:false,

      contentType: false,

      processData: false,

      success: function(resultData) { 

          //console.log(resultData);

          if (resultData.response.wallet_balance != 0) {

            var wallet_amount = resultData.response.wallet_balance;

            $('input[name="wallet_amt"]').click(function () {

              if ($(this).is(':checked')) {

                  var result_alert = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> Wallet balance apply successfully</div>';

                  $("#wallet_amount").html(result_alert);

                  $("#wallet_amt").val(wallet_amount);

              }



              if(!$(this).is(":checked")){ 

                  var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Remove!</strong>  Not apply wallet balance</div>';

                  $("#wallet_amount").html(result_alert);

                  $("#wallet_amt").val('');

              }

            });

          }else{

              $('input[name="wallet_amt"]').click(function () {

                if ($(this).is(':checked')) {

                    var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Info!</strong>  Your wallet amount is insufficient</div>';

                  $("#wallet_amount").html(result_alert);

                  $("#wallet_amt").val('');

                }

              });

              

          }



      },

      error: function(errorData) { 

          var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';

          $("#wallet_amount").html(result_alert);

      }

    });

  });

  //end apply wallet balance



  // start get all user address list

  $.ajaxSetup({

    headers: {'votive':'123456'}

  });

  var user_id = "<?php echo $user_id ?>";

  $(document).ready(function(){

    var formData = new FormData();

    formData.append('user_id',user_id);

    $.ajax({

      type: 'POST',

      url: "<?php echo url('/').'/api/getUserAddressList';?>",

      data: formData,

      enctype: 'multipart/form-data',

      cache:false,

      contentType: false,

      processData: false,

      success: function(resultData){ 

        var yourArray = [];

        $.each(resultData.response.addressList, function(key, value) { 

         var addressBook = '<div class="main_pop_s_m_one"><label class="container_on">'+resultData.response.addressList[key].address_type+'<input type="radio" checked="checked" name="address_id" value="'+resultData.response.addressList[key].id+'"><span class="checkmark"></span></label><div class="main_pop_s_m_one_bt"><span><strong>'+resultData.response.addressList[key].fullname+'</strong><a href="javascript:void(0);" onClick="show_editaddress('+resultData.response.addressList[key].id+')" class="editbtn">Edit</a></span><p>'+resultData.response.addressList[key].street_address+','+resultData.response.addressList[key].city+','+resultData.response.addressList[key].zip_code+'</p><p>'+resultData.response.addressList[key].contact_number+'</p></div></div>';

          yourArray.push(addressBook); 

        });



        $(".addressBookList").html(yourArray);



      }

    });

  });

  // end get all user address list

  function show_editaddress(id){
  
    $('#showAddressBooknew').modal('hide');
    $('#exampleModalCenterOne').modal('hide');
    $('#editaddress'+id).modal('show');

   // console.log(id);
  }

$(document).ready(function(){   
     $('#addressmodel').click(function() {      
      $('#exampleModalCenterOne').modal('hide');
     });
  });

$(document).ready(function(){   
     $('#addnaddress').click(function() {      
      $('#showAddressBooknew').modal('hide');
     });
  });

$(document).ready(function(){ 
  $(".checkmark").click(function(){ 
    var code = $(this).data("attr");
    //alert(code);
    $("#code").val(code);
  });
});



  $(document).ready(function(){   
     $('#umreward').click(function() { 

     $('#saddbookid').hide(); 
      $("#rewardpopid").css({"position": "fixed","z-index": "9999", "margin": "0px auto",
    "float": "none","left": "0px","right": "0px"});
     $('#rewardpopid').toggle();

     });
     

     $('#clsreward').click(function() { 

     $('#saddbookid').show(); 

     $('#rewardpopid').hide();

     });

    $('#getAddressId').click(function() {

        var addressId = $('input[name=address_id]:checked').val();

        var formData = new FormData();

        formData.append('address_id',addressId);


        $.ajax({

            type: 'POST',

            url: "<?php echo url('/').'/api/getUserAddressById'; ?>",

            data: formData,

            enctype: 'multipart/form-data',

            cache:false,

            contentType: false,

            processData: false,

            success: function(resultData){  

             var showAddress = '<strong>'+resultData.response.addressList.fullname+'</strong> <p>'+resultData.response.addressList.street_address+','+resultData.response.addressList.city+','+resultData.response.addressList.zip_code+'</p>';

              //var showAddress = '<div class="main_pop_s_m_one"><label class="container_on">'+resultData.response.addressList.address_type+'<input type="radio" checked="checked" name="radio"><span class="checkmark"></span></label><div class="main_pop_s_m_one_bt"><span><strong>'+resultData.response.addressList.fullname+'</strong></span><p>'+resultData.response.addressList.street_address+','+resultData.response.addressList.city+','+resultData.response.addressList.zip_code+'</p><p>'+resultData.response.addressList.contact_number+'</p></div></div>';

              $(".show_address").html(showAddress);

              $("#address_id").val(addressId);

              var sp = $('#shipping_price .tot_r').text();
              var sret = sp.split(" ");
              var sprice = sret[0];

              var totalprice = $("#total_price .tot_r").text();
              var ret = totalprice.split(" ");
              var price = ret[0];

              var gp = price - sprice;
              
              var gp1 = gp + resultData.response.addressList.shipping_price;

              $('#sprice').val(resultData.response.addressList.shipping_price);

              $('#shipping_price .tot_r').html(resultData.response.addressList.shipping_price + ' USD');

              $('#total_price .tot_r').html(gp1 + ' USD');
              //$('#showAddressBooknew').hide();

               //$('#exampleModalCenterOne').model("hide");

              $('#showAddressBooknew').modal("hide");

            }

          }); 

    });

  });
 $('#closecheckout').click(function() { 
   
    $(".modal-backdrop").css({"position": "relative"});

});
 $('#updateaddclose').click(function() { 
   
    $(".modal-backdrop").css({"position": "relative"});

});

    
 $('#rcouponid').click(function() { 

  var coupon = $('input[name="rcoupon"]:checked').val();

  if(coupon!=0 && coupon!=1){

   var fprice = $('#fprice').val();

   var sprice = $('#sprice').val();

              var dec = (coupon / 100).toFixed(2); //its convert 10 into 0.10

              var mult = (fprice * dec).toFixed(2); // gives the value for subtract from main value

              var discont = fprice - mult;

              var total_price = parseFloat(discont) + parseFloat(sprice); 

   var coupon_type = 'USD'; 

    var ccode = $("#ccode").text();

    $("#coupon_discount").html('<span class="tot_l">Coupon Discount</span><span class="tot_r">'+mult+' '+coupon_type+'</span>');

    $("#total_price").html('<span class="tot_l"><strong>Total</strong></span><span class="tot_r"><strong>'+total_price+' '+coupon_type+'</strong></span>');

    $("#coupon_code").val(ccode);

  }

 });



</script>

<script type="text/javascript">
  <?php if(!empty($tmpImgData)){ 
    foreach ($tmpImgData as  $value) { 
      if($value->type!="square"){
      ?>

    $(document).ready(function() {


      var degrees = 0;
      $('.rotatebtn').click(function rotateMe(e) {
       degrees += 90;

       if(degrees==90){
         degrees = 90;
          $('.btn4').css({
        'transform': 'rotate(' + degrees + 'deg)',
        '-ms-transform': 'rotate(' + degrees + 'deg)',
        '-moz-transform': 'rotate(' + degrees + 'deg)',
        '-webkit-transform': 'rotate(' + degrees + 'deg)',
        '-o-transform': 'rotate(' + degrees + 'deg)',
        'display' : 'block',
        
        });
          $('.rotate_img').css({'width' : '92%',
        'margin-top': '-86%',
        'height': '226px',});
          $('.rotation_deg').val(degrees);  
    
       }else{
          degrees = 0;
          $('.btn4').css({
        'transform': 'rotate(' + degrees + 'deg)',
        '-ms-transform': 'rotate(' + degrees + 'deg)',
        '-moz-transform': 'rotate(' + degrees + 'deg)',
        '-webkit-transform': 'rotate(' + degrees + 'deg)',
        '-o-transform': 'rotate(' + degrees + 'deg)',
        'display' : 'block',
        });

        $('.rotate_img').css({'width' : '79%',
        'margin-top': '-96%',
        'height': '278px',});
        $('.rotation_deg').val(degrees);  
       } 
      //$('.img').addClass('rotated'); // for one time rotation
      })
      });

<?php }else{ ?>
         $(document).ready(function() {


      var degrees = 0;
      $('.rotatebtn').click(function rotateMe(e) {
       degrees += 90;

       if(degrees==90){
         degrees = 90;
          $('.btn4').css({
        'transform': 'rotate(' + degrees + 'deg)',
        '-ms-transform': 'rotate(' + degrees + 'deg)',
        '-moz-transform': 'rotate(' + degrees + 'deg)',
        '-webkit-transform': 'rotate(' + degrees + 'deg)',
        '-o-transform': 'rotate(' + degrees + 'deg)',
        'display' : 'block',
        
        });
           $('.rotate_img').css({'width' : '79%',
        'margin-top': '-78%',
        'height': '226px',});
        $('.rotation_deg').val(degrees); 
    
       }else{
          degrees = 0;
          $('.btn4').css({
        'transform': 'rotate(' + degrees + 'deg)',
        '-ms-transform': 'rotate(' + degrees + 'deg)',
        '-moz-transform': 'rotate(' + degrees + 'deg)',
        '-webkit-transform': 'rotate(' + degrees + 'deg)',
        '-o-transform': 'rotate(' + degrees + 'deg)',
        'display' : 'block',
        });

        $('.rotate_img').css({'width' : '79%',
        'margin-top': '-78%',
        'height': '226px',});
        $('.rotation_deg').val(degrees);  
       } 
      //$('.img').addClass('rotated'); // for one time rotation
      })
      });
<?php   
}
}
 ?>
 <?php } ?> 
 <?php //}  ?>


</script>
<script type="text/javascript">
 <?php $msgs = Session::has('message'); ?>
  <?php if($msgs === "Order created Succesfully."){ ?>
    $(document).ready(function(){
      //alert('<?php echo $msgs; ?>');
          $('#orderthanks').modal('show');
    
     });

  <?php } ?>
   </script>
<script type="text/javascript">
 $(document).ready(function(){

     
        $("#crop").click(function(){
            var img = $("#cropbox").attr('src');
            var imgURL = img;
            var imgURLSegment = imgURL.substr(imgURL.lastIndexOf('/') + 1);

            var xx = $("#x2").val();
            var yy = $("#y2").val();

            var ww = $("#w2").val();
            var hh = $("#h2").val();
            
            $("#cropped_img").show();
            $("#cropped_img").attr('src','image-crop/'+xx+'/'+yy+'/'+ww+'/'+hh+'/'+imgURLSegment);
             var delay = 1000; 

            setTimeout(function(){ window.location.href = "<?php echo url('/select_frame'); ?>"; }, delay);
        });
  });



  var myCroppie = $('#image-container').croppie({
        enableExif: true,
                enableOrientation: true, 
        size: "original", 
        quality: 1,
              viewport: {
                width: 250,
                height: 250,
                type: 'square' //square
              },
              boundary: {
              width: 350,
              height: 350
         },
             
            });

  $('#adjustModal').on('shown.bs.modal', function(){ 
       myCroppie.croppie('bind', {
         url:$("#ImgID").val()
       }).then(function(){
         console.log('jQuery bind complete');
       });
});

  $('.upload-image').on('click', function (ev) {

    if($('#image').val() == "" && $('#cropframeID').val() ==""){
     
         return false;
    }

  myCroppie.croppie('result', {
    type: 'canvas',
    size: "original"
  }).then(function (img) {
    console.log(img);
 
    $.ajax({
      url: "{{url('/')}}/uploadCropImage",
      type: "POST",
      data: {"image":img,'crop_frame_id':$('#cropframeID').val()},
      success: function (resultData) {
      if(resultData.status){
       $('#notify').html(resultData.msg).removeClass('alert alert-danger').addClass('alert alert-success').fadeIn();
           location.reload();
    }else{
      $('#notify').html("Please try again").removeClass('alert alert-success').addClass('alert alert-danger').fadeIn();
       }
       
      }
    });
  });
});

  $('#savecropimg').on('click',function(){

    $.ajax({
        type: 'POST',
        url: "{{url('/')}}/save-crop-img",
        data: $('#imgcropfrm').serialize(),
        success: function(resultData){
        if(resultData.status){
          $('#notify').html(resultData.msg).removeClass('alert alert-danger').addClass('alert alert-success').fadeIn();
           $('#adjustModal').modal('hide');
        }else{
          $('#notify').html(resultData.msg).removeClass('alert alert-success').addClass('alert alert-danger');
        }  

        }
      }); 
    return false;
});

</script>

<div id="mybtnreload">

<script type="text/javascript">
// Get the modal
var modald = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("closed")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modald.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modald.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modald) {
    modald.style.display = "none";
  }
}
</script>

</div>

<script src="https://checkout.stripe.com/checkout.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script> 


<script type="text/javascript">
function showProcessing() {
    $('.subscribe-process').show();
}
function hideProcessing() {
    $('.subscribe-process').hide();
}


// function paypal() {
//     $('#stripebtn').hide();
//     $('#paypalbtn').show();
// }
// function stripe() {
//     $('#stripebtn').show();
//     $('#paypalbtn').hide();
// }

$('#paypal').click(function() {
  if($('#paypal').is(':checked')) 
  { 
    $('#stripebtn').hide();
    $('#paypalbtn').show();
  }                      
});


$('#stripe').click(function() {
  if($('#stripe').is(':checked')) 
  { 
    $('#stripebtn').show();
    $('#paypalbtn').hide();
  }                      
});


// Handling and displaying error during form submit.
function subscribeErrorHandler(jqXHR, textStatus, errorThrown) {
    try {
        var resp = JSON.parse(jqXHR.responseText);
        if ('error_param' in resp) {
            var errorMap = {};
            var errParam = resp.error_param;
            var errMsg = resp.error_msg;
            errorMap[errParam] = errMsg;
        } else {
            var errMsg = resp.error_msg;
            $("#alert-danger").addClass('alert alert-danger').removeClass('d-none').text(errMsg);
        }
    } catch (err) {
        $("#alert-danger").show().text("Error while processing your request");
    }
}

// Forward to thank you page after receiving success response.
function subscribeResponseHandler(responseJSON) {
//window.location.replace(responseJSON.successMsg);
    var url = "<?php echo url('/'); ?>";
    if (responseJSON.state == 'success') {
        //$("#alert-success").addClass('alert alert-success').removeClass('d-none').text(responseJSON.message);
       // $("#alert-danger").addClass('d-none');
        //alert(responseJSON.message);
        window.location.href = url+'/'+"payment-successful";
    }
    if (responseJSON.state == 'error') {
        //$("#alert-danger").addClass('alert alert-danger').removeClass('d-none').text(responseJSON.message);
        //$("#alert-success").addClass('d-none');
        alert(responseJSON.message);
    }

}
var handler = StripeCheckout.configure({
//Replace it with your stripe publishable key
    key: "{{ env('STRIPE_KEY') }}",
    image: 'https://networkprogramming.files.wordpress.com/2018/10/twitter.png', // add your company logo here
    allowRememberMe: false,
    token: handleStripeToken
});

function submitpayment() {
    var form = $("#subscribe-form");
    if (parseInt($("#amount").val()) <= 0) {
        return false;
    }
    handler.open({
        name: 'Stripe Payment',
        description: $("#plan").val()+' Plan',
        amount: ($("#amount").val() * 100)
    });
    return false;
}

function handleStripeToken(token, args) {
    form = $("#Order_form11");
    $("input[name='stripeToken']").val(token.id);
    var options = {
        beforeSend: showProcessing,
        // post-submit callback when error returns
        error: subscribeErrorHandler,
        // post-submit callback when success returns
        success: subscribeResponseHandler,
        complete: hideProcessing,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        dataType: 'json'
    };

    form.ajaxSubmit(options);
    return false;
}

$("#submit-btn-1").click(function(){
   $("#amount").val('9');
   $("#plan").val('Basic');
});
$("#submit-btn-2").click(function(){
   $("#amount").val('19');
   $("#plan").val('Premium');
});

$('#dark').click(function() {
  if($('#dark').is(':checked')) 
  { 
    var color = $('#dark').val();
    $('#color').val(color);
  }                      
});

$('#light').click(function() {
  if($('#light').is(':checked')) 
  { 
    var color = $('#light').val();
    $('#color').val(color);
  }                      
});

$('#green').click(function() {
  if($('#green').is(':checked')) 
  { 
    var color = $('#green').val();
    $('#color').val(color);
  }                      
});

$('#blue').click(function() {
  if($('#blue').is(':checked')) 
  { 
    var color = $('#blue').val();
    $('#color').val(color);
  }                      
});

$('#origin').click(function() {
  if($('#origin').is(':checked')) 
  { 
    var color = $('#origin').val();
    $('#color').val(color);
  }                      
});
</script>

@endsection


@section('tag_body')

<body>

  @endsection



  @section('content')

  <!--==========================

      Frame Style Section

      ============================-->


      <section id="secreload" class="section bx_mainCrop">

        <div class="selectFrameSec ">

          <h2>Frame Style</h2>

          <?php $guser_cat_id = Session::get('guser_cat_id'); ?>
          
          <ul class="nav nav-tabs" style="text-align: center">
            <h3>Painting Type</h3>
            <br/>
            <h4 style="font-size: 28px;"><?php echo $category->category_name; ?></h4>
            <br/>
          </ul> 
          <ul class="nav nav-tabs frameListsize">
            
          </ul>   

        </div>


        <div class="framesTabing">

          <div class="sectionWraper selectframe">
            <center>
            <div id="coloroption" style="display: none !important;">
               <label class="customradio"> <span class="radiotextsty">Dark </span>   
              <input type="radio" id="dark" name="color" value="Dark" checked=""> 
              <span class="checkmark"></span>
           </label>

              <label class="customradio"> <span class="radiotextsty">Light</span>  
              <input type="radio" id="light" name="color" value="Light"> 
              <span class="checkmark"></span>
           </label>

               <label class="customradio"> <span class="radiotextsty">Blue</span>  
              <input type="radio" id="blue" name="color" value="Blue"> 
              <span class="checkmark"></span>
           </label>

               <label class="customradio"> <span class="radiotextsty">Green</span>  
              <input type="radio" id="green" name="color" value="Green"> 
              <span class="checkmark"></span>
           </label>

               <label class="customradio"> <span class="radiotextsty">Origin</span>  
              <input type="radio" id="origin" name="color" value="Origin"> 
              <span class="checkmark"></span>
           </label>

            </div>
          </center>
            @if ($errors->any())

            <div class="alert alert-danger">

             <ul>

               @foreach ($errors->all() as $error)

               <li>{{ $error }}</li>

               @endforeach

             </ul>

           </div>

           @endif



           @if(Session::has('message'))

           <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>

           @endif



           @if(Session::has('error'))

           <div class="alert {{ Session::get('alert-class', 'alert-danger') }}"><p>{{ Session::get('error') }}</p></div>

           @endif



            <div class="tab-content">

              <div id="home" class="tab-pane fade in active show">


                @if (!empty($msg))

                <div class="alert alert-danger">

                  <ul>

                    

                    <li>{{ $msg }}</li>

                    

                  </ul>

                </div>

                @endif

               <?php $tempimgid = '';?>

               <?php //print_r($tmpImgData); ?>

                <?php if(!empty($tmpImgData)){ ?>

                <div class="row addimages">
                  <div class="frame_mobile_sec">
                  <?php foreach ($tmpImgData as  $value) { ?>

                    <?php $tempimgid = $value->id; ?>

                    <div class='col-3' id="row{{$value->id}}" style="margin: auto;">

                      <div class='bx_mig'>

                         <a  href="javascript:void(0);" class="img_bbb show_modal" data-toggle="modal" data-target="#showModal" data-src="{{url('/')}}/public/uploads/order_images/{{$value->filename}}" data-id="<?php echo $value->id ?>" >
                              
                        <?php if($value->alignment=="vertical" && $value->type=="square"){ ?>
                          @if($value->rotation=='0' && ($value->frame_id=='5' || $value->frame_id=='7'))
                             <?php if(!empty($value->thumb_filename)){ ?>
                                  <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;height: 270px;width: 270px;" >
                                <img id="upimages"  src="{{url('/')}}/public/uploads/order_images/{{$value->thumb_filename}}"  class="up_images" style="display: block;margin-left: -51.5%;margin-right: auto !important;min-width: 221px;width: 265px;object-fit: cover;height: 262px;"> 
                              <?php }else{ ?>

                                     <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;height: 270px;width: 270px;" >
                                      <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->filename}}"  class="up_images" style="display: block;margin-left: -51.5%;margin-right: auto !important;min-width: 221px;width: 265px;object-fit: cover;height: 262px;"> 
                              <?php } ?>
                              @elseif($value->rotation=='90' && ($value->frame_id=='5' || $value->frame_id=='7'))
                              <?php if(!empty($value->thumb_filename)){ ?>
                              <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;height: 270px;width: 270px;">
                              <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->thumb_filename}}"  class="up_images" style="display: block;margin-left: -50.2%;margin-right: auto !important;min-width: 221px;width: 262px;object-fit: cover;height: 264px;">
                              <?php } else { ?>
                              <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;height: 270px;width: 270px;">
                              <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->filename}}"  class="up_images" style="display: block;margin-left: -50.2%;margin-right: auto !important;min-width: 221px;width: 262px;object-fit: cover;height: 264px;">
                              <?php } ?>
                             @endif

                             @if($value->rotation=='0' && ($value->frame_id=='3' || $value->frame_id=='6'))
                             <?php if(!empty($value->thumb_filename)){ ?>
                                  <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;height: 270px;width: 270px;" >
                                <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->thumb_filename}}"  class="up_images" style="display: block;margin-left: -50%;margin-right: auto !important;min-width: 221px;width: 260px;object-fit: cover;height: 260px;"> 
                              <?php }else{ ?>

                                     <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;height: 270px;width: 270px;" >
                                      <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->filename}}"  class="up_images" style="display: block;margin-left: -50%;margin-right: auto !important;min-width: 221px;width: 260px;object-fit: cover;height: 260px;"> 
                              <?php } ?>
                              @elseif($value->rotation=='90' && ($value->frame_id=='3' || $value->frame_id=='6'))
                              <?php if(!empty($value->thumb_filename)){ ?>
                              <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;height: 270px;width: 270px;">
                              <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->thumb_filename}}"  class="up_images" style="display: block;margin-left: -50%;margin-right: auto !important;min-width: 221px;width: 260px;object-fit: cover;height: 260px;">
                              <?php } else { ?>
                              <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;height: 270px;width: 270px;">
                              <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->filename}}"  class="up_images" style="display: block;margin-left: -51%;margin-right: auto !important;min-width: 221px;width: 260px;object-fit: cover;height: 260px;">
                              <?php } ?>
                             @endif

                        <?php }?>


                            <?php if($value->alignment=="vertical" && $value->type=="rectangle"){ ?>
                          @if($value->rotation=='0')
                             <?php if(!empty($value->thumb_filename)){ ?>
                                  <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;margin-left: auto !important;margin-right: auto !important;height: 290px;" >
                                <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->thumb_filename}}"  class="up_images" style="display: block;margin-left: -100%;margin-right: auto !important;min-width: 273px;width: 200px;object-fit: cover;height: 285px;"> 
                              <?php }else{ ?>

                                     <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;margin-left: auto !important;margin-right: auto !important;height: 290px;" >
                                <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->filename}}"  class="up_images" style="display: block;margin-left: -100%;margin-right: auto !important;min-width: 273px;width: 200px;object-fit: cover;height: 285px;"> 
                              <?php } ?>
                              @else
                              <?php if(!empty($value->thumb_filename)){ ?>
                              <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;margin-left: 15% !important;margin-right: auto !important;width: 66%;height: 290px;">
                              <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->thumb_filename}}"  class="up_images" style="display: block;margin-left: -80%;margin-top: 20%;height: 182px;width: 275px;">
                              <?php } else { ?>
                              <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;margin-left: 15% !important;margin-right: auto !important;width: 66%;height: 290px;">
                              <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->filename}}"  class="up_images" style="display: block;margin-left: -80%;margin-top: 20%;height: 182px;width: 275px;">
                              <?php } ?>
                             @endif

                        <?php }?>


                        <?php if($value->alignment=="horizontal"){ ?>
                          @if($value->rotation=='90')
                             <?php if(!empty($value->thumb_filename)){ ?>
                                  <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform:rotate(0deg);display: block;margin-left: auto !important;margin-right: auto !important;height: 288px;width: 200px;" >
                                <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->thumb_filename}}"  class="up_images" style="display: block;margin-left: -91%;margin-right: auto !important;width: 81% !important;"> 
                            <?php } else { ?> 
                                 <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(0deg);display: block;margin-left: auto !important;margin-right: auto !important;height: 288px;width: 200px;" >
                                <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->filename}}"  class="up_images" style="display: block;margin-left: -91%;margin-right: auto !important;width: 81% !important;"> 
                            <?php } ?> 
                              @else
                              <?php if(!empty($value->thumb_filename)){ ?>
                              <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(90deg);display: block;margin-left: 15% !important;margin-right: auto !important;width: 66%;height: 290px;">
                              <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->thumb_filename}}"  class="up_images" style="display: block;margin-left: -80%;margin-top: 20%;height: 182px;width: 275px;">
                  <?php } else { ?>
                      <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(90deg);display: block;margin-left: 15% !important;margin-right: auto !important;width: 66%;height: 290px;">
                              <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->filename}}"  class="up_images" style="display: block;margin-left: -80%;margin-top: 20%;height: 182px;width: 275px;">
                  <?php } ?>
                             @endif

                        <?php }?>

                        </a> 

                      </div>

                      <div class="number">
                        <span class="numbersBox1">
                          <center><label><b>Face Count</b></label>
                          <input type="number" name="face_count" id="face_count" value="1" class="form-control" style="width: 50%;"></center>
                        </span>
                      </div>

                    </div>

                  <!--   <div id="fb-root"></div> -->
              

                  <?php } ?>
                </div>
                  </div>
                <?php }  ?>


                <div class="number1">
                	
                </div> 
               
                <div class="cleanStyl1">

                <div class="SquareUploadButton animated">

                  <div class="plus-icon">

                    <svg viewBox="0 0 37.76 38.93"><path fill="rgb(250,170,27)" class="plus-shape" d="M21.22,0V17.1H37.76v4.39H21.22V38.93H16.54V21.59H0V17.1H16.54V0Z"></path></svg>

                  </div> 



                  <div class="split">

                    <div class="button top">

                      <div class="tile-uploader skeleton">

                        <div class="drop-zone" aria-disabled="false" style="position: relative;">

                          <form method="POST" enctype="multipart/form-data"  id="previewForm" action="#">

                              <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />

                              <input type="hidden" name="cat_id" id="cat_id" value="{{Request::segment(2)}}" />

                              <input type="file" class="file-upload" id="images" name="filename[]" accept="image/*" onChange="submit_preview_image();">


                              <input type="hidden" class="input-quantity" name="frame_id" id="frame_id" value="3">

                      
                              <input type="hidden" id="user_id" name="user_id" value="{{!empty(@Auth::user()->id) ? @Auth::user()->id:csrf_token()}}">

                              <input type="hidden" class="select_frame_id" name="quantity" value="1">

                              <button type="submit" id="btnupload" >Upload Files!</button>

                          </form>

                        </div>

                      </div>

                      <img class="icon" src="{{url('/').'/resources/front_assets'}}/img/icon_upload.png">Upload Photos

                    </div>

                    <div class="button bottom">                    

                      <div class="cloud-icons" id="cloudsocial">

                        <ul>

                          <li>

                            <a><img class="cloud-icon" id="iconfb" src="{{url('/').'/resources/front_assets'}}/img/icon_fb.png"></a>

                          </li>

                          <li>

                            <a><img class="cloud-icon" id="iconinsta" src="{{url('/').'/resources/front_assets'}}/img/icon_insta.png"></a>

                          </li>

                          <li>

                            <a><img class="cloud-icon" id="icondrive" src="{{url('/').'/resources/front_assets'}}/img/icon_gdrive.png"></a>

                          </li>


                        </ul>

                      </div>

                      <div class="text">Choose from Online Services</div>

                    </div>

                  </div>

                </div>

              </div>
       

<!-- Trigger/Open The Modal -->
<button id="myBtn"><img src="https://votivelaravel.in/newframeit/resources/front_assets/img/plusadd.png"></button>

<!-- The Modal -->
<div id="myModal" class="modald">

  <!-- Modal content -->
  <div class="modal-contentd">
    <div class="modal-headerd" >
      <span class="closed"></span>
      </div>
     <div class="modal-bodyd social-icon ">

                   <div class="mobileup" id="mobileup">

                    <div class="button top ">

                      <div class="tile-uploader skeleton">

                        <div class="drop-zone" aria-disabled="false" style="position: relative;">

                          <form method="POST" enctype="multipart/form-data"  id="previewFormmobile" action="#">

                              <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />

                              <input type="hidden" name="cat_id" id="cat_id" value="{{Request::segment(2)}}" />

                              <input type="file" class="file-upload" id="images" name="filename[]" accept="image/*" onChange="submit_preview_image_mobile();">


                              <input type="hidden" class="input-quantity" name="frame_id" id="frame_id" value="3">

                      
                              <input type="hidden" id="user_id" name="user_id" value="{{!empty(@Auth::user()->id) ? @Auth::user()->id:csrf_token()}}">

                              <input type="hidden" class="select_frame_id" name="quantity" value="1">

                              <button type="submit" id="btnupload" >Upload Files!</button>

                          </form>

                        </div>

                      </div>

                      <img class="cloud-icon" src="{{url('/').'/resources/front_assets'}}/img/phone.png"><span>Choose from Phone</span>

                    </div>
                  </div>

                           
 <!--     <a> <img class="cloud-icon" id="iconphone" src="https://votivelaravel.in/newframeit/resources/front_assets/img/phone.png"> <span>Choose from Phone </span> </a> -->
 

<a id="iconfb"><span>Choose from Facebook</span></a> 

<a id="iconinsta"><span>Choose from Instagram </span> </a>

<a id="icondrive"><span>Choose from Google Drive </span> </a>


      </div>
   
  </div>

</div>


            
<style type="text/css">
  
 .social-icon img{
  width: 35px;
 } 
</style>

              </div>

            </div>


          </div>

        </div>

        

      </section> 

      

      @endsection



      @section('page_footer')

      <footer class="footerSectopm wow fadeInUp">

        <div class="footerBtn">

          <!-- <a href="<?php //echo url('/select_frame').'/'.$category->cat_id; ?>">Continue<i class="fa fa-chevron-right"></i></a> -->

          <?php if($user_id=='' && $tempimgid!=''){ ?>  

          <button id="userlogin"> <span href="#" data-toggle="modal" data-target="#loginModal">Sign in</span> </button> 

          <?php }else{ ?>

          <button onClick="continue1(<?php echo $category->cat_id; ?>)" id="continue1">Continue<i class="fa fa-chevron-right"></i></button>

          <?php } ?> 

          <!--  <button onClick="continue2(<?php //echo $category->cat_id; ?>)" id="continue2" style="display:none;">Continue<i class="fa fa-chevron-right"></i></button> -->

           <button data-toggle="modal" data-target="#exampleModalCenter" style="display:none;" id="ckeckout">Check Out <i class="fa fa-chevron-right"></i></button>

           <button style="display:none;" id="userlogin"> <span href="#" data-toggle="modal" data-target="#loginModal">Sign in</span> </button> 
   

        </div>

      </footer>



      <!-- start remove and adjust Modal -->

      <div class="modal fade pop_address" id="exampleModalCenterApc" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

        <div class="modal-dialog modal-dialog-centered" role="document">

          <div class="modal-content">        

            <div class="modal-body">

              <div class="address-form">

                <div class="top-bar-container">

                  <div class="top-bar">

                    <div class="left-comp">

                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times; <span class="cls_btn">Close</span></span>

                      </button>        

                    </div>

                    <div class="title_fgh">Add Promo Code</div>                  

                  </div>                

                </div>



                <form id="addPromoCode" mehtod="POST" action="#" class="add_form">

                  <div id="applycouponBox"></div>

                  <div class="form-label">Insert your promo code</div>

                  <div class="in_firs">

                    <input type="hidden" name="user_id" value="{{!empty(@Auth::user()->id) ? @Auth::user()->id:''}}">

                    <input class="FormInput" type="text" name="coupon_code" >

                  </div>

                  <div class="button_cont">            

                    <input class="btn_Submit" type="submit" value="Claim">

                  </div>

                </form>          

              </div>     

            </div>        

          </div>

        </div>

      </div>

      <!-- end remove and adjust Modal -->



      <!-- start remove and adjust Modal -->

      <div class="modal fade" id="adjustModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

        <div class="modal-dialog modal-dialog-centered" role="document">

          <div class="modal-content">   

              <!-- Modal Header cropboxwidth-->

              <div class="modal-header">

                  <center><h4 class="modal-title">Upload Image and Adjust</h4></center>

                  <!-- <input type='button' id="crop" value='Done'> -->
                  <div class="updiv"><button class="pull-right  upload-image" id="crop" >Done</button></div>

                  <input type='button' class="upload-image" id="rotatedone" onClick="save_rotation();" value='Done' style="display:none;">
                  <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->

              </div>

              <!-- Modal body -->

              <div class="modal-body">

                <div id="cropdiv">
          <div class="size-frma">        
                  
          <label class="customradio"> <span class="radiotextsty">Square crop </span>
            <input type="radio" class="sqare" name="cropf" value="sqare">
           <span class="checkmark"></span>
           </label>
          <label  class="customradio"><span class="radiotextsty">Rectangle Vertical crop </span>
            <input type="radio" class="reactangle_v" name="cropf" value="reactangle_v"> 
              <span class="checkmark"></span>
          </label>
          <label  class="customradio">
            <input type="radio" class="reactangle_h" name="cropf" value="reactangle_h"><span class="radiotextsty"> Rectangle horizontal crop</span>
            <span class="checkmark"></span>
          </label>
        </div>

               <input type="hidden" id="ImgID" name="">
                <input type="hidden" name="crop_frame_id" id="cropframeID" value="">
           
            <div id="image-container">
              
            </div>
           <div id="notify" style="display:none;" class="alert-dismissible"></div>
             
          <!--   <input type="file" id="image" style="visibility: hidden;"> -->
                <div id="btn" class="salic-icon">
                  <center> 
                  <i class="fa fa-crop" aria-hidden="true" style="font-size: 20px;"></i>
                   <i class="fa fa-repeat rotatebtn" id="div1" aria-hidden="true" style="font-size: 20px;"></i>
                  </center>
                </div>
                <!-- <div style="display: none;">
                  <img src="#" id="cropped_img" style="display: none;">
                </div> -->
              </div>

               <!-- <input type="hidden" name="x2" id="x2" value="">
               <input type="hidden" name="y2" id="y2" value="">
               <input type="hidden" name="w2" id="w2" value="">
               <input type="hidden" name="h2" id="h2" value=""> -->


              <div id="divrotation" style="display: none;">
                <h5>Pinch and Zoom</h5>
                  <div class="bx_migPinch" style="height: 100%;min-height: 250px;margin-top: 15%;">
                  
                   
                   <div class="" id="row187">
                  
                   <input type="hidden" name="rotation_id" class="rotation_id" value="">
                   <input type="hidden" name="rotation_deg" class="rotation_deg" value="">
                  <?php //if($value->title=='Rectangle'){ ?>
                        <style>

                        </style>
                        <div class="rotatediv" >
                          <?php /* if(!empty($tmpImgData)){ ?>
                          <?php foreach ($tmpImgData as  $value) {
                              // echo '<pre>';
                              // print_r($tmpImgData);

                           }
                           } */?> 
                           @if(!empty($value->images))
                           <?php if($value->type=='square'){ ?>
                            
                             <img class="btn4" src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}"  style="display: none;margin-left: auto;margin-right: auto;width:80%">
                            

                            <img  src="{{url('/')}}/public/uploads/order_images/1595922106_fhdjhjdfh.jpg" class="show_modal img_bbb rotate_img" style="display: block;margin-left: auto;margin-right: auto;width: 80%;margin-top: -79.5%;height: 235px;">

                          
                        <?php } else{ ?>
                          <img class="btn4" src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}"  style="display: none;margin-left: auto;margin-right: auto;width:80%">

                          <img  src="{{url('/')}}/public/uploads/order_images/1595922106_fhdjhjdfh.jpg" class="show_modal img_bbb rotate_img" style="display: block;margin-left: auto;margin-right: auto;width: 79%;margin-top: -54%;height: 158px;">
                        <?php } ?>
                          @endif
                      </div>
                  <?php //}else { ?>
                        
                  <?php //} ?>
                      <?php //} 
                  //} ?> 
                        
                    </div>
                    <div id="btn" class="salic-icon" style="margin-top: 20%;">
                        <center> 
                        <i class="fa fa-crop" id="div2" aria-hidden="true" style="font-size: 20px;" ></i>
                        <i class="fa fa-repeat rotatebtn"  aria-hidden="true" style="font-size: 20px;"></i>
                        </center>
                        </div>
                  
                </div>
              </div>
              </div>       

          </div>

        </div>

      </div>
      <!-- end remove and adjust Modal -->



       <div class="modal fade" id="sharepopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered bx_pinch" role="document">
          <div class="modal-content"> 
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  
              </div>
              <div class="modal-body">
                <h2>Tell Your Friends</h2>
                <p>Receive $25 coupon when friends make their first purchase.
                <br>Share your referral URL with friend</p>
                <div class="alert alert-success">
                  <strong>Your Referral URL</strong> {{url('/signup')}}/{{@Auth::user()->my_referral_code}}
                </div>
                <div class="addthis_inline_share_toolbox"></div>
                
                <!-- <span class="ico_rot rotatebtn"><i class="fa fa-repeat" aria-hidden="true"></i></span> -->
              </div>
          </div>
        </div>
      </div>


  <div class="modal fade" id="myframesave" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered bx_pinch" role="document" style="max-width: 100%;
    width: 300px;">
          <div class="modal-content"> 
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Your frame are saved !</h4>
                 <!--  <a href="#">Done</a> -->
              </div>
              <div class="modal-body trans_su">
                 <img  id="" class="her_ico" width="110px" src="{{url('/')}}/resources/front_assets/img/heart-icon.png">
                <h4 class="modal-title">Lovely Photos !</h4>
                <strong class="ord_id">Can't wait for your <br> frames to be printed</strong>

                <span class="bot_butt_s"><a href="{{url('/')}}">Back to Homepage</a>
                <a href="{{url('/')}}/edit-profile">Go to Your Orders</a>

                <p data-toggle="modal" data-target="#sharepopup" class="fri_bod adjust_img" data-dismiss="modal" >Shared this product and earn credits</p>
                </span>
                
              </div>
          </div>
        </div>
      </div>


      <div class="modal fade" id="orderthanks" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered bx_pinch" role="document" style="max-width: 100%;
    width: 300px;">
          <div class="modal-content"> 
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Transaction Successful</h4>
                 <!--  <a href="#">Done</a> -->
              </div>
              <div class="modal-body trans_su">
                 <img  id="" class="her_ico" width="110px" src="{{url('/')}}/resources/front_assets/img/heart-icon.png">
                <h4 class="modal-title">Thank You <br> for your order</h4>
                <strong class="ord_id"></strong>

                <span class="bot_butt_s"><a href="{{url('/')}}">Back to Homepage</a>
                <a href="{{url('/')}}/edit-profile">Go to Your Orders</a>

                <p data-toggle="modal" data-target="#sharepopup" class="fri_bod adjust_img" data-dismiss="modal" >Shared this product and earn credits</p>
                </span>
                
              </div>
          </div>
        </div>
      </div>


     <!--  <div id="myframesave" class="modal fade" role="dialog">
  <div class="modal-dialog trans_suoo">
    <div class="modal-content trans_suo">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Your frame are saved !</h4>
      </div>
      <div class="modal-body trans_su">

        <img  id="" class="her_ico" width="110px" src="{{url('/')}}/resources/front_assets/img/heart-icon.png">

        <h3>Lovely Photos !</h3>
    <strong class="ord_id">Can't wait for your <br> frames to be printed</strong>

    <span class="bot_butt_s"><a href="{{url('/')}}">Back to Homepage</a>
    <a href="{{url('/')}}/edit-profile">Go to Your Orders</a>

    <p>Shared this product and earn credits</p>
    </span>
      </div>
 
    </div>

  </div>
</div> -->



      <!-- start remove and adjust Modal -->

      <div class="modal fade pop_card" id="showModal" tabindex="-1" role="dialog" aria-labelledby="exampleShowModal" aria-hidden="true">

        <div class="modal-dialog modal-dialog-centered" role="document">

          <div class="modal-content">      

            <div class="modal-body">        

              <ul class="list_diss">

                
<li class="last_dis"> <a href="javascript:void(0);"  data-toggle="modal" data-target="#adjustModal" data-dismiss="modal" ><i class="fa fa-crop" aria-hidden="true"></i> Adjust</a></li>

                <!-- <li><a href="javascript:void(0);" data-toggle="modal" data-target="#sharepopup" class="fri_bod adjust_img" data-dismiss="modal" >share</a></li> -->

<li class="last_dis"> <a href="javascript:void(0);" class="mid_bod delete_preview" data-dismiss="modal" ><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</a></li>



<li class="last_dis"><a href="javascript:void(0);" class="mid_bod" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Dismiss</a>

                </li>

              </ul>

            </div>        

          </div>

        </div>

      </div>

      <!-- end remove and adjust Modal -->



      <div class="modal fade pop_cart" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

        <div class="modal-dialog" role="document">

          <div class="modal-content">

            <button type="button" class="close" id="closecheckout" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">&times;</span>

            </button> 




            <form id="Order_form11" method="POST" action="{{url('createOrder')}}" enctype="multipart/form-data">      

              <div class="modal-body">

                <h5 class="modal_hd cl_oo" id="exampleModalLongTitle">Checkout</h5>

                

                  <!-- <ul class="list_address">

                  <li class="show_address">

                      <a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModalCenterOne"><img class="cloud-icon" src="{{url('/').'/resources/front_assets'}}/img/icon_home.png">Add Address</a>

                   

                  </li>

                  <li>

                    <a href="#" data-toggle="modal" data-target="#exampleModalCenterApc"><img class="cloud-icon" src="{{url('/').'/resources/front_assets'}}/img/icon_card.png"> Add Promo Code</a>

                  </li>

                  <div id="wallet_amount"></div>

                  <div id="order_msg"></div>

                  </ul> -->



<div class="pop_a_top">

<!-- <a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModalCenterOne"><img class="cloud-icon" src="{{url('/').'/resources/front_assets'}}/img/icon_home.png"> <b>Add Address</b> </a> -->

<span class="ll_home"> 

  <a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModalCenterOne"><h4><img class="cloud-icon" src="{{url('/').'/resources/front_assets'}}/img/icon_home.png"> <b id="clsreward">Add Address </b></h4></a>

  <!-- <i data-toggle="modal" data-target="#exampleModalCenterOne" class="fa fa-home" aria-hidden="true"></i>

  <b>Add Address</b>  -->

  <h4><i data-toggle="modal" data-target="#exampleModalCenterOne" class="fa fa-money-bill" aria-hidden="true"></i>

  <b id="clsreward">Payment Method</b></h4>
  <input type="radio" name="payment" id="paypal" value="paypal" checked="checked"> Paypal
  <input type="radio" name="payment" id="stripe" value="stripe" onClick="stripe();"> Stripe
</span>



<!-- <span class="l_rrr_r show_address"> 

<strong>  </strong> 

<p></p><br/>

</span> -->


<!-- <a href="javascript:void(0);" data-toggle="modal" data-target="#rewardpopid" id="umreward"><h4><i class="fa fa-star-o" aria-hidden="true"></i><b >Use My Rewards</b></h4></a>   -->
   

</div>

                  

                    <div class="tot">



                    <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />



                    <input type="hidden" name="user_id" value="{{!empty(@Auth::user()->id) ? @Auth::user()->id:''}}">

                    

                    <input type="hidden" name="address_id" class="address_id" id="address_id" value="{{!empty(@Auth::user()->ship_id) ? @Auth::user()->ship_id:''}}">

                   

                    <input type="hidden" name="coupon_code" class="coupon_code" id="coupon_code" value="">

                    <input type="hidden" name="wallet_amount" class="wallet_amt" id="wallet_amt" value="">



                    <?php 

                      $qty = 0;  $frame_price = 0; $currency = "";

                      if(!empty($tmpImgData)){ 

                      foreach ($tmpImgData as  $value) { 

                      $qty +=$value->quantity;

                      $frame_price = $value->price * $qty;

                      $currency = 'USD';

                    ?>
                    <?php if(!empty($value->thumb_filename)){ ?>
                        <input type="hidden" name="tile_image[]" value="{{ $value->thumb_filename }}">
                    <?php }else{ ?>
                         <input type="hidden" name="tile_image[]" value="{{ $value->filename }}">
                    <?php } ?>  

                       <input type="hidden" name="rotation[]" value="{{ $value->rotation }}">

                      <input type="hidden" name="quantity[]" value="{{ $value->quantity }}">



                      <input type="hidden" name="frame_id" class="frame_id" value="{{ $value->frame_id }}">



                    <?php

                        } 

                      } 

                    ?>

                    <input type="hidden" name="stripeToken" id="stripeToken" value="" /> 

                    <input type="hidden" name="total_tile" class="tttile" id="ttile" value="1">

                    <input type="hidden" name="color" class="color" id="color" value="Dark">

                    <input type="hidden" name="total_price1" class="ttprice" id="fprice" value="{{ $frame_price }}">

                    <input type="hidden" name="shipping_price" id="sprice" value="{{!empty(@Auth::user()->shipping_price) ? @Auth::user()->shipping_price:''}}">

                    <?php if($category->head_count>0){ ?>
                    <span class="">Number People</span>
                    <input type="hidden" name="number_people" class="number_people" id="number_people" value="">
                    <br>
                    <?php } ?>
                  
                    <span class="tot_l">1 Frame </span>

                    <span class="tot_r tprice">{{ $frame_price }} USD</span>

                  </div>

                  <div class="tot">

                    <span class="tot_l"><input type="checkbox" name="wallet_amt" /> Wallet Balance</span>

                    <span class="tot_r">{{!empty(@Auth::user()->wallet_balance) ? @Auth::user()->wallet_balance:'0'}} USD</span>

                  </div>

                  <div class="tot" id="rdiscount"></div>

                  <div class="tot" id="coupon_discount"> </div>



                  <div class="tot" id="shipping_price"><span class="tot_l">Shipping Price</span><span class="tot_r">{{@Auth::user()->shipping_price }} USD</span>

                  </div>

                  <?php 

                    $date = date('d/m/Y', strtotime('+7 days'));

                  ?>

                  <p class="pr_co">Delivery Date <?php echo $date; ?> </p>           
   
                  <div class="tot tot_main" id="total_price">

                    <span class="tot_l"><strong>Total</strong></span>

                    <span class="tot_r ttprice"><strong>{{ $frame_price + @Auth::user()->shipping_price }} USD</strong></span>

                  </div>





                <?php //if($qty == '0'){ ?>



                  <div class="btn_bot pl_ord12">

                 

                  <!--  <a href="javascript:void(0);"  data-toggle="modal" data-target="#place_order_sed" > Place Order</a> -->



                <?php //}else{ ?> 

                  <!-- <div>

                  </div>  -->


                  <div class="btn_bot pl_ord">

                    <input type="submit" id="paypalbtn" name="Place Order"  value="Place Order">

                  </div>

                  <div class="btn_bot pl_ord">
                    <a href="#" id="stripebtn" onClick="submitpayment();" style="display:none">Place Order</a>
                  </div>


                 <div class="plkkk"><a href="javascript:void(0);" class="save_frT" id="savefmo" onClick="save_frame_mo();"> Save Frames to My Order</a></div>



                <?php //} ?> 



              </div>

            </form> 
     
          </div>

        </div>

      </div>



      <!-- update address start -->


      <!-- create address start -->

      <div class="modal fade pop_address" id="exampleModalCenterOne" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

        <div class="modal-dialog modal-dialog-centered" role="document" id="saddbookid">

          <div class="modal-content">        

            <div class="modal-body">

              <div class="address-form">



                <form  id="createShipAddress1" action="#" enctype="multipart/form-data">

                    @csrf

                  <input type="hidden" name="user_id" value="{{!empty(@Auth::user()->id) ? @Auth::user()->id:''}}">



                  <div class="top-bar-container">

                    <div class="top-bar">

                      <div class="left-comp">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                          <span aria-hidden="true">&times; <span class="cls_btn">Close</span></span>

                        </button>        

                      </div>

                      <div class="title_fgh">Add Address</div>

                      <div class="title_fgh">

                     <!--  <button type="button" class="btn btn-default Sel_add" data-toggle="modal" data-target="#showAddressBooknew" id="addressmodel">Select From Address Book</button> -->

                      <button type="button" class="btn btn-default Sel_add" data-toggle="modal" data-target="#showAddressBooknew" id="addressmodel">Select From Address Book</button>

                      <div class="right-comp">

                        <input type="submit" class="DoneButton" name="Sbmtform" value="Done">

                        <!-- <a href="#" class="DoneButton">Done</a> -->

                      </div>

                    </div>

                    <div id="signupResBox1"></div>

                    <div class="bottom-comp"></div>

                  </div>



                  <div class="ioc_emoh">

                    <img class="cloud-icon" src="{{url('/').'/resources/front_assets'}}/img/icon_home.png">

                  </div>



                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">Address Type</div>

                    <input class="FormInput newAddressFormStyle" type="text" id="address_type" name="address_type" placeholder="Address Type" value="">

                  </div>



                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">Full Name</div>

                    <input class="FormInput newAddressFormStyle" type="text" id="fullname" name="fullname" placeholder="Full Name" value="">

                  </div>



                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">Street Address</div>

                    <input class="FormInput newAddressFormStyle" type="text" id="street_address" name="street_address" placeholder="Street Address"  value="">

                  </div>           



                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">Zip Code</div>

                    <input class="FormInput newAddressFormStyle" type="text" id="zip_code" name="zip_code" placeholder="Zip Code" value="">

                  </div>



                  <div class="newAddressInputWrapper country-select-container">

                    <div class="InputLabel">Country</div>

                      <select class="InputLabel country_list" name="country">

                        <option value="">Select Country</option>

                      </select>

                  </div>



                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">State / Province</div>

                    <input class="FormInput newAddressFormStyle" type="text" id="state" name="state" placeholder="State" value="">

                  </div>



                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">City</div>

                    <input class="FormInput newAddressFormStyle" type="text" id="city" name="city" placeholder="City" value="">

                  </div>



                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">Phone Number</div>

                    <input class="FormInput newAddressFormStyle" type="text" id="contact_number" name="contact_number" placeholder="Contact Number" value="">

                  </div> 

                </div>

                </form>



              </div>     

            </div>        

          </div>

        </div>

      </div>



      <div class="rewardpop">
        <div class="modal-dialog modal-dialog-centered" id="rewardpopid" style="display:none;">      
            <div class="modal-body">
    
      <!-- Modal content-->

      <div class="modal-content select_form_address_n my_reew" style="padding:20px;">
        <div class="modal-header my_reew_nn">

        <!--   <button type="button" class="close" data-dismiss="modal">&times; Close</button> -->
          <h4 class="modal-title">My Rewards</h4>
           <button type="button" class="close" data-dismiss="modal">&times; </button>
        <!--   <a href="#">Done</a> -->
        </div>
        <div class="modal-body select_form_address_nn">
         
          <div class="main_pop_s_m">
          <input type="hidden" name="userid" value="<?php echo @Auth::user()->id; ?>" id="userid"> 
          <input type="hidden" name="totalamount" id="totalamount" value="{{$frame_price}}">
		  <div class="main_pop_s_m_one ">
		  	<div id="mymsgreward"></div>
		  <label class="container_on">Available Reward Credits
		   <input type="radio" checked="checked" id="rtype" name="rcoupon" value="Reward">
		  <span class="checkmark"></span><a href="#" class="pt_s">{{!empty(@Auth::user()->wallet_balance) ? @Auth::user()->wallet_balance:'0'}} USD</a>
		  </label>

		 <div class="main_pop_s_m_one_bt">
		   <input type="number" class="form-control col_wht" id="ramount" aria-describedby="emailHelp" placeholder="Amount Redeem Reward Credits" required>
		   
		 </div>
		 </div>


			<?php foreach ($coupon as $key => $value) { 

				?>
			<?php $date=date('yy-m-d'); 
			if($date<=$value->end_date){
			?>	
			 <div class="main_pop_s_m_one">
			 <label class="container_on">{{$value->name}}
			  <input type="radio" name="rcouponcode" id="valuecoupon"  value="{{$value->value}}">
			  <span class="checkmark" data-attr="{{$value->code}}"></span>
			 </label>
			 <p class="pr_co">promocode <span id="ccode">{{$value->code}}</span> expire on {{$value->end_date}}</p>
			</div>

			<?php } }?>


			<div class="main_pop_s_m_one">
			 <label class="container_on">Insert your promocode
			  <input type="radio" name="rcoupon" id="coupontype" value="Coupon">
			  <span class="checkmark"></span>
			 </label>
			<div class="main_pop_s_m_one_bt">
			  <input type="text" class="form-control col_wht" id="code" aria-describedby="emailHelp" placeholder="Code" required>
			 </div>
			</div>


			<div class="main_pop_s_m_one sel_sect">
			<!-- <a href="#" class="select_s_ss" id="rcouponid">Select</a> -->
			<a href="#" class="select_s_ss" id="" onClick="applyrewardorcoupon();">Select</a>
			</div>

</div>
</div>
                  
</div>
</div>
</div>
</div>

	<?php $i=1; 
    if(!empty($addressList)){
  ?>
     @foreach($addressList as $address)
         <!-- create address start -->
      <div class="editaddress">
        <div class="modal fade pop_address" id="editaddress{{$address->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="z-index: 9999;">
        <div class="modal-dialog" role="document">
          <div class="modal-content">        
            <div class="modal-body">
              <div class="address-form">

                <form  id="updateShipAddress" action="#" enctype="multipart/form-data">

                  {{ csrf_field() }}  
                  
                  <input type="hidden" name="user_id" value="{{!empty(@Auth::user()->id) ? @Auth::user()->id:''}}">
                  <input type="hidden" name="ship_id" id="addressids" value="{{$address->id}}">

                   <div class="top-bar-container">
                    <div class="top-bar">
                      <div class="left-comp">
                        <button type="button" class="close" id="updateaddclose" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times; <span class="cls_btn">Close</span></span>
                        </button>        
                      </div>
                      <div class="title_fgh">Update Address</div>
                      <div class="right-comp">
                        <input type="submit" class="DoneButton" name="upSbmtform" value="Done">
                        <!-- <a href="#" class="DoneButton">Done</a> -->
                      </div>
                    </div>
                    <div id="updatesignupResBox"></div>
                    <div class="bottom-comp"></div>
                  </div>

                  <div class="ioc_emoh">
                    <img class="cloud-icon" src="{{url('/').'/resources/front_assets'}}/img/icon_home.png">
                  </div>

                  <div class="newAddressInputWrapper">
                    <div class="InputLabel">Address Type</div>
                    <input class="FormInput newAddressFormStyle" type="text" id="address_type" name="address_type" placeholder="Address Type" value="{{$address->address_type}}">
                  </div>

                  <div class="newAddressInputWrapper">
                    <div class="InputLabel">Full Name</div>
                    <input class="FormInput newAddressFormStyle" type="text" id="fullname" name="fullname" placeholder="Full Name" value="{{$address->fullname}}">
                  </div>

                  <div class="newAddressInputWrapper">
                    <div class="InputLabel">Street Address</div>
                    <input class="FormInput newAddressFormStyle" type="text" id="street_address" name="street_address" placeholder="Street Address"  value="{{$address->street_address}}">
                  </div>           

                  <div class="newAddressInputWrapper">
                    <div class="InputLabel">Zip Code</div>
                    <input class="FormInput newAddressFormStyle" type="text" id="zip_code" name="zip_code" placeholder="Zip Code" value="{{$address->zip_code}}">
                  </div>

                  <div class="newAddressInputWrapper country-select-container">
                    <div class="InputLabel">Country</div>
                      <select class="InputLabel country_list" name="country">
                      	 @foreach($countries as $country)
                         <option  value="{{$country->id}}" <?php if($country->id==$address->country_id){ echo "selected"; } ?>>{{$country->name}}</option>
                        @endforeach
                      </select>
                  </div>

                  <div class="newAddressInputWrapper">
                    <div class="InputLabel">State / Province</div>
                    <input class="FormInput newAddressFormStyle" type="text" id="state" name="state" placeholder="State" value="{{$address->state}}">
                  </div>

                  <div class="newAddressInputWrapper">
                    <div class="InputLabel">City</div>
                    <input class="FormInput newAddressFormStyle" type="text" id="city" name="city" placeholder="City" value="{{$address->city}}">
                  </div>

                  <div class="newAddressInputWrapper">
                    <div class="InputLabel">Phone Number</div>
                    <input class="FormInput newAddressFormStyle" type="text" id="contact_number" name="contact_number" placeholder="Contact Number" value="{{$address->contact_number}}">
                  </div> 

                </form>

              </div>     
            </div>
            </div>        
          </div>
        </div>
      </div>   
      <?php $i++; ?>
      <!-- update address stop -->
      @endforeach
    <?php } ?>

  <!-- Modal -->

  <div class="modal fade select_form_address" id="showAddressBooknew" role="dialog">

    <div class="modal-dialog select_form_addressll">
  <!-- Modal content-->

      <div class="modal-content select_form_address_n">

        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal">&times; Close</button>

          <h4 class="modal-title">Address Book</h4>

          <a href="#"></a>

        </div>

        <div class="modal-body select_form_address_nn">

         

        <div class="main_pop_s_m addressBookList">

        

        </div>



        </div>

        <div class="modal-footer">

          <button type="button" class="btn btn-default add_ad" id="addnaddress" data-toggle="modal" data-target="#exampleModalCenterOne"> + Add new Address </button>

          <button type="submit" class="btn btn-default Sel_add" id="getAddressId">Select</button>

        </div>

      </div>

      

    </div>

  </div>



 


  <div class="modal fade pop_card" id="exampleModalCenterTwo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" role="document">

      <div class="modal-content">

        <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">

          <span aria-hidden="true">&times;</span>

        </button>-->        

        <div class="modal-body">        

          <ul class="list_diss">

            <li>

              <a href="javascript:void(0);" class="fri_bod" data-toggle="modal" data-target="#exampleModalCenterThree">Use Credit Card</a>

            </li>

            <li>

              <a href="javascript:void(0);" class="mid_bod">Use Paypal</a>

            </li>

            <li class="last_dis">

              <a href="javascript:void(0);" class="mid_bod" data-dismiss="modal">Dismiss</a>

            </li>

          </ul>

        </div>        

      </div>

    </div>

  </div>



      <div class="modal fade pop_address" id="exampleModalCenterThree" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

        <div class="modal-dialog modal-dialog-centered" role="document">

          <div class="modal-content">        

            <div class="modal-body">

              <div class="address-form">

                <div class="top-bar-container">

                  <div class="top-bar">

                    <div class="left-comp">

                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times; <span class="cls_btn">Close</span></span>

                      </button>        

                    </div>

                    <div class="title_fgh">Add Card</div>

                    <div class="right-comp">

                      <a href="#" class="DoneButton">Done</a>

                    </div>

                  </div>

                  <div class="bottom-comp"></div>

                </div>

                <ul class="list_crd">

                  <li><a href="#"><img class="cloud-icon" src="{{url('/').'/resources/front_assets'}}/img/icon5.png"></a></li>

                  <li><a href="#"><img class="cloud-icon" src="{{url('/').'/resources/front_assets'}}/img/icon4.png"></a></li>

                  <li><a href="#"><img class="cloud-icon" src="{{url('/').'/resources/front_assets'}}/img/icon1.png"></a></li>

                  <li><a href="#"><img class="cloud-icon" src="{{url('/').'/resources/front_assets'}}/img/icon2.png"></a></li>              

                  <li><a href="#"><img class="cloud-icon" src="{{url('/').'/resources/front_assets'}}/img/icon3.png"></a></li>

                </ul>

                <div class="ioc_emoh">

                  <img class="cloud-icon" src="{{url('/').'/resources/front_assets'}}/img/icon_card.png"> <span>Card Detail</span>

                </div>

                <form>

                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">Card Number</div>

                    <input class="FormInput newAddressFormStyle" type="text" name="" placeholder="4444-4444-4444-4444" value="">

                  </div>

                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">MM/YY</div>

                    <input class="FormInput newAddressFormStyle" type="text" name="street" placeholder="MM/YY"  value="">

                  </div>           

                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">CVV</div>

                    <input class="FormInput newAddressFormStyle" type="text" name="city" placeholder="012" value="">

                  </div>

                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">Email</div>

                    <input class="FormInput newAddressFormStyle" type="text" name="state" placeholder="test@gmail.com" value="">

                  </div>

                </form>

              </div>     

            </div>        

          </div>

        </div>

      </div>



      <div class="modal fade pop_address" id="place_order_sed" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

        <div class="modal-dialog modal-dialog-centered" role="document">

          <div class="modal-content">        

            <div class="modal-body">

              <div class="address-form">



                <form  id="createShipAddress" action="#" enctype="multipart/form-data">



                  <input type="hidden" name="user_id" value="{{!empty(@Auth::user()->id) ? @Auth::user()->id:''}}">



                  <div class="top-bar-container">

                    <div class="top-bar">

                      <div class="left-comp">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                          <span aria-hidden="true">&times; <span class="cls_btn">Close</span></span>

                        </button>        

                      </div>

                      <div class="title_fgh">Add Address</div>

                      <div class="title_fgh">

                      <button type="button" class="btn btn-default Sel_add" data-toggle="modal" data-target="#showAddressBooknew">Select From Address Book</button>

                      <div class="right-comp">

                        <input type="submit" class="DoneButton" name="Sbmtform" value="Done">

                        <!-- <a href="#" class="DoneButton">Done</a> -->

                      </div>

                    </div>

                    <div id="signupResBox"></div>

                    <div class="bottom-comp"></div>

                  </div>



                  <div class="ioc_emoh">

                    <img class="cloud-icon" src="{{url('/').'/resources/front_assets'}}/img/icon_home.png">

                  </div>



                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">Address Type</div>

                    <input class="FormInput newAddressFormStyle" type="text" id="address_type" name="address_type" placeholder="Address Type" value="">

                  </div>



                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">Full Name</div>

                    <input class="FormInput newAddressFormStyle" type="text" id="fullname" name="fullname" placeholder="Full Name" value="">

                  </div>



                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">Street Address</div>

                    <input class="FormInput newAddressFormStyle" type="text" id="street_address" name="street_address" placeholder="Street Address"  value="">

                  </div>           



                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">Zip Code</div>

                    <input class="FormInput newAddressFormStyle" type="text" id="zip_code" name="zip_code" placeholder="Zip Code" value="">

                  </div>



                  <div class="newAddressInputWrapper country-select-container">

                    <div class="InputLabel">Country</div>

                      <select class="InputLabel country_list" name="country">

                        <option value="">Select Country</option>

                      </select>

                  </div>



                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">State / Province</div>

                    <input class="FormInput newAddressFormStyle" type="text" id="state" name="state" placeholder="State" value="">

                  </div>



                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">City</div>

                    <input class="FormInput newAddressFormStyle" type="text" id="city" name="city" placeholder="City" value="">

                  </div>



                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">Phone Number</div>

                    <input class="FormInput newAddressFormStyle" type="text" id="contact_number" name="contact_number" placeholder="Contact Number" value="">

                  </div> 



                </form>



              </div>     

            </div>        

          </div>

        </div>

      </div>

  <!-- Go to www.addthis.com/dashboard to customize your tools -->
<!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f322945f84fb08f"></script> -->


 @endsection





