@extends('user.layout.layout')
@section('title', 'User - Profile')

@section('current_page_css')

<style type="text/css">

.bx_mig a {
    box-shadow: none;
    background: transparent;
    display: inline-flex;
    padding: 0px;
    background-size: 90%;
    background-position-x: 8px;
    background-position-y: 8px;
}

</style>

@endsection

@section('current_page_js')

<?php  $user_id=Auth::user()->id; ?>

<script type="text/javascript">

  function save_frame_order() {

    var user_id = "<?php echo $user_id ?>";

    $.ajax({

      type: "GET",

      dataType: "json",

      url: "<?php echo url('/save_frame_order'); ?>",

      data: {'user_id':user_id},

      success: function(data){ 

        if(data.status=='success'){ 

        //alert(data.msg);	

        window.location.href = "<?php echo url('/select_frame'); ?>"; 

        }else{

           alert(data.msg);

        }

      }

    });

  }



  $.ajaxSetup({
    headers: {'votive':'123456'}
  });
  
  var user_id = "<?php echo $user_id ?>";
  $(document).ready(function(){
    var formData = new FormData();
    formData.append('user_id',user_id);
    //alert(id);
    $.ajax({
      type: 'POST',
      //beforeSend: function(xhr){xhr.setRequestHeader('votive':'123456');},
      url: "<?php echo url('/').'/api/getuserdetails'; ?>",
      data: formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData){ 
          //console.log(resultData);
          if(resultData.status){
          $("#first_name").val(resultData.response.first_name);
          $("#last_name").val(resultData.response.last_name);
          //$("#email").val(resultData.response.email);
          $("#contact_number").val(resultData.response.contact_number);

          if(resultData.response.date_of_birth){
            $("#date_of_birth").val(resultData.response.date_of_birth);
          }else{
            $("#date_of_birth").val();
          }

          if(resultData.response.gender){
            $("#gender").val(resultData.response.gender);
          }else{
            $("#gender").val();
          }

          if(resultData.response.address){
            $("#address").val(resultData.response.address);
          }else{
            $("#address").val();
          }

         if(resultData.response.profile_pic){          

              $("#blah").attr("src",resultData.response.profile_pic);
          }else{
            $("#blah").attr("src","http://localhost/frameit/resources/front_assets/img/user_image.jpg");
          }

          $("#err_msg").html(result_alert);


              }

           },error: function(errorData) { 
              console.log(errorData);
              var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
              
              $("#err_msg").html(result_alert);
            }
          });
    });
   

    $('#editProfile_form').validate({ 
      // initialize the plugin
      rules: {
       first_name: {
        required: true
      },
      last_name: {
        required: true
      },
      gender:{
        required: true,
      },
      date_of_birth:{
        required: true,
      },
      address:{
        required: true,
      },
      contact_number: {
        required: true,
        digits:true,
        minlength : 8,
        maxlength : 13
      },
    },
    submitHandler: function(form) {
         //form.submit();
         update_profile();
       }
    });

    $.ajaxSetup({
      headers: {'votive':'123456'}
    });

    function update_profile() {
      var $this = $('form#editProfile_form')[0];
      var formData = new FormData($this);
      //var user_id = "<?php //echo $user_id ?>";
      //formData.append('user_id',user_id);
      console.log(formData);
      $.ajax({
        type: 'POST',
        url: "<?php echo url('/').'/api/updateprofile'; ?>",
        data: formData,
          //dataType: "text",
          enctype: 'multipart/form-data',
          cache:false,
          contentType: false,
          processData: false,
          success: function(resultData) { 
            console.log(resultData);
            if (resultData.status) {
              var result_alert = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> '+resultData.msg+'</div>';
              $("#signupResBox").html(result_alert);
              window.location.href = "<?php echo url('/edit-profile'); ?>";
              //document.getElementById("signup_form").reset();
            } else {
              var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
              
              $("#signupResBox").html(result_alert);
            }
          },
          error: function(errorData) { 
            //console.log(errorData);
            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
            
            $("#signupResBox").html(result_alert);
          }
        });
    }

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }

    $("#imgInp").change(function() {
      readURL(this);

     var file = this.files[0];
        var fileType = file.type;
        var match = ['image/jpeg', 'image/png', 'image/jpg'];
        if(!((fileType == match[0]) || (fileType == match[1]) || (fileType == match[2]))){
            alert('Sorry, only JPG, JPEG, & PNG files are allowed to upload.');
            $("#imgInp").val('');
            return false;
        }
  });
  
  $( function() {
    //$( "#date_of_birth" ).datepicker();
    var date = $('#date_of_birth').datepicker({ dateFormat: 'yy-mm-dd' }).val();
  } );

</script>



<script type="text/javascript">
  //start get user order list
  $.ajaxSetup({
    headers: {'votive':'123456'}
  });

  var user_id = "<?php echo $user_id ?>";
  
  $(document).ready(function(){
    var formData = new FormData();
    formData.append('user_id',user_id);
    $.ajax({
        type: 'POST',
        url: "<?php echo url('/').'/api/getAllUserOrderList'; ?>",
        data: formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData){ 
          //console.log(resultData);
          if(resultData.status){

            var orderArray = [];
            var j=1;
            $.each(resultData.response.orderList, function( i, l ){
              var order_id = resultData.response.orderList[i].order_id;
              var site_url = "<?php echo url('/').'/order_details'; ?>";
              //var orders = '<tr><td>'+j+'</td><td>'+resultData.response.orderList[i].order_id+'</td><td>'+resultData.response.orderList[i].frame_name+'</td><td>'+resultData.response.orderList[i].total_tile+'</td><td>'+resultData.response.orderList[i].total_price+' USD</td><td>'+resultData.response.orderList[i].tracking_code+'</td><td>'+resultData.response.orderList[i].order_status+'</td><td>'+resultData.response.orderList[i].delivery_date+'</td><td>'+resultData.response.orderList[i].created_at+'</td><td><a href="'+site_url+'/'+order_id+'">View Details</a></td></tr>';


              var orders = '<div class="edit_pr_section_one"><div class="col-md-2"><div class="section_one_nn"><p> Order id:</p><strong>'+resultData.response.orderList[i].order_id+'</strong></div></div><div class="col-md-2"><div class="section_one_nn"><p>Placed one</p><strong>'+resultData.response.orderList[i].created_at+'</strong></div></div><div class="col-md-2"><div class="section_one_nn"><p> Items</p><strong>'+resultData.response.orderList[i].total_tile+' Frames '+resultData.response.orderList[i].frame_name+' Frame Style</strong></div></div><div class="col-md-2"><div class="section_one_nn"><p> Paid on</p><strong>'+resultData.response.orderList[i].created_at+'</strong></div></div><div class="col-md-2"><div class="section_one_nn"><p> Total</p><strong>USD '+resultData.response.orderList[i].total_price+' </strong></div></div><div class="col-md-2"><div class="section_one_nn manage"><a href="#" class="mang_ac ">Manage</a><a href="'+site_url+'/'+order_id+'">Details</a></div></div></div>';

              orderArray.push(orders);
              j++;
            });
            //$(".orderList").html(orderArray);

          }else{

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> No order Found.</div>';
            $("#err_msg").html(result_alert);

          }

        },error: function(errorData) { 

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
            $("#err_msg").html(result_alert);

        }
    });
  });

  //end get user order list
  </script>


@endsection

@section('tag_body')
  <body>
@endsection

@section('content')
  <section id="" class="section bx_mainCrop">
    <div class="selectFrameSec"></div>

 <div id="my_profile_sec" class=" area-padding">
  <div class="container">
  <div class="inner_secPro">

    <div class="row  wow slideInUp" data-wow-delay=".1s">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="section-headline text-center">
         <!--  <h2><span>Edit</span> Profile</h2> -->

           <div class="inn_h_secc">
           <a href="{{url('/')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Back to homepage</a>
           <h3>Save Order</h3>
           </div>

        </div>
      </div>
    </div>
    <div class="col-lg-12" role="tabpanel">
      <div class="row manag-tabs">
       
        <div class="col-sm-2">
          <ul class="nav nav-pills brand-pills nav-stacked" role="tablist">
            <?php

            $currentPath= Route::getFacadeRoot()->current()->uri(); 

            if($currentPath=='my_order'){ $classAct='active'; }else{ $classAct=''; } 

            ?>
            <!-- <li class="brand-nav "><a href="#" aria-controls="tab1" role="tab" data-toggle="tab"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a></li> -->
            <li class="brand-nav"><a href="{{url('/profile')}}" aria-controls="tab2" role="tab" ><i class="fa fa-user" aria-hidden="true"></i> My Profile</a></li>
            <li class="brand-nav "><a href="{{url('/my_order')}}" aria-controls="tab3" role="tab" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i> My Order </a></li>

            <li class="brand-nav <?php echo $classAct; ?>"><a href="{{url('/save_frame_list')}}" aria-controls="tab3" role="tab" ><i class="fa fa-shopping-cart" aria-hidden="true"></i> Save Order </a></li>
<!-- 
             <li class="brand-nav"><a href="{{url('/my-rewards')}}" aria-controls="tab4" role="tab"><i class="fa fa-sign-out" aria-hidden="true"></i> My Rewards</a></li> -->
          
          </ul>
        </div>
     
        <div class="col-sm-10">
          <div class="tab-content">
          
            <div role="tabpanel" class="tab-pane active" id="tab3">

              <div class="profile-basic background-white p20 ed_p">
              
               <div class="edit_pro">
                      

          <div class="edit_pr_section orderList">

                <?php if(!empty($tmpImgData)){ ?>

                    <div class="edit_pr_section orderList">

                    @foreach($tmpImgData as $ord)

                      <div class="edit_pr_section_one">
                      <div class="col-md-2">
                      <div class="section_one_nn">
                      <p> WishList id:</p>
                      <strong>{{$ord->id}}</strong>
                      </div></div>

                      <div class="col-md-2">
                      <div class="section_one_nn">
                      <p>Date on</p>
                      <strong>{{$ord->created_at}}</strong>
                      </div></div>

                      <div class="col-md-2">
                      <div class="section_one_nn">
                      <p>Items</p>
                      <strong>{{$ord->total_tile}} Frames {{$ord->title}} Frame Style</strong>
                      </div></div>

                      <div class="col-md-2">
                      <div class="section_one_nn">
                      <p>Paid on</p>
                      <strong>{{$ord->created_at}}</strong>
                      </div></div>

                      <div class="col-md-2">
                      <div class="section_one_nn">
                      <p> Total</p>
                      <strong>USD {{$ord->total_price}}</strong>
                      </div></div>

                      <div class="col-md-2">
                      <div class="section_one_nn">
                      <a href="{{url('/save_frame_order')}}/{{$ord->id}}" class="mang_ac{{$ord->id}}">Order</a>
                      <a href="{{url('/save_frame_detail')}}/{{$ord->id}}">Details</a>
                      </div></div>

                      </div>

                    @endforeach


                </div>
               
                  

				          <?php } ?>

		<!--  <div class="section_one_nn"><a href="#" class="mang_ac205" onclick="save_frame_order();">Order</a></div>		 --> 

</div>

</div>
</div>
</div>                
</div>
</div>
</div>
</div>
</div>
</div>
</div>

</section>


@endsection

@section('page_footer')
  
@endsection