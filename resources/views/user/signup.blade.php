<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  }
* {
  box-sizing: border-box;
  }
.container {
  padding:16px;
  background-color:white;
  width:100%;
  border:6px solid black;
  max-width:650px; 
  margin:0px auto;
  }
input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
  }
input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
  }
hr{
  border:1px solid #f1f1f1;
  margin-bottom:25px;
  }
.registerbtn {
  background-color:#4CAF50;
  color:white;
  padding:16px 20px;
  margin:8px 0;
  border:none;
  cursor:pointer;
  width:100%;
  opacity:0.9;
  }
.registerbtn:hover {
  opacity: 1;
  }
a {
  color: dodgerblue;
  }
.signin {
  background-color:#f1f1f1;
  text-align:center;
  }
  button.btn_Submit {
    background: #faaa1b;
    border: 0px;
    padding: 10px 25px;
    display: inline-block;
    color: #fff;
    font-weight: 500;
    font-size: 16px;
    border-radius: 2px;
    cursor: pointer;
  }
</style>

</head>
<body>
  <form class="sign_form" id="signup_form">
    <div class="container">
      <h1>Register</h1>
      <p>Please fill in this form to create an account.</p>
      <div id="signupResBox"></div>
      <hr>
      @csrf
      <label for="email"><b>Name</b></label>
      <input type="text" placeholder="Enter Name" name="name" id="name" required>
      <label for="email"><b>Email</b></label>
      <input type="text" placeholder="Enter Email" name="email" id="email" required>
      <label for="psw"><b>Password</b></label>
      <input type="password" placeholder="Enter Password" name="password" id="password" required>
      <label for="psw-repeat"><b>Repeat Password</b></label>
      <input type="password" placeholder="Confirm Password" name="confirm_password" id="confirm_password" required>
      <label for="email"><b>Phone Number</b></label>
      <input type="text" placeholder="Phone Number" name="contact_number" id="contact_number" required>
      <input type="hidden" name="my_referral_code" value="{{ Request::segment(2) }}" id="my_referral_code" readonly="">
      <input type="hidden"  name="user_referral_id" value="{{ $user->id }}" id="user_referral_id" readonly="">
      <hr>
      <button class="btn_Submit" type="submit" >Register</button>
    </div>
  </form>
<script src="{{url('/').'/resources/front_assets'}}/lib/jquery/jquery.min.js"></script>
<script src="{{url('/').'/resources/front_assets'}}/lib/jquery/jquery-migrate.min.js"></script>
<script src="{{url('/').'/resources/front_assets'}}/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="{{url('/').'/resources/front_assets'}}/lib/superfish/hoverIntent.js"></script>
<script src="{{url('/').'/resources/front_assets'}}/lib/superfish/superfish.min.js"></script>
<script src="{{url('/').'/resources/front_assets'}}/lib/easing/easing.min.js"></script>
<script src="{{url('/').'/resources/front_assets'}}/lib/modal-video/js/modal-video.js"></script>
<script src="{{url('/').'/resources/front_assets'}}/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{url('/').'/resources/front_assets'}}/js/main.js"></script>
<script src='https://cdn.jsdelivr.net/jquery.slick/1.5.9/slick.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>-->
<!-- Croppie js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="{{ url('resources/front_assets/js/jquery.validate.min.js') }}"></script>
<script src="{{ url('resources/front_assets/js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
  $('#signup_form').validate({ 
      // initialize the plugin

      rules: {

       name: {

        required: true

      },

      email: {

        required: true,

        email: true

      },

      password: {

        required: true,

        minlength : 6

      },

      confirm_password: {

        required: true,

        equalTo : "#password"

      },

      contact_number: {

        required: true,

        digits:true,

        minlength : 8,

        maxlength : 13

      },

    },

    submitHandler: function(form) {

         //form.submit();

         register();

       }

     });



  $.ajaxSetup({

    headers: {'votive':'123456'}

  });



  function register() {

    var $this = $('form#signup_form')[0];

    var formData = new FormData($this);

    $.ajax({

      type: 'POST',

      url: "<?php echo url('/').'/api/user_register'; ?>",

      data: formData,

        //dataType: "text",

        enctype: 'multipart/form-data',

        cache:false,

        contentType: false,

        processData: false,

        success: function(resultData) { 

          console.log(resultData);

          if (resultData.status) {

            var result_alert = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> '+resultData.msg+'</div>';

            $("#signupResBox").html(result_alert);

            document.getElementById("signup_form").reset();

          } else {

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';

            

            $("#signupResBox").html(result_alert);

          }

        },
        error: function(errorData) { 
          console.log(errorData);
          var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
          $("#signupResBox").html(result_alert);
        }
      });
  }
</script>
</body>
</html>

