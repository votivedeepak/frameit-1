@extends('user.layout.layout')
@section('title', 'User - Profile')

@section('current_page_css')
@endsection

@section('current_page_js')
@endsection

@section('tag_body')
  <body class="step_main">
@endsection

@section('content')
	<section id="" class="section selectFramStyles">
    <div class="container">
      <div class="selectFrameSec">
        <h2>1/3</h2>        
      </div>
      <div class="stp_one">
        <div class="content">
          <div class="bg-layer">
            <div class="bg-circle"></div>
          </div>
          <div class="step-1-wrapper visible">
            <h2>Who's it for?</h2>
            <div class="options">
              <a href="{{url('/login_two')}}" style="margin: auto;">
                <div class="option" style="margin: auto;">
                  <img src="{{url('/').'/resources/front_assets'}}/img/photos.svg">
                  <div class="msub">
                    <h3>For myself</h3><p>I'm decorating my walls</p>
                  </div>
                </div>
              </a>
              <!-- <div class="option">
                <img src="{{url('/').'/resources/front_assets'}}/img/gift.svg">
                <div class="msub">
                  <h3>For someone else</h3>
                  <p>I'm buying a gift</p>
                </div>
              </div> -->
            </div>
          </div>
        </div>
      </div>

      <div class="framesTabing">
        <div class="sectionWraper"></div>
      </div>
  </div>
  </section> 
@endsection

@section('page_footer')
	
@endsection