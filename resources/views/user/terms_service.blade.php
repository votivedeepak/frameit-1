@extends('user.layout.layout')
@section('title', 'User - Privacy Policy')
<?php $segment1 =  Request::segment(1);  ?>
@section('current_page_css')
@if($segment1 == 'terms_and_condition_mobile')
<style type="text/css">
  header#header {
    display: none;
}
</style>
@endif
@endsection

@section('current_page_js')

<script type="text/javascript">
  //start get faq list
  $.ajaxSetup({
    headers: {'votive':'123456'}
  });
  
  $(document).ready(function(){
    var formData = new FormData();
    $.ajax({
        type: 'POST',
        url: "<?php echo url('/').'/api/getAllFaqList'; ?>",
        data: formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData){ 
          console.log(resultData);
          if(resultData.status){

            var faqArray = [];
            var j=1;
            $.each(resultData.response.faqList, function( i, l ){

              var faqs = '<div class="card"><div class="card-header bg-warning text-white"><div class="card-link" data-toggle="collapse" href="#collapse'+resultData.response.faqList[i].id+'">'+resultData.response.faqList[i].question+'</div></div><div id="collapse'+resultData.response.faqList[i].id+'" class="collapse" data-parent="#accordion"><div class="card-body">'+resultData.response.faqList[i].answer+'</div></div></div><br/>';

              faqArray.push(faqs);
              j++;
            });
            $(".faqList").html(faqArray);

          }else{

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> No Faq List Found.</div>';
            $("#err_msg").html(result_alert);

          }

        },error: function(errorData) { 

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
            $("#err_msg").html(result_alert);

        }
    });
  });
  //end get faq list
</script>
@endsection

@section('tag_body')
<body>
@endsection

@section('content')

<!--==========================-->
<section id="home" class="section bx_mainCrop">
    <div id="my_profile_sec" class=" area-padding" style="padding-top:30px;">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="section-headline text-center" style="padding-top:30px;">
              <h2><span>{{ $terms_of_service[0]->page_name }}</span></h2>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container" style="padding-top:30px;"> 
      <div class="col-lg-12">
        {!! $terms_of_service[0]->description !!}
     </div>
  </div>
</section>
<!--============================-->
</body>
@endsection
