               <?php $tempimgid = '';?>

                <?php if(!empty($tmpImgData)){ ?>

                  <?php foreach ($tmpImgData as  $value) { ?>

                    <?php $tempimgid = $value->id; ?>

                    <div class='col-3' id="row{{$value->id}}" style="margin: auto;">

                      <div class='bx_mig'>

                         <a  href="javascript:void(0);" class="img_bbb show_modal" data-toggle="modal" data-target="#showModal" data-src="{{url('/')}}/public/uploads/order_images/{{$value->filename}}" data-id="<?php echo $value->id ?>" >
                              
                        <?php if($value->alignment=="vertical" && $value->type=="square"){ ?>
                          @if($value->rotation=='0' && ($value->frame_id=='5' || $value->frame_id=='7'))
                             <?php if(!empty($value->thumb_filename)){ ?>
                                  <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;height: 270px;width: 270px;" >
                                <img id="upimages"  src="{{url('/')}}/public/uploads/order_images/{{$value->thumb_filename}}"  class="up_images" style="display: block;margin-left: -51.5%;margin-right: auto !important;min-width: 221px;width: 265px;object-fit: cover;height: 262px;"> 
                              <?php }else{ ?>

                                     <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;height: 270px;width: 270px;" >
                                      <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->filename}}"  class="up_images" style="display: block;margin-left: -51.5%;margin-right: auto !important;min-width: 221px;width: 265px;object-fit: cover;height: 262px;"> 
                              <?php } ?>
                              @elseif($value->rotation=='90' && ($value->frame_id=='5' || $value->frame_id=='7'))
                              <?php if(!empty($value->thumb_filename)){ ?>
                              <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;height: 270px;width: 270px;">
                              <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->thumb_filename}}"  class="up_images" style="display: block;margin-left: -50.2%;margin-right: auto !important;min-width: 221px;width: 262px;object-fit: cover;height: 264px;">
                              <?php } else { ?>
                              <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;height: 270px;width: 270px;">
                              <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->filename}}"  class="up_images" style="display: block;margin-left: -50.2%;margin-right: auto !important;min-width: 221px;width: 262px;object-fit: cover;height: 264px;">
                              <?php } ?>
                             @endif

                             @if($value->rotation=='0' && ($value->frame_id=='3' || $value->frame_id=='6'))
                             <?php if(!empty($value->thumb_filename)){ ?>
                                  <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;height: 270px;width: 270px;" >
                                <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->thumb_filename}}"  class="up_images" style="display: block;margin-left: -50%;margin-right: auto !important;min-width: 221px;width: 260px;object-fit: cover;height: 260px;"> 
                              <?php }else{ ?>

                                     <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;height: 270px;width: 270px;" >
                                      <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->filename}}"  class="up_images" style="display: block;margin-left: -50%;margin-right: auto !important;min-width: 221px;width: 260px;object-fit: cover;height: 260px;"> 
                              <?php } ?>
                              @elseif($value->rotation=='90' && ($value->frame_id=='3' || $value->frame_id=='6'))
                              <?php if(!empty($value->thumb_filename)){ ?>
                              <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;height: 270px;width: 270px;">
                              <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->thumb_filename}}"  class="up_images" style="display: block;margin-left: -50%;margin-right: auto !important;min-width: 221px;width: 260px;object-fit: cover;height: 260px;">
                              <?php } else { ?>
                              <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;height: 270px;width: 270px;">
                              <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->filename}}"  class="up_images" style="display: block;margin-left: -51%;margin-right: auto !important;min-width: 221px;width: 260px;object-fit: cover;height: 260px;">
                              <?php } ?>
                             @endif

                        <?php }?>


                            <?php if($value->alignment=="vertical" && $value->type=="rectangle"){ ?>
                          @if($value->rotation=='0')
                             <?php if(!empty($value->thumb_filename)){ ?>
                                  <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;margin-left: auto !important;margin-right: auto !important;height: 290px;" >
                                <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->thumb_filename}}"  class="up_images" style="display: block;margin-left: -100%;margin-right: auto !important;min-width: 273px;width: 200px;object-fit: cover;height: 285px;"> 
                              <?php }else{ ?>

                                     <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;margin-left: auto !important;margin-right: auto !important;height: 290px;" >
                                <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->filename}}"  class="up_images" style="display: block;margin-left: -100%;margin-right: auto !important;min-width: 273px;width: 200px;object-fit: cover;height: 285px;"> 
                              <?php } ?>
                              @else
                              <?php if(!empty($value->thumb_filename)){ ?>
                              <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;margin-left: 15% !important;margin-right: auto !important;width: 66%;height: 290px;">
                              <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->thumb_filename}}"  class="up_images" style="display: block;margin-left: -80%;margin-top: 20%;height: 182px;width: 275px;">
                              <?php } else { ?>
                              <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(<?php echo $value->rotation.'deg';?>);display: block;margin-left: 15% !important;margin-right: auto !important;width: 66%;height: 290px;">
                              <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->filename}}"  class="up_images" style="display: block;margin-left: -80%;margin-top: 20%;height: 182px;width: 275px;">
                              <?php } ?>
                             @endif

                        <?php }?>


                        <?php if($value->alignment=="horizontal"){ ?>
                          @if($value->rotation=='90')
                             <?php if(!empty($value->thumb_filename)){ ?>
                                  <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform:rotate(0deg);display: block;margin-left: auto !important;margin-right: auto !important;height: 288px;width: 200px;" >
                                <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->thumb_filename}}"  class="up_images" style="display: block;margin-left: -91%;margin-right: auto !important;width: 81% !important;"> 
                            <?php } else { ?> 
                                 <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(0deg);display: block;margin-left: auto !important;margin-right: auto !important;height: 288px;width: 200px;" >
                                <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->filename}}"  class="up_images" style="display: block;margin-left: -91%;margin-right: auto !important;width: 81% !important;"> 
                            <?php } ?> 
                              @else
                              <?php if(!empty($value->thumb_filename)){ ?>
                              <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(90deg);display: block;margin-left: 15% !important;margin-right: auto !important;width: 66%;height: 290px;">
                              <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->thumb_filename}}"  class="up_images" style="display: block;margin-left: -80%;margin-top: 20%;height: 182px;width: 275px;">
                  <?php } else { ?>
                      <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$value->images}}" style="transform: rotate(90deg);display: block;margin-left: 15% !important;margin-right: auto !important;width: 66%;height: 290px;">
                              <img  id="upimages" src="{{url('/')}}/public/uploads/order_images/{{$value->filename}}"  class="up_images" style="display: block;margin-left: -80%;margin-top: 20%;height: 182px;width: 275px;">
                  <?php } ?>
                             @endif

                        <?php }?>

                        </a> 

                      </div>

                      <div class="number">
                        <span class="numbersBox1">
                          <center><label><b>Face Count</b></label>
                          <input type="number" name="face_count" id="face_count" value="1" class="form-control" style="width: 50%;"></center>
                        </span>
                      </div>

                    </div>

                  <!--   <div id="fb-root"></div> -->
              

                  <?php } ?>

                <?php }  ?>