@extends('user.layout.layout')
@section('title', 'User - Privacy Policy')
<?php $segment1 =  Request::segment(1);  ?>
@section('current_page_css')
@if($segment1 == 'terms_and_condition_mobile')
<style type="text/css">
  header#header {
    display: none;
}
</style>
@endif
@endsection

@section('current_page_js')

<script type="text/javascript">
  //start get faq list
  $.ajaxSetup({
    headers: {'votive':'123456'}
  });
  
  $(document).ready(function(){
    var formData = new FormData();
    $.ajax({
        type: 'POST',
        url: "<?php echo url('/').'/api/getAllFaqList'; ?>",
        data: formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData){ 
          console.log(resultData);
          if(resultData.status){

            var faqArray = [];
            var j=1;
            $.each(resultData.response.faqList, function( i, l ){

              var faqs = '<div class="card"><div class="card-header bg-warning text-white"><div class="card-link" data-toggle="collapse" href="#collapse'+resultData.response.faqList[i].id+'">'+resultData.response.faqList[i].question+'</div></div><div id="collapse'+resultData.response.faqList[i].id+'" class="collapse" data-parent="#accordion"><div class="card-body">'+resultData.response.faqList[i].answer+'</div></div></div><br/>';

              faqArray.push(faqs);
              j++;
            });
            $(".faqList").html(faqArray);

          }else{

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> No Faq List Found.</div>';
            $("#err_msg").html(result_alert);

          }

        },error: function(errorData) { 

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
            $("#err_msg").html(result_alert);

        }
    });
  });
  //end get faq list
</script>
@endsection

@section('tag_body')
<body>
@endsection

@section('content')

<!--==========================-->
<section id="home" class="section bx_mainCrop">
    <div id="my_profile_sec" class=" area-padding" style="padding-top:30px;">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="section-headline text-center" style="padding-top:30px;">
              <h2><span>Terms And Conditions</span></h2>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container"> 
      <div class="col-lg-12">
        <div class="mt-4"><h3>FRAMEIT Terms of Service</h3></div>
        <div>
            <p>
              Here’s an overview of our terms. You can read our complete legal information below, but these are a few of the most important bits, in plain English.
            </p>
            <ul>
              <li>You still own all the pictures you order from us. You keep the copyright to all your images. We won’t sell them, share them, or use them for anything other than getting your tiles printed and delivered to you.
              </li>
              <li>We need access to your photo gallery to show you your pictures in the FRAMEIT app. When you open our app for the first time, it’ll ask you for permission to access your photos. We need that so we can let you select, crop, and order your pics in our app. If you want, you can turn that permission off in your settings later, or delete the app to remove the permission.
              </li>
              <li>If you want, we’ll delete all your pictures and personal information from our systems. All you need to do is send us an email to <a href="">hi@frameit.com.my</a> from the email address you entered when you made your order.
              </li>
              <li>You agree not to order any pictures you don’t own, or any pornography or discriminatory content. There are lots of people involved in the printing process, so we can’t print any images with nudity or offensive content. We also can’t print pictures you don’t own the copyright to.
              </li>
              <li>If there’s a problem with your tiles, we’ll give you a refund or replace them for you. If there are any issues, just send us an email to <a href="">hi@frameit.com.my</a> or a chat through the app. We’ll get it all sorted out.
              </li>
            </ul>
            <p>
            Frame Mate Sdn. Bhd.. (as defined below) allows users to upload images and/or pictures ("Images") to our website ("Site") or our mobile application (together with the Site, "App") in order to print those Images on a wall tile ("Tile(s)"). If you are a user, you are contracting with Frame Mate Sdn. Bhd.. (collectively "FRAMEIT", "us", "our", or "we"). These Terms of Service ("Terms") govern your access to and use of the App and services available through the App ("Services"). Our Privacy Notice, available at https://frameit.com.my/privacy-notice governs our collection, processing and transfer of any Personal Data (as such term is defined in the Privacy Notice). "You" means any adult user of the App or Services or any parent or guardian of any minor whom you allow to use the App or the Services, and for whom you will be held strictly responsible.
            </p>
            <p>
              Please read these Terms carefully. By clicking on the button marked "I agree" you signify your assent to these Terms. Changes may be made to these Terms from time to time. If you do not agree to any of these Terms, please do not click the button marked "I agree", and do not use the App or Services.
            </p>
            <h3>Use of the Services</h3>
            <p>
              Use of the Services and access to the App is void where prohibited. By using the Services, you represent and warrant that (a) all information you submit is truthful and accurate; (b) you are 18 years of age or older; (c) have your parent's or guardian's permission to enter into these Terms, and have the ability to form a binding contract; (d) your use of the App or Services does not violate any applicable law or regulation or any obligation you may have to a third party; (e) your use of the App and Services shall be in compliance with all applicable laws and regulations; (f) you are solely responsible for obtaining the consent of the parent or guardian ("Parental Consent") before transferring us any information, including images, of any children.

            </p>
            <p>
              Certain categories of Personal Data are subject to special protections under Regulation 2016/679 of the European Parliament and of the Council of 27 April 2016 (General Data Protection Regulation, hereafter "GDPR"). These include Personal Data relating to among others, a person's racial origin, religious beliefs, health, sexual orientation, listed in Article 9 of the GDPR ("Special Categories of Data") and Personal Data related to children (together with Special Categories of Data, "Sensitive Data"). You hereby represent and warrant that you will not add any Images that contain Sensitive Data unless you have obtained explicit consent if and as required under applicable law from the relevant individual (including Parental Consent if and as required) and that you shall be solely responsible and liable for any such Sensitive Data, including in the event of any data breach relating to the Services.
            </p>
            <p>
              You may use the App and the Services in accordance with and subject to these Terms. FRAMEIT may, at its sole discretion and at any time, modify or discontinue, temporarily or permanently, any part of the App or the Services without notice. FRAMEIT shall not be liable to you or to any third party for any modification, suspension, or discontinuance of the Services.
            </p>
            <p>
              In order to use the App, you will be required to provide certain information as may be requested by FRAMEIT.
            </p>
            <p>
              You may have the option to link the App with certain third-party services, as may be listed in the App from time to time, such as your Facebook, Google Photos, or Instagram account ("Linked Account"). By doing so, you represent and warrant that you have the authority to provide FRAMEIT with access to account information related to such Linked Account, and to provide FRAMEIT with access to photos or pictures saved in such Linked Account.
              You are fully and solely responsible for the security of your computer system and/or mobile phone and/or other device and all of your activity on the App. We do not police for and cannot guarantee that we will learn of or prevent any inappropriate use of the App.
            </p>
            <p>
              FRAMEIT may, for any reason, at its sole discretion and without notice, discontinue or refuse to provide you with the App or Services. Grounds for such refusal may include but are not limited to (i) violation of the letter or spirit of these Terms, (ii) fraudulent, harassing or abusive behavior, (iii) behavior that is illegal or harmful to other users, third parties, or the business interests of FRAMEIT,
            </p>
             
            <ul>
            <li>breach of your representations and warranties herein or breach of any of the terms of these Terms, or (v) use of your chargeback rights with your credit card company or denial or dispute of any preapproval obtained by FRAMEIT from your credit card company.
            </li>
            </ul>
            <p>
              If we believe, at our sole discretion, that a violation of these Terms or any illegal or inappropriate behavior has occurred, we may take any other corrective action we deem appropriate. We reserve the right to investigate suspected violations of these Terms or illegal and inappropriate behavior on the App. We will fully cooperate with any law enforcement investigation or court order requesting or directing us to disclose the identity, behavior or (User) Content (as defined below) of anyone believed to have violated these Terms or to have engaged in illegal behavior in connection with the App and/or the Services.
            </p>
            <p>
              Any suspension, termination, or cancellation of Services shall not affect your obligations to FRAMEIT under these Terms (including but not limited to ownership, indemnification, limitation of liability and payment obligation), nor will it affect any other terms herein, which by their sense and context are intended to survive such suspension, termination, or cancellation.
            </p>

            <b>Content and User Content</b>
            <p>Certain types of content may be made available through the App or the Services. "Content" as used in these Terms means, collectively, all content on or made available on the App or through the Services, including any images, photos, pictures, and any modifications or derivatives of the foregoing.
            <p>
             The App may allow you to upload certain content including but not limited to Images and photos. All content uploaded by you is referred to as "User Content." User Content may include Sensitive Data. You are and shall remain at all times fully and solely responsible for any User Content you upload to the App. YOU REPRESENT AND WARRANT THAT ANY SUCH USER CONTENT COMPLIES WITH ALL APPLICABLE LAW, THAT YOU HAVE ALL NECESSARY RIGHTS AND AUTHORITIES TO SUBMIT ANY SUCH USER CONTENT THROUGH THE APP AND SERVICES, AND THAT NO SUCH USER CONTENT INFRINGES OR VIOLATES ANY THIRD PARTY INTELLECTUAL PROPERTY RIGHTS, PRIVACY OR PUBLICITY RIGHTS, OR MORAL RIGHTS. YOU FURTHER REPRESENT AND WARRANT THAT YOU HAVE OBTAINED ALL RIGHTS AS REQUIRED UNDER APPLICABLE LAW, TO PROVIDE FRAMEIT WITH SENSITIVE DATA FOR THE PURPOSE OF PROCESSING SUCH SENSITIVE DATA IN ACCORDANCE WITH THESE TERMS AND OUR PRIVACY NOTICE AND THAT YOU HAVE OBTAINED CONSENT, IN ACCORDANCE WITH APPLICABLE LAW, FROM ANY DATA SUBJECT OR PARENT OR GUARDIAN OF DATA SUBJECT, AS APPLICABLE, WHOSE DATA, INCLUDING SENSITIVE DATA, IS SUBMITTED THROUGH THE APP AND SERVICES. FRAMEIT has noobligation to accept any request to print or maintain any User Content. Moreover, FRAMEIT reserves the right to remove and permanently delete any User Content uploaded by you, without notice and for any reason. WE DO NOT ENDORSE ANY CONTENT (INCLUDING WITHOUT LIMITATION ANY USER CONTENT) OR ANY OPINION, RECOMMENDATION, OR ADVICE EXPRESSED IN ANY CONTENT, AND WE EXPRESSLY DISCLAIM ANY AND ALL LIABILITY IN CONNECTION WITH CONTENT.

            </p>
            <p>  
             WE DISCLAIM ALL LIABILITY, REGARDLESS OF THE FORM OF ACTION, FOR THE ACTS OR OMISSIONS OF USERS (INCLUDING UNAUTHORIZED USERS), WHETHER SUCH ACTS OR OMISSIONS OCCUR DURING THE USE OF THE SERVICE OR OTHERWISE.
             FRAMEIT may retain the User Content, including Images (including in your account as your order history) as long as your account is valid and has not been terminated by you or by us, for any reason. You may ask us to delete any User Content, including Images, by sending us an email, specifying your request, as detailed in our Privacy Notice. Any Images uploaded by you to the Services and not elected by you to be printed on Tiles will be automatically deleted after thirty (30) days from the day you have uploaded the Image(s).
            </p>
            <h3>User Content Restrictions</h3>
            <p>
              Without limiting the foregoing, you agree that you will not transmit, submit or upload any User Content or act in any way that: (a) restricts or inhibits use of the App and/or Services; (b) violates the legal rights of others; (c) infringes (or results in the infringement of) the intellectual property, moral, publicity, privacy, or other rights of any third party; (d) is (or you reasonably believe or should reasonably believe to be) stolen, illegal, counterfeit, fraudulent, pirated, unauthorized, or violent, or in furtherance of any illegal, counterfeiting, fraudulent, pirating, unauthorized, or violent activity, or that involves (or you reasonably believe or should reasonably believe to involve) any stolen, illegal, counterfeit, fraudulent, pirated, or unauthorized material; (e) does not comply with all applicable laws, rules and regulations; (f) imposes an unreasonably or disproportionately large load on our infrastructure; or (g) posts, stores, transmits, offers, or solicits anything that contains the following, or that you know contains links to the following or to locations that in turn contain links to the following:

            </p>
            <p>
             material that we determine to be offensive (including material promoting or glorifying hate, violence, bigotry, or any entity past or present principally dedicated to such causes or items associated with such an entity), (2) material that is racially or ethnically insensitive, or that is defamatory, harassing or threatening, (3) pornography or obscene material, and anything depicting children in sexually suggestive situations whether or not depicting actual children or may be harmful to a minor, (4) any virus, worm, Trojan horse, or other harmful or disruptive component, or (5) anything that encourages conduct that would be considered a criminal offense, give rise to civil liability, violate any law or regulation or is otherwise inappropriate or offensive.
            </p>

            <h3>Use Restrictions</h3>
            <p>
              You may not do or attempt to do or facilitate a third party in doing any of the following: (1) attempt to decipher, decompile, disassemble, or reverse-engineer any of the software used to provide the Services or the App without our prior written authorization, including framing or mirroring any part of the App or attempt to reverse-engineer any of the technology used within the Tiles; (2) circumvent, disable, or otherwise interfere with security-related features of the Services or the App or features that prevent or restrict use or copying of any content; (3) use the App, Content, or Services in connection with any commercial endeavors in any manner, except for the purposes specifically set forth in these Terms; (4) use any robot, spider, site search or retrieval application, or any other manual or automatic device or process to retrieve, index, data-mine, or in any way reproduce or circumvent the navigational structure or presentation of the App; or (5) use the App, Content, or Services in any manner not permitted by these Terms.
            </p>
            <h3>Intellectual Property</h3>
            <p>FRAMEIT, its affiliates or its licensors, as the case may be, owns the App and the Services, including all worldwide intellectual property rights in the App, and the trademarks, service marks, and logos contained therein registered and unregistered. Except as expressly permitted herein, you may not copy, further develop, reproduce, republish, modify, alter, download, post, broadcast, transmit or otherwise use the App, or the Services. You will not remove, alter or conceal any copyright, trademark, service mark or other proprietary rights notices incorporated in the App or Services, if any. All trademarks are trademarks or registered trademarks of their respective owners. Nothing in these Terms grants you any right to use any trademark, service mark, logo, or trade name of FRAMEIT or any third party.
 
            You own the Images you upload to the App and/or Services and all intellectual property rights related thereto. FRAMEIT will use the Images only for the purposes of providing the Services. For the purpose of receiving the Services, and as part thereof the Tile(s), you are giving FRAMEIT the right to use the Images, per the below:
 
            By submitting or uploading any User Content and/or Images, you grant FRAMEIT and its successors a worldwide, limited, non-exclusive, royalty-free, perpetual, sublicensable and transferable license to use, copy, and print such User Content and/or Images on the Tile(s).
            </p>
            <h3>Copyright</h3>
            <p>
              The policy of FRAMEIT is not to infringe upon or violate the intellectual property rights or other rights of any third party, and FRAMEIT will refuse to use and remove any User Content in connection with the App that infringes the rights of any third party. Under the Digital Millennium Copyright Act of 1998 (the "DMCA"), FRAMEIT will remove any Content (including without limitation any User Content) if properly notified of that such material infringes third party rights, and may do so at its sole discretion, without prior notice to users at any time. The policy of FRAMEIT is to terminate the accounts of repeat infringers in appropriate circumstances.
 
              You are in the best position to judge whether User Content is in violation of intellectual property or personal rights of any third-party. You accept full responsibility for avoiding infringement of the intellectual property or personal rights of others in connection with User Content.
 
              If you believe that something appearing on the App infringes your copyright, you may send us a notice requesting that it be removed, or access to it blocked. If you believe that such a notice has been wrongly filed against you, the DMCA lets you send us a counter-notice. Notices and counter-notices must meet the DMCA’s requirements. We suggest that you consult your legal advisor before filing a notice or counter-notice. Be aware that there can be substantial penalties for false claims. Send notices and counter-notices to us by contacting hi@frameit.com.my.

            </p>
            <h3>Fees and Payment</h3>
            <p>
              In connection with your purchase of a Tile through the Services, you will be asked to provide customary billing information such as name, billing address and credit card information either to FRAMEIT or its third-party payment processor(s). You agree to pay FRAMEIT the Fee as set forth in the App during the purchase process for any confirmed transactions made in connection with your account in accordance with these Terms by one of the methods described on the App. You hereby authorize the collection of such amounts by charging the credit card provided, either directly by FRAMEIT or indirectly, via a third-party online payment processor or by one of the payment methods described on the App. If you are directed to a third-party payment processor(s), you may be subject to terms and conditions governing use of that third party's service and that third party's Personal Data collection practices. Please review such terms and conditions and privacy policy before using the Services.
 
              Where applicable, in certain locations, taxes may also be charged. It is the responsibility of the user to determine whether user is required to collect any applicable taxes, and to clarify such information when purchasing the Tile(s). FRAMEIT does not accept any responsibility for the calculation or collection of any applicable taxes. Except as expressly provided below, fees are non-refundable.

            </p>
            <h3>Refunds</h3>
            <p>
              If you are not completely satisfied with your order due to a mistake we have made or because your Tile(s) have arrived damaged, you may request a refund by emailing us at hi@frameit.com.my and sending us a picture of the damaged Tile(s). Subject to applicable law, we will not refund any amounts if your dissatisfaction with the Tile(s) is due to a mistake on your part or simply because you have changed your mind.

            </p>
            <h3>Third Party Applications and Services</h3>
            <p>
              Portions of the App and/or Services may involve linking to or using web sites belonging to third parties. The App may also provide you with links to access the sites of third-party vendors including, without limitation, for the purpose of reviewing or using their services. We have no control over third-party sites and/or mobile applications, and all use of third-party sites and/or mobile applications and services is at your own risk. Additionally, FRAMEIT cannot accept responsibility for any payments processed or submitted through such third-party sites and/or mobile applications, or for the privacy policies of any such sites. FRAMEIT is not responsible for content or services available by means of such sites and/or mobile applications. FRAMEIT does not endorse any products or services offered by third parties and we urge our users to exercise caution in using third-party sites.

            </p>
            <h3>Disclaimers and Disclaimer of Warranty</h3>
            <p>
              Your use of the App, Services and Tiles(s) is at your sole discretion and risk. The App, content thereof (including User Content), Services and Tiles are provided on an "AS IS" and "AS AVAILABLE" basis without warranties of any kind. While we make reasonable efforts to print the Images on the Tile(s) as similar to the respective Image as provided by you through the App as possible, we do not make any warranties or representations in respect thereof. We do not represent or warrant that Tile(s) will be of good quality or useful for your needs.
 
              WE EXPRESSLY DISCLAIM ALL WARRANTIES OF ANY KIND, EXPRESS, IMPLIED OR STATUTORY, RELATING TO: (A) THE APP, CONTENT (INCLUDING USER CONTENT), SERVICES AND TILES, INCLUDING WITHOUT LIMITATION THE WARRANTIES OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT OF PROPRIETARY RIGHTS, COURSE OF DEALING OR COURSE OF PERFORMANCE. WE DISCLAIM ANY WARRANTIES, EXPRESS OR IMPLIED, (I) REGARDING THE SECURITY, ACCURACY, RELIABILITY, TIMELINESS AND PERFORMANCE OF THE APP AND SERVICES; OR
              (II) THAT THE APP AND SERVICES WILL BE ERROR-FREE OR THAT ANY ERRORS WILL BE CORRECTED; OR (III) REGARDING THE PERFORMANCE OF OR ACCURACY, QUALITY, CURRENCY, COMPLETENESS OR USEFULNESS OF ANY INFORMATION PROVIDED BY THE APP AND SERVICES; (B) ATTACHING, STICKING AND/OR RE-STICKING THE TILES.
 
              We take no responsibility for the quality of the Tile(s) or the use thereof by you or any other third party, nor do we take any responsibility for the transport of the Tile(s) and/or the state in which the Tile(s) will arrive at the address provided by you during the registration process and/or any damage that may occur if you choose to attach, stick and/or re-stick the Tile(s) on any surface and/or any other (physical) damage of any kind caused due to the Tiles, the attachment or detachment thereof by you or anyone else on or from any surface. No advice or information, whether oral or written, obtained by you from us, shall create any warranty not expressly stated in these Terms. If you choose to rely on such information, you do so solely at your own risk. Some states or jurisdictions do not allow the exclusion of certain warranties. Accordingly, some of the above exclusions may not apply to you. Check your local laws for any restrictions or limitations regarding the exclusion of implied warranties.

            </p>
            
            <h3>Limitation of Liability</h3>
            <p>
           We assume no responsibility for any error, omission, interruption, deletion, defect, delay in operation or transmission, communications line failure, theft or destruction or unauthorized access to, or alteration of, any Content or Services, quality, attachment or detachment of the Tile(s) and the quality of the printed Image on the Tile(s). We are not responsible for any problems or technical malfunction of any telephone network or lines, computer online systems, servers or providers, computer equipment, software, failure of any email due to technical problems or traffic congestion on the Internet or on any of the App or Services or combination thereof, including any injury or damage to you or to any person's cellular phone or computer related to or resulting from participation or downloading materials and/or uploading of the Image(s) in connection with the App or Services. Under no circumstances shall we be responsible for any loss or damage, including personal injury or death, resulting from use of the App or Services, from any Content posted on or through the App or Services, or from the conduct of any users of the App or Services, whether online or offline.
 
          IN NO EVENT SHALL WE OR ANY OF OUR OFFICERS, DIRECTORS, EMPLOYEES, ASSIGNEES OR AGENTS BE LIABLE TO YOU FOR ANY DAMAGES WHATSOEVER, INCLUDING WITHOUT LIMITATION INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES, ARISING OUT OF OR IN CONNECTION WITH YOUR USE OF THE APP,SERVICES AND/OR TILE(S), INCLUDING BUT NOT LIMITED TO THE QUALITY, ACCURACY, OR UTILITY OF THE TILES PROVIDED AS PART OF OR THROUGH THE APP OR THE SERVICES, WHETHER THE DAMAGES ARE FORESEEABLE AND WHETHER OR NOT WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. THE FOREGOING LIMITATION OF LIABILITY SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY LAW IN THE APPLICABLE JURISDICTION AND IN NO EVENT SHALL OUR MAXIMUM CUMULATIVE LIABILITY TO YOU UNDER ANY CAUSE(S) OF ACTION (WHETHER IN CONTRACT, TORT OR OTHERWISE) EXCEED THE AMOUNT YOU HAVE PAID US IN THE TWELVE MONTHS IMMEDIATELY PRECEDING THE APPLICABLE CLAIM OR SERIES OF CLAIMS.

          </p>
          <h3>Indemnification</h3>
          <p>
            You agree to indemnify, defend, and hold harmless FRAMEIT and each of its employees, directors, officers, subcontractors and agents, against any and all claims, damages, or costs or expenses (including court costs and attorneys’ fees) that arise directly or indirectly from: (a) breach of these Terms by you or anyone using your computer, mobile device or password (whether authorized or unauthorized); (b) any claim, loss or damage experienced from your use or attempted use of the App or the Services, including any Image uploaded to the App or Services; (c) your violation of any law or regulation or any of your obligations, representations, or warranties hereunder; (d) your infringement of any right of any third party; and (e) any other matter for which you are responsible hereunder or under applicable law.
          </p>
          
            <h3>Miscellaneous</h3>
            <p>
            These Terms shall be governed solely by the laws of the State of Malaysia, and the competent courts in Malaysia shall have exclusive jurisdiction to hear any disputes arising hereunder. Your conduct may also be subject to other laws. In any action to enforce these Terms, the prevailing party will be entitled to costs and attorneys’ fees. Any cause of action against FRAMEIT must be brought within one (1) year of the date such cause of action arose. In the event that any provision of these Terms is held to be unenforceable, such provision shall be replaced with an enforceable provision which most closely achieves the effect of the original provision, and the remaining terms of these Terms shall remain in full force and effect. Nothing in these Terms creates any agency, employment, joint venture, or partnership relationship between you and FRAMEIT or enables you to act on behalf of FRAMEIT. Except as may be expressly stated in these Terms, these Terms constitute the entire agreement between us and you pertaining to the subject matter hereof, and any and all other agreements existing between us relating thereto are hereby canceled. We may assign and/or transfer our rights and obligations hereunder to any third party without prior notice. You shall not
            assign and/or transfer any of your rights or obligations hereunder, and any assignment in violation of the foregoing shall be void. No waiver of any breach or default hereunder shall be deemed to be a waiver of any preceding or subsequent breach or default. If we are required to provide notice to you hereunder, we may provide such notice to the contact details you provided upon registration.
 
            Last updated: January 2020

            </p>
            



  
        </div>
     </div>
  </div>
</section>
<!--============================-->
</body>
@endsection
