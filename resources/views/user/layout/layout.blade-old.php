<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>FrameIt</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta content="" name="keywords">
  <meta content="" name="description">
  <meta property="og:url"           content="http://votivelaravel.in/frameit/select_frame" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="FrameIt" />
  <meta property="og:description"   content="FrameIt description" />
  <meta property="og:image"         content="http://votivelaravel.in/frameit/resources/front_assets/img/logo.png" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Roboto:100,300,400,500,700|Philosopher:400,400i,700,700i" rel="stylesheet">
    <!-- Bootstrap css --> 
    <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
    <link href="{{url('/').'/resources/front_assets'}}/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Croppie css -->
    <link href="{{url('/').'/resources/assets/css'}}/croppie.min.css" rel="stylesheet">
    <!-- Font Awesome 5 -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <!-- Libraries CSS Files -->

    <link href="{{url('/').'/resources/front_assets'}}/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="{{url('/').'/resources/front_assets'}}/lib/owlcarousel/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="{{url('/').'/resources/front_assets'}}/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{url('/').'/resources/front_assets'}}/lib/animate/animate.min.css" rel="stylesheet">
    <link href="{{url('/').'/resources/front_assets'}}/lib/modal-video/css/modal-video.min.css" rel="stylesheet">

    <link href="{{url('/').'/resources/assets/css'}}/slick.css" rel="stylesheet">
    <link rel='stylesheet' href="{{url('/').'/resources/assets/css'}}/slick-theme.css">

    <!-- Main Stylesheet File -->
    <link href="{{url('/').'/resources/front_assets'}}/css/style.css" rel="stylesheet">

    <!-- Master Stylesheet File -->
    <link href="{{url('/').'/resources/front_assets'}}/css/master_style.css" rel="stylesheet">

    <!-- <link rel='stylesheet' href='https://cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css'>
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/jquery.slick/1.5.9/slick-theme.css'> -->
    <link rel="stylesheet" href="{{url('/').'/resources/front_assets'}}/css/multiple_style.css" />
    @yield('current_page_css')
    <link rel="stylesheet" href="{{url('/').'/resources/assets/css'}}/jquery-ui.css">
    <link rel="stylesheet" href="{{url('/').'/resources/assets/'}}/css/jquery.Jcrop.min.css" type="text/css" />
    </head>

    @yield('tag_body')


    <!-- Navbar Header -->
    @include('user.layout.header')

    @yield('content')

    <!--========================== Footer ============================-->
    @yield('page_footer')


  <!-- <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  -->
  <!-- JavaScript Libraries -->
  <script src="{{url('/').'/resources/front_assets'}}/lib/jquery/jquery.min.js"></script>
  <script src="{{url('/').'/resources/front_assets'}}/lib/jquery/jquery-migrate.min.js"></script>
  <script src="{{url('/').'/resources/front_assets'}}/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{url('/').'/resources/front_assets'}}/lib/superfish/hoverIntent.js"></script>
  <script src="{{url('/').'/resources/front_assets'}}/lib/superfish/superfish.min.js"></script>
  <script src="{{url('/').'/resources/front_assets'}}/lib/easing/easing.min.js"></script>
  <script src="{{url('/').'/resources/front_assets'}}/lib/modal-video/js/modal-video.js"></script>
  <script src="{{url('/').'/resources/front_assets'}}/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="{{url('/').'/resources/assets/js'}}/jquery-ui.js"></script>
  <!-- Template Main Javascript File -->
  <script src="{{url('/').'/resources/front_assets'}}/js/main.js"></script>
  <script src="{{url('/').'/resources/assets/js'}}/slick.min.js"></script>
  <!--  jQuery and Popper.js  -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" 
      integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" 
      crossorigin="anonymous"></script>
  <!-- Boostrap 4 -->
  <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" 
      integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" 
      crossorigin="anonymous"></script>-->
  <!-- Croppie js -->
 <script src="{{url('/').'/resources/assets/js'}}/croppie.min.js"></script>
  <script type="text/javascript" src="{{url('/').'/resources/assets/js'}}/jquery.cookie.min.js"></script>

   <script src="{{url('/').'/resources'}}/js/jquery.Jcrop.min.js"></script>

  <script type="text/javascript">
   $('.center').slick({
  centerMode: true,
  centerPadding: '60px',
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: true,
  dots: true,
  infinite: true,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        centerMode: true,
        centerPadding: '500px',
        
      }
    },
    {
      breakpoint: 320,
      settings: {
       slidesToShow: 3,
       slidesToScroll: 3,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    }
  ]
});

  </script>


  <script type="text/javascript">
    $('.dropdown>button, #overlay').on('click', function(){
      $('.dropdown-menu, #overlay').toggleClass('show')
    })
  </script>
  <!-- Validation -->
  <script src="{{ url('resources/front_assets/js/jquery.validate.min.js') }}"></script>
  <script src="{{ url('resources/front_assets/js/additional-methods.min.js') }}"></script>
  <script type="text/javascript">
    $('#second_step_form').validate({ 
      // initialize the plugin
      rules: {
       firstName: {
        required: true
      }
    },
    submitHandler: function(form) {
         //form.submit();
         step_second();
       }
     });

    function step_second() {
      var $this = $('form#second_step_form')[0];
      var formData = new FormData($this);
      $.ajax({
        type: 'POST',
        url: "<?php echo url('/').'/register_name'; ?>",
        data: formData,
        //dataType: "text",
        enctype: 'multipart/form-data',
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData) { 
          console.log(resultData);
          if (resultData == 1) {
            window.location.href = "<?php echo url('/login_three'); ?>";
          } else {
            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
            
            $("#displayResBox").html(result_alert);
            document.getElementById("second_step_form").reset();
          }
        },
        error: function(errorData) { 
          console.log(errorData);
          var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
          
          $("#displayResBox").html(result_alert);
        }
      });
    }

    $('#third_step_form').validate({ 
      // initialize the plugin
      rules: {
       email: {
        required: true,
        email: true
      }
    },
    submitHandler: function(form) {
         //form.submit();
         step_three();
       }
     });

    function step_three() 
    {
      var $this = $('form#third_step_form')[0];
      var formData = new FormData($this);
      $.ajax({
        type: 'POST',
        url: "<?php echo url('/').'/register_email'; ?>",
        data: formData,
          //dataType: "text",
          enctype: 'multipart/form-data',
          cache:false,
          contentType: false,
          processData: false,
          success: function(resultData) { 
            console.log(resultData);
            if (resultData == 1) {
              window.location.href = "<?php echo url('/login_three'); ?>";
            } else {
              var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
              
              $("#displayResBox").html(result_alert);
              document.getElementById("second_step_form").reset();
            }
          },
          error: function(errorData) { 
            console.log(errorData);
            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
            
            $("#displayResBox").html(result_alert);
          }
        });
    }

    //show adjust remove modal
  $(document).on('click','.show_modal',function() {
     var img = $(this).data('src');

     //alert(img);

      $(".imgrotate").attr("src",img);
      $(".img").attr("src",img);
       $(".cropbox").attr("src",img);
      $(".jcrop-holder img").attr("src",img);

      $(".rotate_img").attr("src",img);
      $(".imgrotate1").val(img);
      var id = $(this).data('id');
      $('.imgid').attr('data-img', id);
      $('.rotation_id').val(id);
      $('.delete_preview').attr('data-id', id); 

      var Imgsrc = $(this).attr('data-src');
      $('#drag-image').attr("src",Imgsrc);
      $('#cropframeID').val($(this).attr('data-id'));
      $('#ImgID').val(Imgsrc);
      
      var pageURL = img;
      var lastURLSegment = pageURL.substr(pageURL.lastIndexOf('/') + 1);
      $(".imgvalue").val(lastURLSegment);
      
      $("#showModal").modal("hide"); 

        var size;
        $('#cropbox').Jcrop({
          aspectRatio: 1,
          onSelect: function(c){

           size = {x:c.x,y:c.y,w:c.w,h:c.h};

           $("#x2").val(size.x);
           $("#y2").val(size.y);
           $("#w2").val(size.w);
           $("#h2").val(size.h);


           $("#crop").css("visibility", "visible");     
          }

        }); 


    });

    $('.show_modal').click(function() {

      var id = $(this).data('id');
      $('.delete_preview').attr('data-id', id); 
      $("#showModal").modal("hide"); 

    });

    //start  remove preview image
    $('.delete_preview').on("click", function() {
      var delete_id = $(this).data('id'); 
      //alert(delete_id);
      $.ajax({
        type: "GET",
        dataType: "json",
        url: "<?php echo url('/delete_preview_image'); ?>",
        data: {'delete_id': delete_id},
        success: function(data){
          window.location.reload();
          //$("#row" + delete_id).remove();
          //window.location.href = "<?php //echo url('/select_frame'); ?>";
        }
      });
    })
    //end  remove preview image


     

    //start add preview image
    $('#previewForm').validate({ 
      rules: {
       uer_id: {
        required: true
      },

      filename: {
        required: true
        }
      },
      submitHandler: function(form) {
        form.submit();
      }
    });

    $("#btnupload").css("display","none")

  //   function submit_preview_image() {
  //     var $this = $('form#previewForm')[0];
  //     var formData = new FormData($this);

  //     var frame_id=$('#frame_id').val();
  //     if(frame_id!=""){
  //     $.ajax({
  //       type: 'POST',
  //       url: "<?php //echo url('/').'/multiple_uploads'; ?>",
  //       data: formData,
  //       //dataType: "text",
  //       enctype: 'multipart/form-data',
  //       cache:false,
  //       contentType: false,
  //       processData: false,
  //       success: function(resultData) {
  //         //$('#myframesave').modal('show'); 
  //         var delay = 4000; 

  //         setTimeout(function(){ window.location.href = "<?php //echo url('/select_frame'); ?>"; }, delay);
  //         //window.location.href = "<?php //echo url('/select_frame'); ?>";
  //       },
  //       error: function(errorData) { 
  //          window.location.href = "<?php //echo url('/select_frame'); ?>";
  //       }
  //     });
  //   }else{
  //     alert('Please select frame');
  //   }
  // }

  function filePreview(input) {
     var frame_id = $('#frame_id').val();
     //alert(frame_id);
    if(frame_id==undefined || frame_id==" "){
      var frame_id = '3';
    }else{
      var frame_id = frame_id;
    }
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
    
     
    //alert(frame_id);
    var localization="en";
    var formData = new FormData();
    formData.append('localization',localization);
    $.ajax({
      type: 'POST',
      url: "<?php echo url('/').'/api/getAllFrameList'; ?>",
      data: formData,
      cache:false,
      contentType: false,
      processData: false,
      success: function(resultData){ 
        var yourArray = [];
        $.each(resultData.response.frameList, function( i, l ){
          if(resultData.response.frameList[i].id == frame_id){
              if(frame_id==2 || frame_id==4){
              $("#home .bx_mig a").css({"background-size": "66% !important", "background-position-x": "36px !important","background-position-y": "33px !important"});
              $("a.img_bbb.show_modal").css({"max-height": "194px", "overflow": "inherit"});
              $("img.set_frame").css({"min-height": "270px"}); 
              }if(frame_id==5){
              $("#home .bx_mig a").css({"background-size": "94% !important", "background-position-x": "5px !important","background-position-y": "2px !important"});
              $(".bx_mig.selectframe a img").css({"margin-right": "0px"});
              }

              //if(resultData.response.frameList[i].type=='rectangle'){
                      $('.frame_mobile_sec').prepend('<div class="col-3" id="row" style="margin:auto;"><div class="bx_mig selectframe"><a class="img_bbb show_modal" style="background-image: url('+e.target.result+');background-repeat: no-repeat; margin-left: auto !important;margin-right: auto !important; height:290px !important;background-size: cover !important;" href="javascript:void(0);" data-toggle="modal" data-target="#showModal" data-src="'+e.target.result+'" data-id=""><img class="set_frame" src="'+resultData.response.frameList[i].image_path+'/'+resultData.response.frameList[i].images+'"></div></div>');

                       $(".number1").append('<span class="numbersBox" style="visibility:none;"><input  class="input-quantity" min="1" id="" value="1" type="hidden"></span><span class="numbersBox1"><center><label><b>Face Count</b></label><input type="number" name="face_count" id="face_count" value="1" class="form-control" style="width: 10%;"></center></span>');

              // }else{
              //         $('.frame_mobile_sec').prepend('<div class="col-3" id="row" style="margin:auto;"><div class="bx_mig selectframe"><a class="img_bbb show_modal" style="background-image: url('+e.target.result+');background-repeat: no-repeat; width:270px !important; height:270px !important;background-size: cover !important;" href="javascript:void(0);" data-toggle="modal" data-target="#showModal" data-src="'+e.target.result+'" data-id=""><img class="set_frame" src="'+resultData.response.frameList[i].image_path+'/'+resultData.response.frameList[i].images+'"></div></div>');
              // }
              
          }
        });
        }
      }); 
          
      };
      reader.readAsDataURL(input.files[0]);
  }
}
$("#images").change(function () {
    //filePreview(this);
});

   function submit_preview_image() { 

      var $this = $('form#previewForm')[0];
      
      var formData = new FormData($this);

      $.ajax({
        type: 'POST',
        url: "<?php echo url('/').'/multiple_uploads'; ?>",
        data: formData,
        dataType: 'json',
        //dataType: "text",
        enctype: 'multipart/form-data',
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData) { 

        // window.location.href = '';

          //$(".cleanStyl1").css({"display": "none"});
          
          $.ajax({
                type: 'POST',
                url: "<?php echo url('/').'/ajax_select_frame'; ?>",
                data: formData,
                //dataType: "text",
                enctype: 'multipart/form-data',
                cache:false,
                contentType: false,
                processData: false,
                success: function (resultDatas) {

                $("#home").load(location.href + " #home");
                
                 
                  //$(".frame_mobile_sec").empty();
              /*  $.each(resultDatas.response.frameLists.tmpImgData, function( i, l ){
                      //alert(resultDatas.response.frameLists.tmpImgData[i].id);
                      //if(resultDatas.response.frameLists.tmpImgData[i].frame_id == frame_id){
                if(resultDatas.response.frameLists.tmpImgData[i].frame_id==2 || resultDatas.response.frameLists.tmpImgData[i].frame_id==4){
                $("#home .bx_mig a").css({"background-size": "66% !important", "background-position-x": "36px !important","background-position-y": "33px !important"});
                $("a.img_bbb.show_modal").css({"max-height": "194px", "overflow": "inherit"});
                $("img.set_frame").css({"min-height": "230px"}); 
                }if(resultDatas.response.frameLists.tmpImgData[i].frame_id==5){
                $("#home .bx_mig a").css({"background-size": "94% !important", "background-position-x": "5px !important","background-position-y": "2px !important"});
                $(".bx_mig.selectframe a img").css({"margin-right": "0px"});
                }

               

                if(resultDatas.response.frameLists.tmpImgData[i].thumb_filename==null){ 
                    $('.frame_mobile_sec').html('<div class="col-3" id="row'+resultDatas.response.frameLists.tmpImgData[i].id+'" style="margin: auto;"><div class="bx_mig selectframe"><a class="img_bbb show_modal" style="background-image: url(https://votivelaravel.in/newframeit/public/uploads/order_images/'+resultDatas.response.frameLists.tmpImgData[i].filename+');background-repeat: no-repeat; width:100; height:100;background-size: cover !important;" href="javascript:void(0);" data-toggle="modal" data-target="#showModal" data-src="https://votivelaravel.in/newframeit/public/uploads/order_images/'+resultDatas.response.frameLists.tmpImgData[i].filename+'" data-id="'+resultDatas.response.frameLists.tmpImgData[i].id+'"><img class="set_frame" src="https://votivelaravel.in/newframeit/public/uploads/frame_images/'+resultDatas.response.frameLists.tmpImgData[i].images+'"></div></div>');
                }else{
                    $('.frame_mobile_sec').html('<div class="col-3" id="row'+resultDatas.response.frameLists.tmpImgData[i].id+'" style="margin: auto;"><div class="bx_mig"><a href="javascript:void(0);" class="img_bbb show_modal" data-toggle="modal" data-target="#showModal" data-src="https://votivelaravel.in/newframeit/public/uploads/order_images/'+resultDatas.response.frameLists.tmpImgData[i].thumb_filename+'" data-id="'+resultDatas.response.frameLists.tmpImgData[i].id+'"><img class="set_frame saveframe" src="https://votivelaravel.in/newframeit/public/uploads/frame_images/'+resultDatas.response.frameLists.tmpImgData[i].images+'" style="transform: rotate(0deg);display: block;height: 270px;width: 270px;"><img src="https://votivelaravel.in/newframeit/public/uploads/order_images/'+resultDatas.response.frameLists.tmpImgData[i].thumb_filename+'" class="up_images"style="display:block;margin-left: -50%;margin-right:auto!important;min-width:221px;width:260px;object-fit:cover;height:260px;"></a></div><div class="number"><span class="numbersBox1"><center><label><b>Face Count</b></label><input type="number" name="face_count" id="face_count" value="1" class="form-control" style="width: 50%;"></center></span></div></div>');
                      }
                   }); */
                }
            });
          
         },
        error: function(errorData) { 
         
        }
      });
    }
    //end submit preview image

    // start add shipping address
    $('#createShipAddress').validate({ 
      rules: {
        fullname: {
          required: true,
        },
        country:{
          required: true,
        },
        streetAddress: {
          required: true
        },
        city:{
          required: true,
        },
        state:{
          required: true,
        },
        address:{
          required: true,
        },
        contact_number: {
          required: true,
          digits:true,
          minlength : 8,
          maxlength : 13
        },
      },
      submitHandler: function(form) {
         //form.submit();
         addShipAddress();
       }
     });

    $.ajaxSetup({
      headers: {'votive':'123456'}
    });

    function addShipAddress() {
      var $this = $('form#createShipAddress')[0];
      var formData = new FormData($this);

      $.ajax({

          type: 'POST',
          url: "<?php echo url('/').'/createShipAddress'; ?>",
          data: formData,
          enctype: 'multipart/form-data',
          cache:false,
          contentType: false,
          processData: false,
          success: function(resultData) { 
            if (resultData.status) {

              var result_alert = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> '+resultData.msg+'</div>';
              $("#signupResBox").html(result_alert);
              $("#shipping_price").html('<span class="tot_l">Shipping Price</span><span class="tot_r">'+resultData.response.ship_price+' '+resultData.response.ship_currency+'</span>');
              var delay = 1500; 
              setTimeout(function(){ window.location.href = "<?php echo url('/profile'); ?>"; }, delay);

            } else {

              var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
              $("#signupResBox").html(result_alert);

            }

          },
          error: function(errorData) { 
              var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
              $("#signupResBox").html(result_alert);
          }
      });
  }
  // end shipping address 

 
// start update ship address
$('#updateShipAddress').validate({ 
  rules: {
    fullname: {
      required: true,
    },
    country:{
      required: true,
    },
    streetAddress: {
      required: true
    },
    city:{
      required: true,
    },
    state:{
      required: true,
    },
    address:{
      required: true,
    },
    contact_number: {
      required: true,
      digits:true,
      minlength : 8,
      maxlength : 13
    },
  },
  submitHandler: function(form) {
     //form.submit();
     update_address();
   }
 });

$.ajaxSetup({
  headers: {'votive':'123456'}
});

function update_address() {
    var $this = $('form#updateShipAddress')[0];
    var formData = new FormData($this);

    $.ajax({

        type: 'POST',
        url: "<?php echo url('/').'/api/editShipAddress'; ?>",
        data: formData,
        enctype: 'multipart/form-data',
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData) { 


          if (resultData.status) {

            var result_alert = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> '+resultData.msg+'</div>';
            $("#updatesignupResBox").html(result_alert);
            $("#shipping_price").html('<span class="tot_l">Shipping Price</span><span class="tot_r">'+resultData.response.ship_price+' '+resultData.response.ship_currency+'</span>');
            // Your delay in milliseconds
            var delay = 1500; 
                setTimeout(function(){ 

                $('#editaddress').hide();

            	//window.location.href = "<?php echo url('/select_frame'); ?>"; 
              window.location.href = ""; 

            }, delay);

          } else {

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
            $("#updatesignupResBox").html(result_alert);
          }
        },
        error: function(errorData) { 

          var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
          $("#updatesignupResBox").html(result_alert);

        }
      });
  }
  // end update address 

  // start get countries list
  $.ajaxSetup({
    headers: {'votive':'123456'}
  });

  $(document).ready(function(){
    var formData = new FormData();
    $.ajax({
      type: 'POST',
      url: "<?php echo url('/').'/api/getAllCountryList';?>",
      data: formData,
      enctype: 'multipart/form-data',
      cache:false,
      contentType: false,
      processData: false,
      success: function(resultData){ 
        //console.log(resultData.response);
        $.each(resultData.response.countryList, function(key, value) {   
          $('.country_list').append($("<option value="+resultData.response.countryList[key].id+">"+resultData.response.countryList[key].name+"</option>")); 
        });

      }
    });
  });
  // end get countries list
  <?php @$cat_id=Request::segment(2);?>
  // start get frame list
  $.ajaxSetup({
    headers: {'votive':'123456'}
  });
  $(document).ready(function(){
    //var frame_id = $.cookie("frame_id");
   var frame_id = $('#frame_id').val();
    if(frame_id==undefined || frame_id==" "){
      var frame_id = '3';
    }else{
      var frame_id = frame_id;
    }
    var cat_id = '<?php echo $cat_id;?>';
    
    var formData = new FormData();
    //formData.append('cat_id', cat_id);
    //alert(cat_id);
    $.ajax({
      type: 'POST',
      url: "<?php echo url('/').'/api/getAllFrameList'; ?>",
      dataType: "json", 
      contentType: "application/json; charset=utf-8", 
      data: JSON.stringify({ cat_id: cat_id }),
      success: function(resultData){ 
        var yourArray = [];
        $.each(resultData.response.frameList, function( i, l ){
          if(resultData.response.frameList[i].id == frame_id && resultData.response.frameList[i].quantity > 0){
            var active_class = 'active show';
            $("#frame_id").val(frame_id);
          }else{
            var active_class = '';
          }
          if(resultData.response.frameList[i].quantity<=0){
            var frame = '<li><a data-toggle="tab" href="#home" class="'+active_class+'"><span class="out_stock">Out of Stock</span><div class="frmStyl "><div id="msg" style="padding-top:10px;color:red"></div><div class="allframe"><img src="'+resultData.response.frameList[i].image_path+'/'+resultData.response.frameList[i].images+'"></div><span>'+resultData.response.frameList[i].title+'</span></div></a></li>';
          }else{
            var frame = '<li onclick="getFrameById('+resultData.response.frameList[i].id+')" data-id="'+resultData.response.frameList[i].id+'"><span class="out_stock"></span><a data-toggle="tab" href="#home" class="'+active_class+'"><div class="frmStyl "><div id="msg" style="padding-top:10px"></div><div class="allframe"><img src="'+resultData.response.frameList[i].image_path+'/'+resultData.response.frameList[i].images+'"></div><span>'+resultData.response.frameList[i].title+'</span></div></a></li>';
          }
          
          yourArray.push(frame);

        });
        $(".frameList").html(yourArray);
        $("#coloroption").show();
      }
    });
  });
  // end get frame list

  //start get frame by id
  function getFrameById(id) {
    var selt = id;
    var facecount  = $('#face_count').val();
    var cat_id = '<?php echo $cat_id;?>';
    var formData = new FormData();
    $.ajax({
      type: 'POST',
      url: "<?php echo url('/').'/api/getAllFrameListbyid'; ?>",
      dataType: "json", 
      contentType: "application/json; charset=utf-8", 
      data: JSON.stringify({ id: id }),
      success: function(resultData){ 
        var yourArray = [];
        //var frm;
        $.each(resultData.response.frameList, function( i, l ){
          if(selt==resultData.response.frameList[i].id && resultData.response.frameList[i].quantity > 0){
            var active_class = 'active show';
          }else{
            var active_class = '';
          }


         
          if(resultData.response.frameList[i].quantity<=0){
            var frame = '<li><a data-toggle="tab" href="#home" class="'+active_class+'"><div class="frmStyl "><div id="msg" style="padding-top:10px;color:red"></div><div class="allframe"><img src="'+resultData.response.frameList[i].image_path+'/'+resultData.response.frameList[i].images+'"></div><span>'+resultData.response.frameList[i].title+'</span></div></a></li>';
          }else{
             var frame = '<li onclick="getFrameById('+resultData.response.frameList[i].id+')" data-id="'+resultData.response.frameList[i].id+'"><a data-toggle="tab" href="#home" class="'+active_class+'"><div class="frmStyl "><div id="msg" style="padding-top:10px"></div><div class="allframe"><img src="'+resultData.response.frameList[i].image_path+'/'+resultData.response.frameList[i].images+'"></div><span>'+resultData.response.frameList[i].title+'</span></div></a></li>';
          }

          yourArray.push(frame);

         
          if(selt==resultData.response.frameList[i].id){

            var frm = '<div class="col-3"><div class="bx_mig"><a href="#" class="kk"><img class="image" data-id="'+resultData.response.frameList[i].id+'" src="'+resultData.response.frameList[i].image_path+'/'+resultData.response.frameList[i].images+'"></a></div></div>';
            $(".select_frame").html(frm);
            $(".set_frame").attr('src',resultData.response.frameList[i].image_path+'/'+resultData.response.frameList[i].images);
            $(".select_frame_id").val(resultData.response.frameList[i].id);
            submit_frame_id(resultData.response.frameList[i].id);
	         $.cookie("frame_id", resultData.response.frameList[i].id);

           $('.frame_id').val(resultData.response.frameList[i].id);

           $('#fprice').val(resultData.response.frameList[i].price);

           var sprice = $('#sprice').val();

           var tt = parseFloat(resultData.response.frameList[i].price) + parseFloat(sprice);

           $('.tprice').html('<strong>'+resultData.response.frameList[i].price+'</strong>');

           $('.ttprice').html('<strong>'+tt+'</strong>');

           $('#number_people').val(facecount);
          
          }
        });

        $(".frameList").html(yourArray);
        $('#ckeckout').show();
        $('#continue1').hide();
         $("#coloroption").show();
        //$('#continue2').hide();
      }
    });
  }
  //end get frame by id

  //start tile select msg
  $('#order_message').click(function () {
      var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Info!</strong>  Please select frame first</div>';
      $("#order_msg").html(result_alert);
  });
  //end tile select msg

  // start quantity increment decrement
  function increment_quantity(cart_id) {
    var inputQuantityElement = $("#input-quantity-"+cart_id);
    var newQuantity = parseInt($(inputQuantityElement).val())+1;
    save_image_quantity(cart_id, newQuantity);
  }

  function decrement_quantity(cart_id) {
    var inputQuantityElement = $("#input-quantity-"+cart_id);
    if($(inputQuantityElement).val() > 1) 
    {
      var newQuantity = parseInt($(inputQuantityElement).val()) - 1;
      save_image_quantity(cart_id, newQuantity);
    }
  }

  function save_image_quantity(id, qty) {
  	var quantity = qty;
  	if(qty>=1 && qty!=""){
    $.ajax({
      type: "GET",
      dataType: "json",
      url: "<?php echo url('/save_image_quantity'); ?>",
      data: {'id':id,'qty':qty},
      success: function(data){
          var delay = 100; 
          setTimeout(function(){ window.location.href = "<?php echo url('/select_frame'); ?>"; }, delay);
      }
    });
	}else{
		var quantity = 1;
		$('.input-quantity').val(quantity);
		alert("frame quantity should not be 0 and not empty");
	}
  }
  // start quantity increment decrement

  //start get faq list
  $.ajaxSetup({
    headers: {'votive':'123456'}
  });
  
  $(document).ready(function(){
    var formData = new FormData();
    $.ajax({
        type: 'POST',
        url: "<?php echo url('/').'/api/getAllFaqList'; ?>",
        data: formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData){ 
          console.log(resultData);
          if(resultData.status){

            var faqArray = [];
            var j=1;
            $.each(resultData.response.faqList, function( i, l ){

              var faqs = '<li><h5>'+resultData.response.faqList[i].question+'</h5><p>'+resultData.response.faqList[i].answer+'</p></li>';

              faqArray.push(faqs);
              j++;
            });
            $(".faqList").html(faqArray);

          }else{

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> No Faq List Found.</div>';
            $("#err_msg").html(result_alert);

          }

        },error: function(errorData) { 

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
            $("#err_msg").html(result_alert);

        }
    });
  });
  //end get faq list
</script>

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
</script>



<script type="text/javascript">
  $('#signup_form').validate({ 
      // initialize the plugin
      rules: {
       name: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      password: {
        required: true,
        minlength : 6
      },
      confirm_password: {
        required: true,
        equalTo : "#password"
      },
      contact_number: {
        required: true,
        digits:true,
        minlength : 8,
        maxlength : 13
      },
    },
    submitHandler: function(form) {
         //form.submit();
         register();
       }
     });

  $.ajaxSetup({
    headers: {'votive':'123456'}
  });

  function register() {
    var $this = $('form#signup_form')[0];
    var formData = new FormData($this);
    $.ajax({
      type: 'POST',
      url: "<?php echo url('/').'/api/user_register'; ?>",
      data: formData,
        //dataType: "text",
        enctype: 'multipart/form-data',
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData) { 
          console.log(resultData);
          if (resultData.status) {
            var result_alert = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> '+resultData.msg+'</div>';
            $("#signupResBox").html(result_alert);
            document.getElementById("signup_form").reset();
          } else {
            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
            
            $("#signupResBox").html(result_alert);
          }
        },
        error: function(errorData) { 
          console.log(errorData);
          var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
          
          $("#signupResBox").html(result_alert);
        }
      });
  }

  $('#forgot_pass_form').validate({ 
      // initialize the plugin
      rules: {
       email: {
        required: true,
        email: true
      },
      password: {
        required: true
      }
    },
    submitHandler: function(form) {
         //form.submit();
         forgot_password();
       }
     });

  $('#login_form').validate({ 
      // initialize the plugin
      rules: {
       email: {
        required: true,
        email: true
      },
      password: {
        required: true
      }
    },
    submitHandler: function(form) {
         //form.submit();
         login();
       }
     });

  function login() {
    var $this = $('form#login_form')[0];
    var formData = new FormData($this);
    $.ajax({
      type: 'POST',
      url: "<?php echo url('/').'/api/login'; ?>",
      data: formData,
        //dataType: "text",
        enctype: 'multipart/form-data',
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData) { 
          //console.log(resultData);
          if (resultData.status==true) {
            //resultData.data.id
            //alert(resultData.status);
            document.getElementById("login_form").reset();
            window.location.href = "<?php echo url('/custom_auth'); ?>"+"/"+resultData.data.id;
          } else {
            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
            
            $("#loginResBox").html(result_alert);
          }
        },
        error: function(errorData) { 
          console.log(errorData);
          var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
          
          $("#loginResBox").html(result_alert);
        }
      });
  }

  function forgot_password() {
    var $this = $('form#forgot_pass_form')[0];
    var formData = new FormData($this);
    $.ajax({
      type: 'POST',
      url: "<?php echo url('/').'/api/forgot_password'; ?>",
      data: formData,
        //dataType: "text",
        enctype: 'multipart/form-data',
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData) { 
          console.log(resultData);
          if (resultData.status) {
            //resultData.data.id
            document.getElementById("forgot_pass_form").reset();
            var result_alert = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
            
            $("#forgotPassResBox").html(result_alert);
          } else {
            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
            
            $("#forgotPassResBox").html(result_alert);
          }
        },
        error: function(errorData) { 
          console.log(errorData);
          var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
          
          $("#forgotPassResBox").html(result_alert);
        }
      });
  }
  
</script>

<script type="text/javascript">
$("#div1").click(function(){
  $("#divrotation").show();
  $("#cropdiv").hide();
  $("#crop").hide();
  $("#rotatedone").show();
});
$("#div2").click(function(){
  $("#divrotation").hide();
  $("#cropdiv").show();
  $("#crop").show();
  $("#rotatedone").hide();
});
</script>

<script type="text/javascript">
  $('#change_password').validate({ 
      // initialize the plugin
      rules: {
       current_password: {
        required: true
      },
      new_password: {
        required: true,
        minlength : 6
      },
      new_confirm_password: {
        required: true,
        equalTo : "#new_password"
      },
    },
    submitHandler: function(form) {
         //form.submit();
         change_password();
       }
     });

  $.ajaxSetup({
    headers: {'votive':'123456'}
  });

  function change_password() {
    var $this = $('form#change_password')[0];
    var formData = new FormData($this);
    //var user_id = "<?php //echo $user_id ?>";
    //formData.append('user_id',user_id);
    console.log(formData);
    $.ajax({
      type: 'POST',
      //url: "<?php //echo url('/').'/api/updateprofile'; ?>",
      url: "<?php echo url('/').'/api/changepassword'; ?>",
      data: formData,
        //dataType: "text",
        enctype: 'multipart/form-data',
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData) { 
          console.log(resultData);
          if (resultData.status) {
            var result_alert = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> '+resultData.msg+'</div>';
            $("#signupResBox").html(result_alert);
            document.getElementById("change_password").reset();
            window.location.href = "<?php echo url('/profile'); ?>";
            
          } else {
            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
            
            $("#signupResBox").html(result_alert);
          }
        },
        error: function(errorData) { 
          //console.log(errorData);
          var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
          
          $("#signupResBox").html(result_alert);
        }
      });
  }
</script>



<script type="text/javascript">
  $(document).ready(function() {
     
    $(".Rig_seed").click(function () {
      $(this).closest(".tab_one_sec").addClass('openaddress');
    });


    $(".address_book_s").click(function () {
      $(this).closest(".tab_one_sec").addClass('add_new');
    });


    $(".add_new_adds").click(function () {
      $(this).closest(".tab_one_sec").addClass('editadd_new');
    });


    $(".add_n_address").click(function () {
      $(this).closest(".tab_one_sec").addClass('editPerinfo');
    });


    $(".add_n_address").click(function () {
      $(this).closest(".tab_one_sec").addClass('editPerinfo');
    });


    $(".mang_ac").click(function () {
      $(this).closest(".edit_pro").addClass('editsen');
    });


   $(".star_sh").click(function () {
      $(this).closest(".four_tab_main").addClass('my_reddd_sss');
    });


   $(".back_m_l a").click(function () {
      $(this).closest(".tab_one_sec").removeClass('openaddress');
   });  
    
  });     
</script>



<!-- <script type="text/javascript">
  $(document).ready(function() {
    $(".mang_ac").click(function () {
       alert("Hello! I am an alert box!");
      $(this).closest(".edit_pro").addClass('editsen');
    });
  });     
</script>
 -->

 <script type="text/javascript">


$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});


$uploadCrop = $('#upload-demo').croppie({
    enableExif: true,
    viewport: {
        width: 150,
        height: 150,
        type: 'Square'
    },
    boundary: {
        width: 200,
        height: 200
    }
});


$('#upload').on('change', function () { 
  var reader = new FileReader();
    reader.onload = function (e) {
      $uploadCrop.croppie('bind', {
        url: e.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
});


$('.upload-result').on('click', function (ev) {
  var id = $('.imgid').attr("data-img");
  //alert(id);
  $uploadCrop.croppie('result', {
    type: 'canvas',
    size: 'viewport'
  }).then(function (resp) {
    $.ajax({
      url: "http://votivelaravel.in/frameit/image-crop",
      type: "POST",
      data: {"image":resp, "id":id},
      success: function (data) {
        //alert(data.success);
        html = '<img src="' + resp + '" />';
        $("#upload-demo-i").html(html);
        window.location.href = "<?php echo url('/select_frame'); ?>";
      }
    });
  });
});



 $("#faq").click(function(){
  $("#loginModal").modal('hide');
    $("#exampleModalCenterFaq").modal('show');
    
});

 $("#forgetp").click(function(){
  $("#loginModal").modal('hide');
    $("#forgotPassModal").modal('show');
    
});

 $("#signp").click(function(){
  $("#loginModal").modal('hide');
    $("#signupModal").modal('show');
    
});

$("#getAddressId").click(function(){
  
    //$("#showAddressBooknew").modal('show');
    $("#exampleModalCenterOne").modal('hide');
     // var delay = 3000; 
     // setTimeout(function(){ window.location.href = "<?php //echo url('/select_frame'); ?>"; }, delay);
});



</script>
 <script type="text/javascript">
    $('.sqare').on('click', function(){
         $('.cr-vp-square').css({
            'width' : '270px',
            'height' : '270px',
        
        });
    });
    $('.reactangle_v').on('click', function(){
         $('.cr-vp-square').css({
            'width' : '270px',
            'height' : '345px',
        
        });
    });
    $('.reactangle_h').on('click', function(){
         $('.cr-vp-square').css({
            'width' : '345px',
            'height' : '270px',
        
        });
    });
  </script>

<script>
function goBack() {
  window.history.back();
}
</script>

<script type="text/javascript" language="javascript">
    $( ".option" ).click(function () {
        var pfor1 = $(this).data('pfor');
        var pfor = localStorage.setItem('pfor', pfor1);
        var pfor2 = localStorage.getItem('pfor');
         $.ajax({
          url: "<?php echo url('/').'/step1_store'; ?>",
          type: "POST",
          data: {'pfor':pfor1},
          dataType: "json",
          enctype: 'multipart/form-data',
        cache:false,
        contentType: false,
        processData: false,
          success: function (data) {
            //alert(data.status);
            
            var delay = 1000; 
            setTimeout(function(){ window.location.href = "<?php echo url('/step_2'); ?>"; }, delay);
            }
          });
            
      });


    $( document ).ready(function() {
      var pfor2 = localStorage.getItem('pfor');
      $('#pfor1').val(pfor2);
    });



   
    // $( "#namebtn" ).click(function () {
    //     var firstName1 = $('#firstName').val();
    //     var firstName = localStorage.setItem('firstName', firstName1);
    //     var firstName2 = localStorage.getItem('firstName');
    //     $.ajax({
    //       url: "<?php //echo url('/').'/step2_store'; ?>",
    //       type: "POST",
    //       data: {'firstName':firstName2},
    //       enctype: 'multipart/form-data',
    //     cache:false,
    //     contentType: false,
    //     processData: false,
    //       success: function (data) {
    //         //alert(data.status);
    //         var delay = 100; 
    //         setTimeout(function(){ window.location.href = "<?php //echo url('/step_3'); ?>"; }, delay);
    //         }
    //       });
       
    //     });
   
    // $( "#emailbtn" ).click(function () {
    //     var email1 = $('#email').val();
    //     alert(email1);
    //     $.ajax({
    //       url: "https://votivetech.in/mixshoot/step3_store",
    //       type: "POST",
    //       data: {'email':email1},
    //       enctype: 'multipart/form-data',
    //     cache:false,
    //     contentType: false,
    //     processData: false,
    //       success: function (data) {
    //         alert(data.email);
    //         var delay = 1000; 
    //         setTimeout(function(){ window.location.href = "<?php //echo url('/select_frame'); ?>"; }, delay);
    //         }
    //       });
    //     });

   </script>



@yield('current_page_js')
</body>
      <div class="modal fade pop_address" id="exampleModalCenterFaq" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">        
            <div class="modal-body">
              <div class="address-form">
                <div class="top-bar-container">
                  <div class="top-bar">
                    <div class="left-comp">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times; <span class="cls_btn">Close</span></span>
                      </button>        
                    </div>
                    <div class="title_fgh">Frequent Questions</div>                  
                  </div>
                  <div class="bottom-comp"></div>
                </div>

                <ul class="list_fq scrollbar_s faqList" id="style-1">
                  
                </ul>              
              </div>     
            </div>        
          </div>
        </div>
      </div>

 <!-- Modal -->






  <div class="modal fade select_form_address add_reward_s" id="rewardnew" role="dialog">
    <div class="modal-dialog select_form_addressll">
    
      <!-- Modal content-->
      <div class="modal-content select_form_address_n my_reew">
        <div class="modal-header my_reew_nn">
        <!--   <button type="button" class="close" data-dismiss="modal">&times; Close</button> -->
          <h4 class="modal-title">My Rewards</h4>
           <button type="button" class="close" data-dismiss="modal">&times; </button>
        <!--   <a href="#">Done</a> -->
        </div>
        <div class="modal-body select_form_address_nn">
         
          <div class="main_pop_s_m">
            
  <div class="main_pop_s_m_one ">
  <label class="container_on">Available Reward Creadits
   <input type="radio" checked="checked" name="radio">
  <span class="checkmark"></span><a href="#" class="pt_s">10pt</a>
  </label>

 <div class="main_pop_s_m_one_bt">
   <input type="email" class="form-control col_wht" id="" aria-describedby="emailHelp" placeholder="Amount Redeem Reward Credits">
   
 </div>
 </div>


 <div class="main_pop_s_m_one">
 <label class="container_on">10% Discount on purchase
  <input type="radio" name="radio">
  <span class="checkmark"></span>
 </label>
 <p class="pr_co">promocode xxxxxx explnas on 30 jUNY 2019</p>
</div>


<div class="main_pop_s_m_one">
 <label class="container_on">20% Discount on purchase
  <input type="radio" name="radio">
  <span class="checkmark"></span>
 </label>
<p class="pr_co">promocode xxxxxx explnas on 30 jUNY 2019</p>
</div>


<div class="main_pop_s_m_one">
 <label class="container_on">Insert your promocode
  <input type="radio" name="radio">
  <span class="checkmark"></span>
 </label>
<div class="main_pop_s_m_one_bt">
  <input type="email" class="form-control col_wht" id="" aria-describedby="emailHelp" placeholder="Code">
 </div>
</div>


<div class="main_pop_s_m_one sel_sect">
<a href="#" class="select_s_ss">Select</a>
</div>



</div>
</div>
       
<!--         <div class="modal-footer">
          <button type="button" class="btn btn-default add_ad"> + Add new Address </button>
          <button type="button" class="btn btn-default Sel_add">Select</button>
        </div> -->
      
</div>
      
</div>
</div>





</html>