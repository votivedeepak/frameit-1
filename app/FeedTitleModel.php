<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedTitleModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'feed_title';
    protected $primaryKey = 'feed_heading_id';
    protected $fillable = [
        'feed_heading_name,status'
    ];
    //public $timestamps = false;
}
