<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'payment_transaction';
    protected $fillable = [
        'user_id', 'order_id', 'txn_id', 'txn_amount', 'payment_method', 'card_number',
        'expiry_month', 'expiry_year', 'cvv', 'txn_status', 'txn_date'
    ];
    //public $timestamps = false;
}
