<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'cms_mgmt';
    protected $fillable = [
        'page_name', 'title', 'description'
    ];
    //public $timestamps = false;
}
