<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class CardModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'card_details';
    protected $fillable = [
        'user_id', 'card_number', 'expiry_month', 'expiry_year', 'cvv'
    ];
    //public $timestamps = false;
}
