<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Socialite;
use Auth;
use Exception;
use App\User;
use App\Helpers\Helper;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

     public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

     public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();
            /*echo "<pre>";print_r($user);die;*/
        } catch (Exception $e) {
            return redirect('auth/facebook');
        }
        $authUser = $this->findOrCreateUser($user);

        Auth::login($authUser, true);

        //return redirect()->route('/select_frame');
        return redirect('/select_frame');
    }

    private function findOrCreateUser($facebookUser)
    {
        $authUser = User::where('social_id', $facebookUser->id)->first();

        if ($authUser){
            return $authUser;
        }

        $name_arr = explode(' ', $facebookUser->name);
        $vrfn_code = Helper::generateRandomString(6);

        return User::create([
            'email' => $facebookUser->email,
            'social_id' => $facebookUser->id,
            'profile_pic' => $facebookUser->avatar,
            'first_name' => (!empty($name_arr[0]) ? $name_arr[0] : ''),
            'last_name' => (!empty($name_arr[1]) ? $name_arr[1] : ''),
            'password' => bcrypt($vrfn_code),
            'role_id' => 2,
            'is_verify_email' => 1,
            'is_verify_contact' => 0,
            'wallet_balance' => 0,
            'register_by' => 'facebook',
            'vrfn_code' => '',
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

    }
}
