<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\User;
use App\ShipModel, App\Group_request, Hash, App\PaymentLogs; 
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;
use Route;
use App\Helpers\Helper;
use Mail;
use Redirect;
class HomeController extends Controller 
{

	public $successStatus = true;
    public $failureStatus = false;

	public function home(Request $request) {
		return view('user/home');
	}

	public function newhome(Request $request) {
        $banner_details = DB::select('select * from banner');
        $icon_details = DB::select('select * from icon_table');
        $feed_details = DB::select('select * from feed_title');
        $feed_image_details = DB::select('select * from feeds');
        $video_details = DB::select('select * from pramotion_videos');
		return view('user/newhome',['banner_details'=>$banner_details,'icon_details'=>$icon_details,'feed_details'=>$feed_details,'feed_image_details'=>$feed_image_details,'video_details'=>$video_details]);
	}

	public function user_dashboard(Request $request) {  

		return view('user/dashboard');
	}

	public function crop_image(Request $request) {  

		return view('user/crop_image');
	}
    
    

    public function order_history(Request $request) {  

        return view('user/orders_history');
    }

    public function order_details(Request $request) { 

        return view('user/order_details');
    }

	
	public function createOrder(Request $request) {  
		$forminput =  $request->all();

        $frame_id=Session::put('frame_id',$forminput['frame_id']);
        $frame_detail = DB::table('frames')->where('id',$frame_id)->first();
        //print_r($forminput);die;
        $validator = Validator::make($request->all(), [ 
            'user_id'  => 'required',
            'address_id'  => 'required',
            'frame_id'  => 'required',
            'total_tile'  => 'required',
            'total_price1'  => 'required',
            //'tile_image.*' => 'mimes:jpeg,png,jpg,gif,svg|max:5000'
            ],
            [   
                'user_id.required'     => 'required user_id',
                'address_id.required'     => 'Please select address',
                'frame_id.required'     => 'Please select frame',
                'total_tile.required'     => 'required total_tile',
                'total_price1.required'     => 'required total_price',
            ]
        );
        

        if ($validator->fails()) { 
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {   
                //return response()->json(['status'=>'false','msg'=>$message]);
                session::flash('error', $message);
                //return redirect('/select_frame/');  
                return Redirect::back()->with('msg', $message);           
            }            
        }
        $total_amt = $forminput['total_price1'];

        $address_id = Session::put('address_id',$forminput['address_id']);
        $ship_price = Session::put('shipping_price',$forminput['shipping_price']);
        $user_id = Session::put('user_id',$forminput['user_id']);
        $total_amt1 = Session::put('total_price', $total_amt);
        
        $total_tile = Session::put('total_tile',$forminput['total_tile']);

        $tile_image = Session::put('tile_image',$forminput['tile_image']);
        $quantity = Session::put('quantity',$forminput['quantity']);

        $rotation = Session::put('rotation',$forminput['rotation']);

        $color = Session::put('color',$forminput['color']);

        Session::put('payment',$forminput['payment']);

        $number_people = !empty(Session::put('number_people',$forminput['number_people']))? Session::put('number_people',$forminput['number_people']):0;


        $wallet_amount = !empty(Session::put('wallet_amount',$forminput['wallet_amount']))? Session::put('wallet_amount',$forminput['wallet_amount']):'';
        
        $coupon_code = !empty(Session::put('coupon_code',$forminput['coupon_code']))? Session::put('coupon_code',$forminput['coupon_code']):'';


    	$country_info = DB::table('shipping_address')->where('id',$forminput['address_id'])->first();
        $country_name = $country_info->country;
        $day_info = DB::table('shipping_price')->where('country_id',$country_name)->first();
        //$ship_price = $day_info->price;

        $ship_price = $forminput['shipping_price'];

        $delivery_days = $day_info->delivery_days;
        $current_date = date('Y-m-d');
        $delivery_date = date('Y-m-d',strtotime($current_date.'+ ' .$delivery_days. 'days'));


        $referral_info = DB::table('my_referrals')->where('refferal_id', $forminput['user_id'])->where('status',1)->first();

       
        $wallet_discount = 0;

        if(!empty($wallet_amount)){

            $wallet_disc  = $total_amt + $ship_price - $wallet_amount;

            $wallet_discount =  $total_amt + $ship_price - $wallet_disc;             


        }

        $coupon_discount = 0;

        if(!empty($coupon_code)){

            $coupon_info = DB::table('coupon')->where('code',$coupon_code)->where('status',1)->first();
            $type = $coupon_info->type;
            $coupon_value = $coupon_info->value;

            if($type == 'percentage'){
                 
                $disc_price = ($total_amt * $coupon_value)/100;
                $coupon_disc = $total_amt - $disc_price ;
                $coupon_discount = $total_amt - $coupon_disc;
            }else{
                $coupon_disc = $total_amt - $coupon_value ;
                $coupon_discount = $total_amt - $coupon_disc;
            }

        }
        
        if(!empty($coupon_discount && $wallet_discount)){

            $total_price = ($total_amt + $ship_price) - ($coupon_discount - $wallet_discount);

        }elseif(!empty($coupon_discount)){

            $total_price = ($total_amt + $ship_price) - $coupon_discount;

        }elseif(!empty($wallet_discount)){

            $total_price = ($total_amt + $ship_price) - $wallet_discount;

        }else{
    
            $total_price = $total_amt + $ship_price;
        
        }

        $user_info = DB::table('users')->where('id',$forminput['user_id'])->first();

            $country = DB::table('countries')->where('id',$country_info->country)->first();

        Session::put('total_amt', $total_price);
        if($forminput['payment']=="paypal"){        
                //paypal
            $client = "AcwBj1jBaPuIaGvVF4WCqtT8PMe8XVlNLriyqP2JVlFViJQpJbmF-CMsTnqI9TOA0Z6kWeD3uG5R0xvO";
            $secret= "EPZ31KCn1aSfHzEkjdV6fI_A31vdzcbhVhV-fkc0GFKvc_WVJZPoKOCAw8TNmhKQVAF4pW46iaDpmznd";

            $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/oauth2/token");
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
                curl_setopt($ch, CURLOPT_USERPWD, $client.":".$secret);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

                $result = curl_exec($ch);

                if(empty($result))die("Error: No response.");
                else
                {
                $json = json_decode($result);
               
                }

                $data = '{
                  "intent":"sale",
                  "redirect_urls":{
                    "return_url":"https://votivelaravel.in/newframeit/payment-successful",
                    "cancel_url":"https://votivelaravel.in/newframeit/payment-cancel"
                  },
                  "payer":{
                    "payment_method":"paypal"
                  },
                  "transactions":[
                    {
                      "amount":{
                        "total":'.$total_price.',
                        "currency":"USD"
                      },
                      "description":"This is the payment transaction description."
                    }
                  ]
                }';

                curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/payments/payment");
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                  "Content-Type: application/json",
                  "Authorization: Bearer ".$json->access_token, 
                  "Content-length: ".strlen($data))
                );

                $result = curl_exec($ch);
                //curl_close($ch);
                $json = json_decode($result);

                $state=$json->state;
                //echo "<pre>";
                //print_r($json);
                $link=$json->links[1]->href;
                return redirect($link);

            }
            else{

               // $input = $request->input();

     
        try {
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                $name = $user_info->first_name;
                $country_name = $country->sortname;
                
               
            
                $unique_id = uniqid(); 
                $total_amt = Session::get('total_price');
                
                $charge = \Stripe\Charge::create(array(
                    'description' => "Plan: ",
                    'source' => $request->stripeToken,                    
                    'amount' => (int)($total_amt * 100),
                    'currency' => 'USD',
                    'shipping' => [
                      'name' => $name,
                      'address' => [
                      'line1' => $country_info->street_address,
                      'postal_code' => $country_info->zip_code,
                      'city' => $country_info->city,
                      'state' => $country_info->state,
                      'country' => 'US',
                      ],
                    ],

                  ));

                // print_r($charge);
                // die;

                DB::table('payment_logs')->insert(
                            [
                              'amount'=> $total_amt,
                              'plan'=> 'Plan',
                              'charge_id'=>$charge->id,
                              'stripe_id'=>$unique_id,                     
                              'quantity'=>1
                                
                            ]

                        );
                
                return response()->json([
                    'message' => 'Charge successful, Thank you for payment!',
                    'state' => 'success'
                ]);                
            } catch (\Exception $ex) {
                return response()->json([
                    'message' => 'There were some issue with the payment. Please try again later.',
                    'state' => 'error'
                ]);
            }  
            }
            // else{
            //     Session::put('total_amt', $total_price);
            //     session::flash('message', 'Order created Succesfully.');
            //     return redirect('/payment');
            // }
                
	}

    public function payment_successful(Request $request) {

        // $ref = $request->Ref;
        // $last = DB::table('orders')->latest()->first();
        // $order_id=$last->id;
        // $ref_id=$last->ref_id;

        $address_id = Session::get('address_id');
        $ship_price = Session::get('shipping_price');
        $user_id = Session::get('user_id');
        $total_amt = Session::get('total_price');
        $frame_id=Session::get('frame_id');
        $total_tile = Session::get('total_tile');

        $tile_image = Session::get('tile_image');
        $quantity = Session::get('quantity');

        $payment = Session::get('payment');

        $rotation = Session::get('rotation');

        $color = Session::get('color');

        $number_people = Session::get('number_people');


        $wallet_amount = !empty(Session::get('wallet_amount'))? Session::get('wallet_amount'):'';
        
        $coupon_code = !empty(Session::get('coupon_code'))? Session::get('coupon_code'):'';


        $country_info = DB::table('shipping_address')->where('id',$address_id)->first();
        $country_name = $country_info->country;
        $day_info = DB::table('shipping_price')->where('country_id',$country_name)->first();
        //$ship_price = $day_info->price;

        $ship_price = $ship_price;

        $delivery_days = $day_info->delivery_days;
        $current_date = date('Y-m-d');
        $delivery_date = date('Y-m-d',strtotime($current_date.'+ ' .$delivery_days. 'days'));


        $referral_info = DB::table('my_referrals')->where('refferal_id', $user_id)->where('status',1)->first();

       
        if(!empty($referral_info)){

            $accepted_id  = $referral_info->refferal_id;

            $check_orders = DB::table('orders')->where('user_id', $accepted_id)->get();

            $order_total = 0;

            foreach ($check_orders as $amt) {
                $order_total += $amt->total_price;
            }

            if(!empty($check_orders)){
                    $order_count = count($check_orders); 
                    $comm = DB::table('referral_commission_settings')->where('status',1)->first();
                    $no_of_orders = $comm->no_of_orders;
                    $order_amt =   $comm->order_amount;
                    $ref_amt =  $comm->referral_amount;

                if(($no_of_orders <= $order_count) && ($order_amt <= $order_total)) {

                    $wallet_info = DB::table('users')->where('id', $user_id)->where('status',1)->first();

                    $wallet_balance = $wallet_info->wallet_balance + $ref_amt;

                    $updateData = DB::table('users')->where('id',$user_id )->update(['wallet_balance' =>$wallet_balance]);

                    $statusData = DB::table('my_referrals')->where('refferal_id',$user_id )->update(['status' =>0]);


                    $trans_info = DB::table('wallet_transaction')->where('user_id', $user_id)->where('status',1)->first();

                    if(!empty($trans_info)){

                        $credit_amount = $trans_info->credit_amount + $ref_amt;

                        $updateData = DB::table('wallet_transaction')->where('user_id',$user_id )->update(['credit_amount' =>$credit_amount]);
                        
                    }else{
                        DB::table('wallet_transaction')->insert(
                            [
                                'user_id' => $user_id,
                                'credit_amount' =>  $ref_amt,
                                'debit_amount' =>  0,
                                'status' => 1,
                                'created_at' =>  date('Y-m-d H:i:s'),
                                
                            ]

                        );
                    }  
                }

            }

        }


        $wallet_discount = 0;

        if(!empty($wallet_amount)){

            $wallet_disc  = $total_amt + $ship_price - $wallet_amount;

            $wallet_discount =  $total_amt + $ship_price - $wallet_disc;             

            $wallet_info = DB::table('users')->where('id', $user_id)->where('status',1)->first();

            $wallet_bal = $wallet_info->wallet_balance;

            $updated_balance = $wallet_bal - $wallet_amount;

            $update_wallet_amount = DB::table('users')->where( 'id', $user_id)->update( ['wallet_balance' =>$updated_balance] );

            $trans_info = DB::table('wallet_transaction')->where('user_id', $user_id)->where('status',1)->first();
            if(!empty($trans_info)){
                if($trans_info->debit_amount == 0){
                    $debit_amount =  $trans_info->credit_amount - $wallet_amount  ;
                }else{
                    $debit_amount =  $trans_info->debit_amount - $wallet_amount  ;
                }
                

                $update_trans_amount = DB::table('wallet_transaction')->where( 'user_id', $user_id)->update( ['debit_amount' =>$debit_amount]);
            }

        }

        $coupon_discount = 0;

        if(!empty($coupon_code)){

            $coupon_info = DB::table('coupon')->where('code',$coupon_code)->where('status',1)->first();
            $type = $coupon_info->type;
            $coupon_value = $coupon_info->value;

            if($type == 'percentage'){
                 
                $disc_price = ($total_amt * $coupon_value)/100;
                $coupon_disc = $total_amt - $disc_price ;
                $coupon_discount = $total_amt - $coupon_disc;
            }else{
                $coupon_disc = $total_amt - $coupon_value ;
                $coupon_discount = $total_amt - $coupon_disc;
            }

        }
        
        if(!empty($coupon_discount && $wallet_discount)){

            $total_price = ($total_amt + $ship_price) - ($coupon_discount - $wallet_discount);

        }elseif(!empty($coupon_discount)){

            $total_price = ($total_amt + $ship_price) - $coupon_discount;

        }elseif(!empty($wallet_discount)){

            $total_price = ($total_amt + $ship_price) - $wallet_discount;

        }else{
            // echo $total_amt; 
            // echo '<pre>';
            // echo $ship_price;
            $total_price = $total_amt + $ship_price;
            

        }


        DB::table('orders')->insert(
                [
                  'user_id' =>  $user_id,
                  'address_id' =>  $address_id,
                  'frame_id' =>  $frame_id,
                  'total_tile' =>  $total_tile,
                  'total_price' =>  $total_price,
                  'coupon_discount' =>  $coupon_discount,
                  'wallet_discount' =>  $wallet_discount,
                  'shipping_price' =>  $ship_price,
                  'coupon_code' => !empty($coupon_code)? $coupon_code:'',
                  'is_applied_coupon' => !empty($coupon_code)? '1':'0',
                  'delivery_date' =>  $delivery_date,
                  'order_status' =>  'pending',
                  'vendor_id' => 0,
                  'status' => 1,
                  'number_people'=>$number_people,
                  'payment_type'=>$payment,
                  'color' => $color,
                  'created_at' =>  date('Y-m-d H:i:s')
                ]
            );

            $order_id = DB::getpdo()->lastInsertId();
            $frame_detail = DB::table('frames')->where('id',$frame_id)->first();
            if(!empty($order_id)){

                $check_qty = DB::table('frames')->where('id',$frame_id)->first();
                $frame_qty = $check_qty->quantity;
                $order_qty = $total_tile;

                $updated_qty = $frame_qty - $order_qty;

                $updateData = DB::table('frames')->where('id',$frame_id )->update(['quantity' => $updated_qty]);

                $tracking_code = Helper::generateRandomString(6);

                DB::table('order_tracking')->insert(
                    [
                      'user_id' =>  $user_id,
                      'order_id' =>  $order_id,
                      'tracking_code' =>  $tracking_code,
                      'tracking_status' =>  1 ,
                      'created_at' =>  date('Y-m-d H:i:s')
                    ]
                );
            }

            
            
            if(!empty($rotation)){
                $rotation = $rotation;
            }else{
                $rotation = 0;
            }
            $check = true;

            if($tile_image && $quantity){

                for ($i = 0; $i < count($tile_image); ++$i) {
                    $data[] = [
                            'order_id'=>$order_id, 'user_id'=> $user_id, 
                            'tile_image' => $tile_image[$i], 'quantity' => $quantity[$i], 'rotation'=>$rotation[$i],
                            'created_at' =>  date('Y-m-d H:i:s')
                        ];
                }

                $check = DB::table('order_detail')->insert($data);
                $res = DB::table('temp_imgdata')->where('user_id', '=', $user_id)->delete();
            }


            $today = date("Ymd");



            $unique = '#'.$today . $order_id;



            $orderidupdate = DB::table('orders')->where('id', $order_id)->update(['order_id' => $unique]);


            if( $check ){

                $users = User::where('id','=',$user_id)->first();
                $data['user_info'] = $users;
                $data['url'] = url('/');
                $data['order_info'] =  DB::table('orders')
                ->select('frames.images','frames.currency','users.first_name', 'users.last_name', 'orders.id', 'orders.total_tile', 'orders.total_price', 
                    'orders.shipping_price', 'orders.coupon_discount', 'orders.wallet_discount', 'orders.address_id','orders.frame_id', 'orders.order_status', 'orders.delivery_date', 'orders.status', 'orders.created_at','.shipping_address.fullname','shipping_address.street_address','shipping_address.city','shipping_address.state','countries.name as country','shipping_address.zip_code','shipping_address.contact_number')
                ->join('users','users.id','=','orders.user_id')
                ->join('frames','frames.id','=','orders.frame_id')
                ->join('shipping_address','shipping_address.id','=','orders.address_id')
                ->join('countries','countries.id','=','shipping_address.country')
                ->where('orders.id', $order_id)
                ->where('users.status', 1)
                ->first();
                $data['order_details'] =  DB::table('order_detail')
                ->select('frames.title','frames.images','frames.price','frames.currency', 'order_detail.order_id', 'order_detail.tile_image', 'order_detail.quantity',  'orders.address_id', 
                  'orders.frame_id', 'orders.shipping_price', 'shipping_price.currency as shipping_currency', 'orders.order_status', 
                    'orders.total_price', 'orders.coupon_discount', 'orders.wallet_discount' , 'orders.delivery_date')
                ->join('orders','orders.id','=','order_detail.order_id')
                ->join('frames','frames.id','=','orders.frame_id')
                ->join('shipping_address','shipping_address.id','=','orders.address_id')
                ->join('shipping_price','shipping_price.country_id','=','shipping_address.country')
                ->where('order_detail.order_id',$order_id)
                ->get();

                if ($_SERVER['SERVER_NAME'] != 'localhost') {

                    $fromEmail = Helper::getFromEmail();
                    $inData['from_email'] = $fromEmail;
                    $inData['email'] = $users->email;
                    Mail::send('emails.invoice.invoice_template', $data, function ($message) use ($inData) {
                        $message->from($inData['from_email'],'FRAMEiT');
                        $message->to($inData['email']);
                        $message->subject('FRAMEiT - Order Confirmation Mail');
                    });

                }

                
                Session::get('total_amt');
                session::flash('message', 'Order created Succesfully.');
                return redirect('/category/'.$frame_detail->category_id.'');

            }else{
                session::flash('error', 'Record not inserted.');
                return redirect('/category/'.$frame_detail->category_id.''); 
            }
        
        Session::get('total_amt');
        session::flash('message', 'Order created Succesfully.');
        return redirect('/category/'.$frame_detail->category_id.'');



    }

     public function payment_fail(Request $request) {
        $frame_id = Session::get('frame_id');
        $frame_detail = DB::table('frames')->where('id',$frame_id)->first();
        session::flash('message', 'Payment Failed');
        return redirect('/category/'.$frame_detail->category_id.'');
    }

     public function payment_cancel(Request $request) {

       $frame_id = Session::get('frame_id');
        $frame_detail = DB::table('frames')->where('id',$frame_id)->first();
        session::flash('message', 'Payment Cancel');
        return redirect('/category/'.$frame_detail->category_id.'');
    }

	public function save_image_quantity(Request $request) {

		$id = $request->id;
		$qty = $request->qty;
		$updateData = DB::table('temp_imgdata')->where('id',$id )->update(['quantity' =>$qty]);
		if($updateData){
			return response()->json(['status'=>'success', 'msg' => 'Record added successully']);
		}else{
			return response()->json(['status'=>'error', 'msg' => 'Something went wrong']);
		}

	}

    public function save_rotation(Request $request) {

        $id = $request->rotate_id;
        $rotate = $request->rotate_deg;
        $updateData = DB::table('temp_imgdata')->where('id',$id )->update(['rotation' =>$rotate]);
        if($updateData){
            return response()->json(['status'=>'success', 'msg' => 'Record added successully']);
        }else{
            return response()->json(['status'=>'error', 'msg' => 'Something went wrong']);
        }

    }

	public function save_frame(Request $request) {

		$user_id = $request->user_id;
		$frame_id = $request->frame_id;

        $frame = DB::table('frames')->select('*')->where('id',$request->frame_id)->first();
        if($frame->quantity > 0){

		  $updateData = DB::table('temp_imgdata')->where('user_id',$user_id)->update(['frame_id' =>$frame_id]);
            if($updateData){
                return response()->json(['status'=>'success', 'msg' => 'Record added successully']);
            }else{
                return response()->json(['status'=>'error', 'msg' => 'Something went wrong']);
            }
        }else{
            return response()->json(['status'=>'error', 'msg' => 'Out of Stock']);
        }
		

	}


		public function save_frame_mo(Request $request) {

          $forminput =  $request->all();
            //print_r($forminput);die;
            $validator = Validator::make($request->all(), [ 
                'user_id'  => 'required',
                //'address_id'  => 'required',
                'frame_id'  => 'required',
                'total_tile'  => 'required',
                'total_price'  => 'required',
                //'tile_image.*' => 'mimes:jpeg,png,jpg,gif,svg|max:5000'
                ],

                [   
                    'user_id.required'     => 'required user_id',
                    //'address_id.required'     => 'required address_id',
                    'frame_id.required'     => 'required frame_id',
                    'total_tile.required'     => 'required total_tile',
                    'total_price.required'     => 'required total_price',
                ]
            );
            if ($validator->fails()) { 
                $messages = $validator->messages();
                foreach ($messages->all() as $message)
                {   
                    return response()->json(['status'=>$this->failureStatus,'msg'=>$message]);
                }            
            }

            $user_id = !empty($forminput['user_id'])? $forminput['user_id']:'';
            // $wishlist_id = !empty($forminput['wishlist_id'])? $forminput['wishlist_id']:'';
            // if(!empty($wishlist_id) && !empty($user_id)){
            //     $res = DB::table('wishlists')->where('id', '=', $wishlist_id)->where('user_id', '=', $user_id)->delete();

            //     if ($res) {
            //         $rest = DB::table('wishlist_detail')->where('wishlist_id', '=', $wishlist_id)->delete();
            //     }
            // }

            $total_amt=$forminput['total_price'];
            $coupon_code=$forminput['coupon_discount'];
            $coupon_discount = 0;
            if(!empty($coupon_code)){
                $coupon_info = DB::table('coupon')->where('code',$coupon_code)->where('status',1)->first();
                $type = $coupon_info->type;
                $coupon_value = $coupon_info->value;
                if($type == 'percentage'){
                    $disc_price = ($total_amt * $coupon_value)/100;
                    $coupon_disc = $total_amt - $disc_price ;
                    $coupon_discount = $total_amt - $coupon_disc;

                }else{
                    $coupon_disc = $total_amt - $coupon_value ;
                    $coupon_discount = $total_amt - $coupon_disc;
                }

            }
            
            if(empty($forminput['address_id'])){
               $address = DB::table('users')->where('id',$forminput['user_id'])->first(); 
               $forminput['address_id'] = $address->ship_id;
            }else{
                $forminput['address_id']=$forminput['address_id'];
            }

            $country_info = DB::table('shipping_address')->where('id',$forminput['address_id'])->first();
            $country_name = $country_info->country;
            $day_info = DB::table('shipping_price')->where('country_id',$country_name)->first();
            $ship_price = $day_info->price;
            if(!empty($ship_price)){

                    $ship_price=$ship_price;

            }else{

                $ship_price=0;

            }
            $wallet_amount = $forminput['wallet_amount'];
            $wallet_discount = 0;
            if(!empty($wallet_amount)){

                $wallet_disc  = $total_amt + $ship_price - $wallet_amount;

                $wallet_discount =  $total_amt + $ship_price - $wallet_disc;             

            }

            DB::table('wishlists')->insert(
                [

                  'user_id' =>  $forminput['user_id'],
                  'address_id' =>  $forminput['address_id'],
                  'frame_id' =>  $forminput['frame_id'],
                  'total_tile' =>  $forminput['total_tile'],
                  'total_price' =>  $forminput['total_price'],
                  'coupon_discount' =>  number_format((float)$coupon_discount, 2),
                  'wallet_discount' =>  number_format((float)$wallet_discount, 2),
                  'shipping_price' =>  $ship_price,
                  'coupon_code' => !empty($coupon_code)? $coupon_code:'',
                  'is_applied_coupon' => !empty($coupon_code)? '1':'0',
                  'created_at' =>  date('Y-m-d H:i:s'),
                  'status' => 1,
                ]
            );
            $order_id = DB::getpdo()->lastInsertId();
            $imagename = array();
            $quantity = array();
            //$filename = $request->image_data;
            $filename = DB::table('temp_imgdata')->where('user_id',$forminput['user_id'])->where('status',0)->get();
            //print_r($filename); die;
            $check = true;
            if(!empty($filename)){
                foreach ($filename as $value) {
                    $name = $value->filename;
                    $qty = $value->quantity;
                    $rotate = $value->rotation;
                    // $imagename[]=$name;
                    // $quantity[]=$qty;
                    // $rotation[]=$rotate;
                    $data[] = [
                        'wishlist_id'=>$order_id, 'user_id'=> $forminput['user_id'], 
                        'tile_image' => $name, 'quantity' => $qty, 'status' => 1,
                        'rotation'=>$rotate,
                        'created_at' =>  date('Y-m-d H:i:s')
                    ];
                }
                $check = DB::table('wishlist_detail')->insert($data);
               // DB::table('temp_imgdata')->where('user_id',$forminput['user_id'])->where('status',0)->update( ['status' =>1] );
                DB::table('temp_imgdata')->where('user_id', '=', $forminput['user_id'])->where('status',0)->delete();
            }
            // $media_json = array();
            // $files=$request->file('tile_image');
            // if($files=$request->file('tile_image')){
            //     foreach($files as $file){
            //         $name = time().'_'.$file->getClientOriginalName();
            //         $destinationPath = public_path('/uploads/order_images');
            //         $imagePath = $destinationPath. '/'.  $name;
            //         $jsonPath = url('/public/uploads/order_images'). '/'.  $name;
            //         $file->move( $destinationPath,$imagePath );
            //     }
            // }
            if( $check ){
                return response()->json(['status'=>'success', 'msg' => 'Wishlist created successfully', 'response'=>['user_id' => $request->user_id, 'wishlist_id' => $order_id  ]]);
            }else{
                return response()->json(['status'=>'error', 'msg' => 'Something went wrong']); 
            }   

		// $user_id = $request->user_id;

		// $updateData = DB::table('temp_imgdata')->where('user_id',$user_id )->update(['status' =>1]);

		// if($updateData){

		// 	return response()->json(['status'=>'success', 'msg' => 'Frames Saved to My Order']);

		// }else{

		// 	return response()->json(['status'=>'error', 'msg' => 'Something went wrong']);

		// }

	}

	// 	public function save_frame_order(Request $request) {

	// 	$user_id = $request->user_id;

	// 	$updateData = DB::table('temp_imgdata')->where('user_id',$user_id )->update(['status' =>0]);

	// 	if($updateData){

	// 		return response()->json(['status'=>'success', 'msg' => 'Frames Saved to My Order']);

	// 	}else{

	// 		return response()->json(['status'=>'error', 'msg' => 'Something went wrong']);

	// 	}

	// }

    public function save_frame_order(Request $request) {
        $userid = Auth::user()->id;
        $wishlist_id = $request->id;

        $filename = DB::table('wishlist_detail')->where('user_id',$userid)->where('wishlist_id',$wishlist_id)->get();

        $frame_id = DB::table('wishlists')->where('user_id',$userid)->where('id',$wishlist_id)->first();

            //print_r($filename); die;
        $data=array();
            $check = true;
            if(!empty($filename)){
                foreach ($filename as $value) {
                    $name = $value->tile_image;
                    $qty = $value->quantity;
                    $rotate = $value->rotation;
                    
                    $data[] = [
                        'user_id'=> $userid, 
                        'filename' => $name, 'quantity' => $qty, 'status' => 0,
                        'rotation'=>$rotate,'frame_id' => $frame_id->frame_id,
                        'created_at' =>  date('Y-m-d H:i:s')
                    ];

                }
                $check = DB::table('temp_imgdata')->insert($data);
            }
            
            DB::table('wishlists')->where('id', '=', $wishlist_id)->delete();
            DB::table('wishlist_detail')->where('wishlist_id', '=', $wishlist_id)->delete();
        if($check){

            return redirect('/select_frame');

        }else{

           // response()->json(['status'=>'error', 'msg' => 'Something went wrong']);
            return redirect('/save_frame_list');

        }

    }



	public function multiple_uploads(Request $request)
	{

		$this->validate($request, [
                'cat_id'=>'required',
				'user_id' => 'required',
                'filename' => 'required',
                'frame_id' => 'required',
                'filename.*' => 'image|mimes:jpeg,png,jpg,gif,svg'

        ]);


        $frame = DB::table('frames')->select('*')->where('category_id',$request->cat_id)->first();

        if($frame->quantity > 0){
        $check = true;
            if($request->hasfile('filename'))
             {

                foreach($request->file('filename') as $image)
                {

                    $name = time().'_'.$image->getClientOriginalName();

                    $destinationPath = public_path('/uploads/order_images');
                    $imagePath = $destinationPath. '/'.  $name;

                    $jsonPath = url('/public/uploads/order_images'). '/'.  $name;
                    $image->move( $destinationPath,$imagePath );

                    
                    $filename = $imagePath;
                    $this->correctImageOrientation($filename);
                    

                    $data[] = [
    	                        'user_id'=> $request->user_id, 'quantity' => 1, 
    	                        'filename' => $name, 'thumb_filename'=>$name, 'frame_id' => $frame->id, 'rotation'=>0,'status'=>0,
    	                        'created_at' =>  date('Y-m-d H:i:s')
    	                    ]; 

                    $datad = array(
                    'user_id'=> $request->user_id, 
                    'quantity' => 1,             
                    'filename' => $name, 
                    'thumb_filename'=>$name, 
                    'frame_id' => $frame->id, 
                    'rotation'=>0,
                    'status'=>0,
                    'created_at' => date('Y-m-d H:i:s')
                  );
                          
                }

                
                    $checkexit = DB::table('temp_imgdata')->where('user_id',$request->user_id)->where('frame_id',$frame->id)->get();

                    if(count($checkexit)>0){

                    $datanew =  $datad;       

                    $check = DB::table('temp_imgdata')->where('user_id',$request->user_id)->where('frame_id',$frame->id)->update($datad);      
                   
                    }else{

                    $datanew =  $data;       
                        
                     $check = DB::table('temp_imgdata')->insert($data);  

                    }        

                    // print_r($check);
                    // die;

                    return $datanew;
                   
                    
                    if ($check) {
                        return $data['model']=1;
                        
        				//return redirect('/select_frame');
        			} else {
        				//return redirect('/select_frame');
        			}
                
                
                 }
        }else{
           
          session::flash('error', 'Out of stock');
                return redirect('/select_frame');
        }
	}

  public  function correctImageOrientation($filename) {
  if (function_exists('exif_read_data')) {
    $exif = @exif_read_data($filename);
    if($exif && isset($exif['Orientation'])) {
      $orientation = $exif['Orientation'];
      if($orientation != 1){
        $img = imagecreatefromjpeg($filename);
        $deg = 0;
        switch ($orientation) {
          case 3:
            $deg = 180;
            break;
          case 6:
            $deg = 270;
            break;
          case 8:
            $deg = 90;
            break;
        }
        if ($deg) {
          $img = imagerotate($img, $deg, 0);        
        }
        // then rewrite the rotated image back to the disk as $filename 
        imagejpeg($img, $filename, 95);
      } // if there is some rotation necessary
    } // if have the exif orientation info
  } // if function exists      
}


public function ajax_select_frame(Request $request) {

        if(@Auth::user()->id){
            $user_id=Auth::user()->id;
            $cat_id = $request->cat_id;
            $data['tmpImgData'] = DB::table('temp_imgdata')
            ->select('temp_imgdata.id','temp_imgdata.filename' ,'temp_imgdata.thumb_filename' ,'temp_imgdata.frame_id','temp_imgdata.quantity','frames.images','frames.price','frames.currency','temp_imgdata.img_top','temp_imgdata.img_left')
            ->leftjoin('frames','frames.id','=','temp_imgdata.frame_id')
            ->where('temp_imgdata.user_id',$user_id)
            ->where('frames.category_id',$cat_id)
            ->where('temp_imgdata.status',0)
            ->orderBy('temp_imgdata.id','DESC')
            ->get();
            
        }else{
        	$token = csrf_token();
            $cat_id = $request->cat_id;
        	$data['tmpImgData'] = DB::table('temp_imgdata')
            ->select('temp_imgdata.id','temp_imgdata.filename' ,'temp_imgdata.thumb_filename' ,'temp_imgdata.frame_id','temp_imgdata.quantity','frames.images','frames.price','frames.currency','temp_imgdata.img_top','temp_imgdata.img_left')
            ->leftjoin('frames','frames.id','=','temp_imgdata.frame_id')
            ->where('temp_imgdata.user_id',$token)
            ->where('frames.category_id',$cat_id)
            ->where('temp_imgdata.status',0)
            ->orderBy('temp_imgdata.id','DESC')
            ->get();
        }

        return response()->json(['status'=>$this->successStatus, 
                'msg' => 'Çerçeve listesi başarıyla',
                'response'=> ['frameLists' => $data]
            ]);

        // $data['coupon'] = DB::table('coupon')->where('status','=',1)->get();

        //return view('user/select_frame',$data);
    }



	public function multiple_uploads_social(Request $request)
	{

         $frame = DB::table('frames')->select('*')->where('category_id',$request->cat_id)->first();

         if($frame->quantity > 0){

         if(isset(Auth::user()->id)){

            $userid = Auth::user()->id;

          }else{

           $userid = csrf_token();

          } 

            $socialimg = json_decode($request->images);

            $frame_id = $frame->id;

            $cat_id = $request->cat_id;


            foreach($socialimg as $image)
            {

            $filedata = file_get_contents($image);

		    $name = uniqid().'.jpg';

		    $file_path = 'public/uploads/order_images/'.$name ;

		    file_put_contents($file_path, $filedata);

		    }


            $data[] = [
            'user_id'=> $userid, 'quantity' => 1, 
            'filename' => $name, 'thumb_filename'=>$name, 'frame_id' => $frame_id, 'rotation'=>0, 'status'=>0,
            'created_at' =>  date('Y-m-d H:i:s')
            ]; 

            $datad = array(
                'user_id'=> $userid, 
                'quantity' => 1,             
                'filename' => $name, 
                'thumb_filename'=>$name, 
                'frame_id' => $frame_id, 
                'rotation'=>0,
                'status'=>0,
                'created_at' => date('Y-m-d H:i:s')
              );    


        $checkexit = DB::table('temp_imgdata')->where('user_id',$userid)->where('frame_id',$frame_id)->get();

        if(count($checkexit)>0){

        $datanew =  $datad;   

        $check = DB::table('temp_imgdata')->where('user_id',$userid)->where('frame_id',$frame_id)->update($datad);      
           
        }else{

        $datanew =  $data;    
                
        $check = DB::table('temp_imgdata')->insert($data);  

        }        

        return $datanew;

          }else{
           
                session::flash('error', 'Out of stock');
                return $cat_id;

         } 
            
           /* if ($check) {
                $data['model']=1;
				return redirect('/select_frame/'.$cat_id);
			} else {
				return redirect('/select_frame/'.$cat_id);
			} */
                
          
	}

	
	public function user_profile(Request $request) {
        $user_id = Auth::user()->id;
        $data['user'] = User::where('id', $user_id)->where('status', 1)->first();
        $data['user_id']=Auth::user()->id;
        $data['countries'] = DB::table('countries')->get();

         $data['addressList'] =  DB::table('shipping_address')
        ->select('shipping_address.id', 'shipping_address.address_type', 'shipping_address.fullname','shipping_address.street_address','shipping_address.city','shipping_address.state',
            'countries.name as country', 'shipping_address.country as country_id' ,'shipping_address.zip_code','shipping_address.contact_number','shipping_price.price as shipping_price' ,'shipping_price.currency as shipping_currency', 'shipping_price.delivery_days', 'shipping_address.status', 'shipping_address.created_at')
        ->leftjoin('shipping_price','shipping_price.country_id','=','shipping_address.country')
        ->leftjoin('countries','countries.id','=','shipping_address.country')
        ->where('user_id',$user_id)
        ->get();



		return view('user/profile')->with($data);
	}
	
	public function edit_profile(Request $request) {

        $user_id = Auth::user()->id;
        $data['orderList'] =  DB::table('order_detail')
        ->select('frames.price', 'frames.images','frames.alignment','frames.type','frames.title','order_detail.order_id', 'orders.order_id as customer_order_id' ,'order_detail.user_id', 'order_detail.tile_image', 'order_detail.quantity','orders.number_people', 'orders.total_tile',  'orders.total_price', 'orders.coupon_discount', 'orders.wallet_discount', 'orders.shipping_price', 'orders.address_id', 'orders.color', 'orders.frame_id','orders.order_status','order_detail.rotation', 'orders.delivery_date', 'order_tracking.tracking_code', 'orders.created_at')
        ->join('orders','orders.id','=','order_detail.order_id')
        ->join('frames','frames.id','=','orders.frame_id')
        ->join('order_tracking','order_tracking.order_id','=','orders.id')
        ->where('orders.user_id', $user_id)
        ->orderBy('orders.order_id', 'DESC')
        ->get();

        $data['shipaddress'] = DB::table('shipping_address')->where('user_id','=',$user_id)->get();



        $data['order'] =  DB::table('orders')
        ->select('frames.title','orders.id','orders.user_id', 'orders.color', 'orders.frame_id', 'orders.number_people', 'orders.total_tile',  'orders.total_price', 'orders.coupon_discount',
         'orders.wallet_discount','orders.shipping_price', 'orders.order_status', 'orders.delivery_date', 'order_tracking.tracking_code', 
         'orders.created_at')
        ->join('order_tracking','order_tracking.order_id','=','orders.id')
        ->join('frames','frames.id','=','orders.frame_id')
        ->where('orders.user_id', $user_id)
        ->orderBy('orders.order_id', 'DESC')
        ->get();


        

        // echo '<pre>';
        // print_r($data['orderList']);
        // die;

		return view('user/edit_profile')->with($data);
	}


 public function my_order(Request $request) {

        $user_id = Auth::user()->id;

		if(Auth::user()->id){

			$user_id=Auth::user()->id;
			$data['tmpImgData'] = $wishList =  DB::table('wishlists')
            ->select('frames.title','wishlists.id','wishlists.user_id', 'wishlists.frame_id', 'wishlists.total_tile',  'wishlists.total_price', 'wishlists.coupon_discount','wishlists.wallet_discount','wishlists.shipping_price', 'wishlists.created_at')
            ->join('frames','frames.id','=','wishlists.frame_id')
            ->where('wishlists.user_id', $user_id)
            ->get();

            if(!empty($data['tmpImgData'])){
                $data['tmpImgData'] = $data['tmpImgData'];
            }else{
                $data['msg'] ="Data not found";
            }
			
		}

        $data['msg']="";

		return view('user/my_order')->with($data);

	}


     public function save_frame_detail(Request $request){
       return view('user/save_frame_detail');
    }

    public function changeemail(Request $request) {

        $user_id = $request->user_id;
        $email = $request->email;
        $usersemail = DB::table('users')->where('email','=',$email)->first();

        if(empty($usersemail)){
            $credentials = array(
                    'email' => $email,
                );
            $res = DB::table('users')->where('id',$user_id )->update( $credentials );
        
            if ($res) {
                
                return json_encode(array('status' => 'success','msg' => 'Email has updated successfully!'));
            } else {
                return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
            }
        }else{
             return json_encode(array('status' => 'error','msg' => 'Email id already exists'));
        }
        
    }

    public function sendrefferalcode(Request $request) {

        $name = $request->name;
        $referral_email_from = $request->referral_email_from;
        $referral_url = $request->referral_url;
        //return json_encode(array('status' => 'success','msg' => $referral_email_from));
        $searchArray = array("{name}","{user_email}");
      
        $replaceArray = array($name,  $referral_email_from);

        $email_content = "<h3>Hello, {name} <br>
        Share referral URL by your friend ".Auth::user()->first_name." ".Auth::user()->last_name."
        <a href='".$referral_url."'>click here</a> 
        ";

        $content = str_replace($searchArray, $replaceArray, $email_content);
       
        $data = [
            'name'      => $name,
            'email'     => $referral_email_from,                 
            'subject'   => "FrameIT Refferal code",
            'content'   => $content,
        ];


        
        send_mail($data);
        return json_encode(array('status' => 'success','msg' => 'Send successfully!'));
    }


     public function changecontactnumber(Request $request) {

        $user_id = $request->user_id;
        $contact = $request->contact;
        

       
            $credentials = array(
                    'contact_number' => $contact,
                );
            $res = DB::table('users')->where('id',$user_id )->update( $credentials );
        
            if ($res) {
                
                return json_encode(array('status' => 'success','msg' => 'Contact number has updated successfully!'));
            } else {
                return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
            }
        
        
    }


    public function changefullname(Request $request) {

        $user_id = $request->user_id;
        $fullname = $request->fullname;
        
        $name = explode(" ", $fullname);

        if(!empty($name[1])){
           $name[1] = $name[1]; 
        }else{
            $name[1] = "";
        }
       
            $credentials = array(
                    'first_name' => $name[0],
                    'last_name'=> $name[1],
                );
            $res = DB::table('users')->where('id',$user_id )->update( $credentials );
        
            if ($res) {
                
                return json_encode(array('status' => 'success','msg' => 'Name has updated successfully!'));
            } else {
                return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
            }
        
        
    }

	public function change_password(Request $request) {

		return view('user/change_password');
	}

	public function fqa_page(Request $request)
	{
		return view('user/faqpage');
	}

	public function delete_preview_image(Request $request){

		$delete_id = $request->delete_id;

		$frame_info = DB::table('temp_imgdata')->where('id','=',$delete_id)->first();
		$file = $frame_info->filename;
		$media_url = url('/').'/public/uploads/order_images/'.$file;
		$res = DB::table('temp_imgdata')->where('id', '=', $delete_id)->delete();

		if ($res) {
			if(file_exists($media_url)){
				@unlink($media_url);
			}
			return json_encode(array('status' => 'success','msg' => 'Image has been deleted successfully!'));
		} else {
			return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
		}
	}

     public function step1(Request $request) { 

        return view('user/step1');
    }
    public function step1_store(Request $request) { 
        $request->session()->put('pfor', $request->input('pfor')); 
        $pfor=$request->session()->get('pfor');
        
        return response()->json(['status'=>$this->successStatus, 'response'=>$pfor]);
    }
    public function step2(Request $request) { 
        $request->session()->get('pfor');
        return view('user/step2');
    }
    public function step2_store(Request $request) { 

        $request->session()->put('pfor', $request->input('pfor')); 
        $request->session()->get('pfor'); 
        
        $request->session()->put('firstName', $request->input('firstName')); 
        $request->session()->get('firstName');

        return redirect('/step_3');
    }

    public function step3(Request $request) { 

        return view('user/step3');
    }

    
    
    public function step3_store(Request $request) { 

            $device_id =  '';

            $device_token =  '';

            $device_type =  '';


            $pfor = $request->session()->get('pfor');
            $name = $request->session()->get('firstName');
            $email = $request->email;


            
            $name_arr = explode(' ', $name);

            //Print_r($name_arr);


            // print_r($email);
            // die;

            $obj = new User;

            $obj->first_name = (!empty($name_arr[0]) ? $name_arr[0] : '');

            $obj->last_name = (!empty($name_arr[1]) ? $name_arr[1] : '');

            $obj->email = $email;

            $password = 123456;
             $obj->password = bcrypt($password);

            $obj->role_id = 2;

            $obj->guest_login = 1;

            $obj->device_id = $device_id;

            $obj->device_token = $device_token;

            $obj->device_type = $device_type;

           $obj->purches_for = $pfor;

            $obj->is_verify_email = 1;

            $obj->is_verify_contact = 1;

            

            $obj->wallet_balance = 0;

            $obj->register_by = 'web';

            

            $obj->status = 1;

            $obj->created_at = date('Y-m-d H:i:s');

            $obj->updated_at = date('Y-m-d H:i:s');

           

            $email = DB::table('users')->where('email',$email)->first();

            



            if (empty($email)) {
                $res = $obj->save();
                $id=$obj->id;

                $credentials = array(

                    'email' => $request->email,

                    'password' => $password

                ); 
                if (Auth::attempt($credentials)){
                }


                //  return response()->json(['status'=>$this->successStatus, 
                // 'msg' => 'Account has been created successfully !', 'email'=>$request->email]);
                return redirect('/category');
              }else{

               
                $credentials = array(

                    'email' => $request->email,

                    'password' => $password

                ); 
                if (Auth::attempt($credentials)){
                }


                 //return response()->json(['status'=>$this->successStatus,'email'=>$request->email]); 
                 return redirect('/category');
              }


        } 


	public function select_frame(Request $request) {

		$cat_id = $request->segment(2);
		// echo '<pre>';
		// print_r($cat_id);
		// die;

		if(isset(Auth::user()->id)){
			$user_id=Auth::user()->id;
			$data['tmpImgData'] = DB::table('temp_imgdata')
			->select('temp_imgdata.id','temp_imgdata.filename','temp_imgdata.thumb_filename','temp_imgdata.frame_id','temp_imgdata.quantity','temp_imgdata.rotation','frames.images','frames.alignment','frames.price','frames.title','frames.type','frames.currency','frames.category_id')
			->leftjoin('frames','frames.id','=','temp_imgdata.frame_id')
			->where('temp_imgdata.user_id',$user_id)
			->where('frames.category_id',$cat_id)
			->where('temp_imgdata.status',0)
            ->orderby('temp_imgdata.id','DESC')
			->get();
            // echo '<pre>';
            // print_r($data['tmpImgData']);
            // die;

             $data['countries'] = DB::table('countries')->get();

         $data['addressList'] =  DB::table('shipping_address')
        ->select('shipping_address.id', 'shipping_address.address_type', 'shipping_address.fullname','shipping_address.street_address','shipping_address.city','shipping_address.state',
            'countries.name as country', 'shipping_address.country as country_id' ,'shipping_address.zip_code','shipping_address.contact_number','shipping_price.price as shipping_price' ,'shipping_price.currency as shipping_currency', 'shipping_price.delivery_days', 'shipping_address.status', 'shipping_address.created_at')
        ->leftjoin('shipping_price','shipping_price.country_id','=','shipping_address.country')
        ->leftjoin('countries','countries.id','=','shipping_address.country')
        ->where('user_id',$user_id)
        ->get();
			
		}
        $data['msg']="";

        $data['coupon'] = DB::table('coupon')->where('status','=',1)->get();

        $data['category'] = DB::table('category')->where('cat_status','=',1)->where('cat_id','=',$cat_id)->first();

		return view('user/select_frame',$data);
	}

	public function logout(){
		Auth::logout();
		return redirect('/');
	}

    //priyanka code
    public function privacy_policy(Request $request) {

        return view('user/privacypolicy');
    }
    public function terms_and_condition(Request $request) {

        return view('user/termsandcondition');
    }
    //end priyanka code


     public function payment() {

        return view('payment'); 
    }

     public function category() {
        $data['category'] = DB::table('category')->where('cat_status','=',1)->get();
        return view('user/category')->with($data); 
    }

    public function select_category(Request $request) {
        
        $cat_id = $request->segment(2);

        $guser_cat_id=Session::put('guser_cat_id',$cat_id);

        $data['category'] = DB::table('category')->where('cat_status','=',1)->where('cat_id',$cat_id)->get();
        if(isset(Auth::user()->id)){

            $user_id=Auth::user()->id;

          }else{

           $user_id = csrf_token();

          }    

            $data['tmpImgData'] = DB::table('temp_imgdata')
            ->select('temp_imgdata.id','temp_imgdata.filename','temp_imgdata.thumb_filename','temp_imgdata.frame_id','temp_imgdata.quantity','temp_imgdata.rotation','frames.images','frames.alignment','frames.price','frames.title','frames.type','frames.currency','frames.category_id')
            ->leftjoin('frames','frames.id','=','temp_imgdata.frame_id')
            ->where('temp_imgdata.user_id',$user_id) 
            ->where('frames.category_id',$cat_id)
            //->where('temp_imgdata.frame_id',$cat_id)
            ->where('temp_imgdata.status',0)
            ->orderby('temp_imgdata.id','DESC')
            ->get();
            // echo '<pre>';
            //print_r($data['tmpImgData']);
            //die;

            if(count($data['tmpImgData']) >1){

            $dataremove = DB::table('temp_imgdata')->where('id',$data['tmpImgData'][1]->id)->delete(); 

            $data['tmpImgData'] = DB::table('temp_imgdata')
            ->select('temp_imgdata.id','temp_imgdata.filename','temp_imgdata.thumb_filename','temp_imgdata.frame_id','temp_imgdata.quantity','temp_imgdata.rotation','frames.images','frames.alignment','frames.price','frames.title','frames.type','frames.currency','frames.category_id')
            ->leftjoin('frames','frames.id','=','temp_imgdata.frame_id')
            ->where('temp_imgdata.user_id',$user_id) 
            ->where('frames.category_id',$cat_id)
            //->where('temp_imgdata.frame_id',$cat_id)
            ->where('temp_imgdata.status',0)
            ->orderby('temp_imgdata.id','DESC')
            ->get();

            }


         $data['countries'] = DB::table('countries')->get();

         $data['addressList'] =  DB::table('shipping_address')
        ->select('shipping_address.id', 'shipping_address.address_type', 'shipping_address.fullname','shipping_address.street_address','shipping_address.city','shipping_address.state',
            'countries.name as country', 'shipping_address.country as country_id' ,'shipping_address.zip_code','shipping_address.contact_number','shipping_price.price as shipping_price' ,'shipping_price.currency as shipping_currency', 'shipping_price.delivery_days', 'shipping_address.status', 'shipping_address.created_at')
        ->leftjoin('shipping_price','shipping_price.country_id','=','shipping_address.country')
        ->leftjoin('countries','countries.id','=','shipping_address.country')
        ->where('user_id',$user_id)
        ->get();
            
      

       // print_r($data); die;

        $data['msg']="";

        $data['coupon'] = DB::table('coupon')->where('status','=',1)->get();

        $data['category'] = DB::table('category')->where('cat_status','=',1)->where('cat_id','=',$cat_id)->first();

        return view('user/select_category',$data);
    }

    //end priyanka code


        public function select_deep_category(Request $request) {
        
        $cat_id = $request->cat_id;

        $guser_cat_id=Session::put('guser_cat_id',$cat_id);

        $data['category'] = DB::table('category')->where('cat_status','=',1)->where('cat_id',$cat_id)->get();
        if(isset(Auth::user()->id)){

            $user_id=Auth::user()->id;

          }else{

           $user_id = csrf_token();

          }  

            $data['tmpImgData'] = DB::table('temp_imgdata')
            ->select('temp_imgdata.id','temp_imgdata.filename','temp_imgdata.thumb_filename','temp_imgdata.frame_id','temp_imgdata.quantity','temp_imgdata.rotation','frames.images','frames.alignment','frames.price','frames.title','frames.type','frames.currency','frames.category_id')
            ->leftjoin('frames','frames.id','=','temp_imgdata.frame_id')
            ->where('temp_imgdata.user_id',$user_id) 
            ->where('frames.category_id',$cat_id)
            //->where('temp_imgdata.frame_id',$cat_id)
            ->where('temp_imgdata.status',0)
            ->orderby('temp_imgdata.id','DESC')
            ->get();
            // echo '<pre>';
            //print_r($data['tmpImgData']);
            //die;

            if(count($data['tmpImgData']) >1){

            $dataremove = DB::table('temp_imgdata')->where('id',$data['tmpImgData'][1]->id)->delete(); 

            $data['tmpImgData'] = DB::table('temp_imgdata')
            ->select('temp_imgdata.id','temp_imgdata.filename','temp_imgdata.thumb_filename','temp_imgdata.frame_id','temp_imgdata.quantity','temp_imgdata.rotation','frames.images','frames.alignment','frames.price','frames.title','frames.type','frames.currency','frames.category_id')
            ->leftjoin('frames','frames.id','=','temp_imgdata.frame_id')
            ->where('temp_imgdata.user_id',$user_id) 
            ->where('frames.category_id',$cat_id)
            //->where('temp_imgdata.frame_id',$cat_id)
            ->where('temp_imgdata.status',0)
            ->orderby('temp_imgdata.id','DESC')
            ->get();

            }

         //return  $data['tmpImgData'];   

         $addon_view =  view('user.select_category_ajax', $data)->render();
         //return response()->json( array('addon_view'=>$addon_view) );
        return response()->json($addon_view);   

    }


        public function createShipAddress(Request $request){
        //print_r($request->all()); die();
        $forminput =  $request->all();
        $validator = Validator::make($request->all(), [ 
            'user_id'  => 'required',
            'address_type'  => 'required',
            'fullname'  => 'required',
            'street_address'  => 'required',
            'city'  => 'required',
            'state'  => 'required',
            'country'  => 'required',
            'zip_code'  => 'required',
            'contact_number'  => 'required'
        ]);
        
        if ($validator->fails()) { 
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {   
                return response()->json(['status'=>$this->failureStatus,'msg'=>$message]);            
            }            
        }

        $price_info = DB::table('shipping_price')->where('country_id',$forminput['country'] )->first();
        $price = !empty($price_info->price)? $price_info->price:'';
        $currency = !empty($price_info->currency)? $price_info->currency:'';


        $obj = new ShipModel;
        $obj->user_id = $forminput['user_id'];
        $obj->address_type = $forminput['address_type'];
        $obj->fullname = $forminput['fullname'];
        $obj->street_address = $forminput['street_address'];
        $obj->city = $forminput['city'];
        $obj->state = $forminput['state'];
        $obj->country = $forminput['country'];
        $obj->zip_code = $forminput['zip_code'];
        $obj->contact_number = $forminput['contact_number'];
        $obj->status = 1;
        $obj->created_at = date('Y-m-d H:i:s');
        $res = $obj->save();
        $address_id = $obj->id;

        if( $res ){
            if (!empty($forminput['user_id'])) {
                $credentials = array(
                    'ship_id' => $address_id,
                    'shipping_price' => $price,
                    'shipping_currency' => $currency
                ); 

                $shipData = DB::table('users')->where('id',$forminput['user_id'] )->update( $credentials );
            }
            return response()->json(['status'=>$this->successStatus, 'msg' => 'Record Inserted successfully','response'=>['address_id' =>$address_id ,
                'ship_price' =>$price, 'ship_currency' => $currency]]);
            
        }else{
            return response()->json(['status'=>$this->failureStatus, 'msg' => 'Something went wrong']); 
        }
    }

  


    public function getUserRewardHistory(Request $request){


        $user_id = Auth::user()->id;
        $user_info = DB::table('users')->where('id', $user_id)->where('status',1)->first();
        if(!empty($user_info)){
            $referral_code = $user_info->my_referral_code;
            $referral_url = url('/').'/signup/'.$referral_code;
            $available_credit = number_format((float)$user_info->wallet_balance, 2);
            $redeem_credit = 0;
            // $redeemList =  DB::table('wallet_transaction')
            // ->select('orders.id','wallet_transaction.debit_amount','orders.created_at')
            // ->leftjoin('orders','orders.user_id','=','wallet_transaction.user_id')
            // ->where('wallet_transaction.user_id', $forminput['user_id'])
            // ->get();
             $redeemList =  DB::table('wallettransaction')
            ->select('orders.id','wallettransaction.debit_amount','orders.created_at')
            ->leftjoin('orders','orders.id','=','wallettransaction.order_id')
            ->where('wallettransaction.user_id', $user_id)
            ->get();
            $result = array();
            $results = array();
            foreach( $redeemList as $key => $reward ){
                if($reward->debit_amount > 0){

                    $result['order_id'] =  $reward->id;
                    $result['amount_redeem'] =  number_format((float)$reward->debit_amount, 2);
                    $result['purchase_date'] =  $reward->created_at;
                    $results[] =  $result;

                }

            }

            $ref_info = DB::table('my_referrals')->where('refferal_id', $user_id)->get();
            //$ref_user = $ref_info->user_id;
            // die;
            foreach( $ref_info as $key => $refinfo1 ){

            $rewardList[] =  DB::table('orders')

                ->select('users.first_name','users.last_name','orders.created_at','wallettransaction.credit_amount')

                ->join('users','users.id','=','orders.user_id')

                ->join('wallettransaction','orders.id','=','wallettransaction.order_id')

                ->where('orders.user_id', $refinfo1->user_id)

                ->get();    
            }
            // print_r(!empty($rewardList));

            // die;
            $result1 = array();
            $results1 = array();
            if(!empty($rewardList)){
                //print_r($rewardList);
                foreach( $rewardList as $rewards ){

                    if(!empty($rewards[0]->first_name)){
                        $result1['first_name'] =  $rewards[0]->first_name;
                        $result1['last_name'] =  $rewards[0]->last_name;
                        $result1['credit_reward'] =  number_format((float)$rewards[0]->credit_amount, 2);
                        $result1['referal_order_date'] =  $rewards[0]->created_at;
                        $results1[] =  $result1;
                    }
                }

            }

            $promoList =  DB::table('coupon')
            ->select('coupon.name')
            ->where('code', 'ZxcEq3')
            ->get();

            $result2 = array();
            $results2 = array();
            foreach( $promoList as $key => $promo ){

                $result2['name'] = "10%  Discount on purchase";
                $result2['value'] = "10";
                $result2['type'] =  "percentage";
                $result2['expiry_date'] =  "2020-06-30";
                $result2['code'] =  "ZxcEq3";
                $results2[] =  $result2;
            }

            

         



         $data['referral_url'] =  $referral_url; 
         $data['available_credit'] =  $available_credit;
         $data['redeem_credit'] =  $redeem_credit;
         $data['redeemList'] =  $results;
         $data['rewardList'] =  $results1;
         $data['couponList'] =  $results2;


		  return view('user/my_reward',$data);



        }else{



            return view('user/my_reward',$data);



        }



    }

    function changeprofilepic(Request $request)
    {
         $validation = Validator::make($request->all(), [
          'file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
         ]);
             if($validation->passes())
             {
                  $image = $request->file('file');
                  $new_name = time() . '.' . $image->getClientOriginalExtension();
                  $image->move(public_path('/uploads/profile_image') , $new_name);
                  

                  $user_id=Auth::user()->id;

                  $credentials = array(
                    'profile_pic' => !empty($new_name)? url('public/uploads/profile_image/').'/'.$new_name :url('resources/assets/images/blank_user.jpg'),
                ); 

                $shipData = DB::table('users')->where('id',$user_id )->update( $credentials );
                if($shipData){
                    return response()->json([
                   'message'   => 'Image Upload Successfully',
                   'uploaded_image' => '<img src="public/uploads/profile_image/'.$new_name.'" class="img-thumbnail" style="width: 180px;height: 180px;" />',
                   'class_name'  => 'alert-success'
                  ]);
                }else{
                   return response()->json([
                   'message'   => $validation->errors()->all(),
                   'uploaded_image' => '',
                   'class_name'  => 'alert-danger'
                  ]); 
                }

             }
             else
             {
                  return response()->json([
                   'message'   => $validation->errors()->all(),
                   'uploaded_image' => '',
                   'class_name'  => 'alert-danger'
                  ]);
             }
    }

    public function contact_us(Request $request) {

        return view('user/contact_us');
    }

     public function imageCropPost(Request $request)
    {
        


        //$updateData = DB::table('temp_imgdata')->where('id',$id )->update(['filename' =>$image_name]);

        // $this->validate($request, [

        //         'user_id' => 'required',
        //         'filename' => 'required',
        //         'frame_id' => 'required',
        //         'filename.*' => 'image|mimes:jpeg,png,jpg,gif,svg'

        // ]);
        //$frame = DB::table('frames')->select('*')->orderby('id','ASC')->first();

        $frame = DB::table('frames')->select('*')->where('id',$request->frame_id)->first();
        if($frame->quantity > 0){
                    $check = true;
                    $image_data = $request->image;
                    //$id=$request->id;
                    $image_array_1 = explode(";", $image_data);
                    $image_array_2 = explode(",", $image_array_1[1]);
                    $data = base64_decode($image_array_2[1]);
                    $image_name = time() . '.png';
                    $upload_path = public_path('uploads/order_images/' . $image_name);
                    file_put_contents($upload_path, $data);
                
                    $data = array(
                                'user_id'=> $request->user_id, 'quantity' => $request->quantity, 
                                'filename' => $image_name, 'frame_id' => $frame->id, 'status'=>0,
                                'created_at' =>  date('Y-m-d H:i:s')
                            ); 
                

                


                    $check = DB::table('temp_imgdata')->insert($data);
                    
                    if ($check) {
                                return response()->json([
                           'success'=>'done'
                          ]);
                    } else {
                        return redirect('/select_frame');


                    }
                
                
                
        }else{
           
          session::flash('error', 'Out of stock');
                return redirect('/select_frame');
        }

        

        
        
    }
    // public function image_crop(Request $request){
    //    // $forminput =  $request->all();
    //     $filename =public_path('uploads/order_images/' . \Request::segment(6));
    //     $img_r = imagecreatefromjpeg(public_path('uploads/order_images/' . \Request::segment(6)));
    //     $dst_r = ImageCreateTrueColor( \Request::segment(4), \Request::segment(5) );

    //     imagecopyresampled($dst_r, $img_r, 0, 0, \Request::segment(2), \Request::segment(3), \Request::segment(4), \Request::segment(5), \Request::segment(4),\Request::segment(5));

    //     header('Content-type: image/jpeg');
    //     //print_r($dst_r);
    //       imagejpeg($dst_r,$filename);
          
    //         imagedestroy($dst_r);
    //         imagedestroy($img_r);
    //    // exit;
    // }

     public function image_crop(Request $request){
       // $forminput =  $request->all();
        $filename =public_path('uploads/order_images/' . \Request::segment(6));
        $img_r = imagecreatefromjpeg(public_path('uploads/order_images/' . \Request::segment(6)));
        $dst_r = ImageCreateTrueColor( \Request::segment(4), \Request::segment(5) );

        imagecopyresampled($dst_r, $img_r, 0, 0, \Request::segment(2), \Request::segment(3), \Request::segment(4), \Request::segment(5), \Request::segment(4),\Request::segment(5));

        header('Content-type: image/jpeg');
        //print_r($dst_r);
          imagejpeg($dst_r,$filename);
          
            imagedestroy($dst_r);
            imagedestroy($img_r);
       // exit;
    }
    public function saveCropImg(Request $request){

       $frame_id =  $request->input('frame_id');
       $image_top =  $request->input('image_top');
       $image_left=  $request->input('image_left');
       $transform =  $request->input('transform');
       $scale=  $request->input('scale');

        $editdata = array('img_top' => $image_top,
                         'img_left'=> $image_left,
                         'transform'=> $transform,
                         'scale'=> $scale
                      );
        DB::table('temp_imgdata')->where('id', $frame_id)->update($editdata);
        return response()->json(['status'=>true,'msg' => 'Resim başarıyla güncellendi']);
    }

    public function uploadCropImage(Request $request)
    {
        $crop_frame_id = $request->crop_frame_id;
        $image = $request->image;

        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);
        $image_name='thumb_'.time().'.png';
        $path = public_path('uploads/order_images/'.$image_name);
        file_put_contents($path, $image);
        DB::table('temp_imgdata')->where('id', $crop_frame_id)->update(['thumb_filename'=>$image_name]);
        return response()->json(['status'=>true,'msg' => "Update image successfully !"]);
    
    }

    public function imagerotateUpload(Request $request) {

            $image = "1595922106_fhdjhjdfh.jpg";
         
            $filename = public_path('uploads/order_images/1595922106_fhdjhjdfh.jpg');
            $path = public_path('uploads/order_images/');
            $degrees =90;

            // Content type
            header('Content-type: image/jpeg');

            // Load
            $source = imagecreatefromjpeg($filename);

            // Rotate
            $rotate = imagerotate($source, $degrees, 0);

            // Output
            imagejpeg($rotate,$filename);

            imagedestroy($source);
            imagedestroy($rotate);

            // $img = $image1;
            // $new_name = time() . '.' . $img->getClientOriginalExtension();
            // $img->move(public_path('/upload') , $new_name);

            //$filename->move(public_path('/upload') , $name);

            //echo $path.''.$name;

            // $rotateFilename =  public_path('uploads/order_images/1595922106_fhdjhjdfh.jpg');
            //  // PATH
            // $image = "1595922106_fhdjhjdfh.jpg";
            // $degrees = 180;
            // $fileTye = strtolower(substr('1595922106_fhdjhjdfh.jpg', strrpos('1595922106_fhdjhjdfh.jpg', '.') + 1));

            // if($fileType == 'png'){
            //     header('Content-type: image/png');
            //     $source = imagecreatefrompng($rotateFilename);
            //     $bgColor = imagecolorallocatealpha($source, 255, 255, 255, 127);
            //     // Rotate
            //     $rotate = imagerotate($source, $degrees, $bgColor);
            //     imagesavealpha($rotate, true);
            //     imagepng($rotate,$rotateFilename);

            //     $image_name = time() . $image;
            //     $upload_path = public_path('upload' . $image_name);
            //     file_put_contents($upload_path, $image_name);

            // }

            // if($fileType == 'jpg' || $fileType == 'jpeg'){
            //     header('Content-type: image/jpeg');
            //     $source = imagecreatefromjpeg($rotateFilename);
            //     // Rotate
            //     $rotate = imagerotate($source, $degrees, 0);
            //     echo imagejpeg($rotate);

            //     //$image_name = $image1;
            //     //$upload_path = public_path('upload/' . $image_name);
            //     //file_put_contents($upload_path, $image_name);
            // }

        
    }


    
     public function getAllSizeFrameListbycategory(Request $request){

        //echo "<pre>"; print_r($request->all());die;
       $cat = $request->cat_id;
       $face_count = $request->face_count;

       if(!empty($cat)){
       
        $frameList = DB::table('frames')->groupBy('size')->where('status', 1)->where('category_id', $cat)->where('face_count', $face_count)->get();
    
       }else{

        $frameList = DB::table('frames')->groupBy('size')->where('status', 1)->where('face_count', $face_count)->get();   
       }
        // echo '<pre>';
        // print_r($frameList);
        // die;

        $result = array();

        $results = array();

        foreach( $frameList as $key => $frame ){

            $frame_images_path = url('/public/uploads/frame_images/');

            $result['id'] =  $frame->id;

            $result['image_path'] =  $frame_images_path;

            $result['title'] =  $frame->title;

            $result['quantity'] =  $frame->quantity;

            $result['price'] =  $frame->price;

            $result['currency'] =  $frame->currency;

            $result['images'] =  $frame->images;

            $result['second_image'] =  $frame->second_image;

            $result['type'] =  $frame->type;

            $result['face_count'] =  $frame->face_count;

            $result['size'] =  $frame->size;

            $result['size_inch'] =  $frame->size_inch;

            $result['status'] =  $frame->status;

            $result['created_at'] = $frame->created_at ;

            $result['updated_at'] = $frame->updated_at ;

            $results[] =  $result;

        }


        if( sizeof($frameList) ){

            return response()->json(['status'=>$this->successStatus, 

                'msg' => 'Frame list successfully',

                'cat_id' => $cat,

                'response'=> ['frameList' => $results]

            ]);
       

        }else{

            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No frames found']); 

        }

    }




}
?>