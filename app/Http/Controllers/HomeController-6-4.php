<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\User;
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;
use Route;
use App\Helpers\Helper;

class HomeController extends Controller 
{
	public function home(Request $request) {
		return view('user/home');
	}


	public function user_dashboard(Request $request) {

		return view('user/dashboard');
	}	


	public function user_profile(Request $request) {

		return view('user/profile');
	}

	public function edit_profile(Request $request) {

		return view('user/edit_profile');
	}

	public function change_password(Request $request) {

		return view('user/change_password');
	}
	

	public function select_frame(Request $request) {

		if(Auth::user()->id){
			$user_id=Auth::user()->id;

			$address = DB::table('shipping_address')->select('*')->where('user_id',$user_id)->first();
			$data['userAddress'] = $address;
			$countriesData = DB::table('countries')->select('*')->get();
			$data['countrydata']=$countriesData;
			// if($address){
			// 	echo "yes";
			// 	die;
			// }else{
			// 	echo "no";
			// 	die;
			// }
		}else{
			$data['userAddress']='';
		}

		return view('user/select_frame',$data);
	}

	public function logout(){
		Auth::logout();
		return redirect('/');
	}


}
?>