<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 

use App\Http\Controllers\Controller; 

use App\FeameModel, App\Group_request, Hash; 

use Illuminate\Support\Facades\Auth; 

use Validator, DB;

use Illuminate\Validation\Rule;

use Session;

use Illuminate\Routing\UrlGenerator;

use Illuminate\Database\QueryException;



class ApiFrameController extends Controller 

{

    public $successStatus = true;

    public $failureStatus = false;

    

    /** 

    * create feed api 

    * 

    * @return \Illuminate\Http\Response 

    */ 

    public function getAllFrameList(Request $request){

       // $cat = $request->cat_id;
       // if(!empty($cat)){
       //      $cat = $request->cat_id;
       //      $frameList = DB::table('frames')->where('status', 1)->where('category_id', $cat)->get();
       // }else{
       //      $frameList = DB::table('frames')->where('status', 1)->get();   
       // }


       $cat = $request->cat_id;
       $size = $request->size;
       $face_count = $request->face_count;
       if(!empty($cat)){
            $cat = $request->cat_id;
            $frameList = DB::table('frames')->where('status', 1)->where('category_id', $cat)->where('size', $size)->where('face_count', $face_count)->get();
       }else{
            $frameList = DB::table('frames')->where('status', 1)->get();   
       } 

        $result = array();

        $results = array();

        foreach( $frameList as $key => $frame ){

            $frame_images_path = url('/public/uploads/frame_images/');

            $result['id'] =  $frame->id;

            $result['image_path'] =  $frame_images_path;

            $result['title'] =  $frame->title;

            $result['quantity'] =  $frame->quantity;

            $result['price'] =  $frame->price;

            $result['currency'] =  $frame->currency;

            $result['images'] =  $frame->images;

            $result['second_image'] =  $frame->second_image;

            $result['type'] =  $frame->type;

            $result['status'] =  $frame->status;

            $result['created_at'] = $frame->created_at ;

            $result['updated_at'] = $frame->updated_at ;

            $results[] =  $result;

        }



        if( sizeof($frameList) ){

            return response()->json(['status'=>$this->successStatus, 

                'msg' => 'Frame list successfully',

                'cat_id' => $cat,

                'response'=> ['frameList' => $results]

            ]);

            

        }else{

            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No frames found']); 

        }

    }


     public function getAllFrameListbyid(Request $request){

       $cat = $request->id;
      
       if(!empty($cat)){

            $cat = $request->id;
            $frameList = DB::table('frames')->where('status', 1)->where('id', $cat)->get();

       }else{
        
            $frameList = DB::table('frames')->where('status', 1)->get();   
       }

        $result = array();

        $results = array();

        foreach( $frameList as $key => $frame ){

            $frame_images_path = url('/public/uploads/frame_images/');

            $result['id'] =  $frame->id;

            $result['image_path'] =  $frame_images_path;

            $result['title'] =  $frame->title;

            $result['quantity'] =  $frame->quantity;

            $result['price'] =  $frame->price;

            $result['currency'] =  $frame->currency;

            $result['images'] =  $frame->images;

            $result['second_image'] =  $frame->second_image;

            $result['type'] =  $frame->type;

            $result['status'] =  $frame->status;

            $result['created_at'] = $frame->created_at ;

            $result['updated_at'] = $frame->updated_at ;

            $results[] =  $result;

        }



        if( sizeof($frameList) ){

            return response()->json(['status'=>$this->successStatus, 

                'msg' => 'Frame list successfully',

                'cat_id' => $cat,

                'response'=> ['frameList' => $results]

            ]);

            

        }else{

            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No frames found']); 

        }

    }


     public function getAllCategoryList(Request $request){
        $frameList = DB::table('category')->where('status', 1)->get();   
      
        $result = array();

        $results = array();

        foreach( $category as $key => $frame ){

            $result['id'] =  $frame->cat_id;

            $result['category_name'] =  $frame->category_name;

            $results[] =  $result;

        }



        if( sizeof($frameList) ){

            return response()->json(['status'=>$this->successStatus, 

                'msg' => 'Category list successfully',
                'response'=> ['categoryList' => $results]

            ]);

            

        }else{

            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No category found']); 

        }

    }


     public function getAllFramesizebycatid(Request $request){
       $cat = $request->cat_id;
       $face_count = $request->face_count;
       if(!empty($cat)){
            $cat = $request->cat_id;
            $frameList = DB::table('frames')->groupBy('size')->where('status', 1)->where('category_id', $cat)->where('face_count', $face_count)->get();
       }else{
            $frameList = DB::table('frames')->groupBy('size')->where('status', 1)->where('face_count', $face_count)->get();   
       }



        

        $result = array();

        $results = array();

        foreach( $frameList as $key => $frame ){

            $frame_images_path = url('/public/uploads/frame_images/');

            $result['id'] =  $frame->id;

            $result['image_path'] =  $frame_images_path;

            $result['title'] =  $frame->title;

            $result['quantity'] =  $frame->quantity;

            $result['price'] =  $frame->price;

            $result['currency'] =  'USD';

            $result['images'] =  $frame->images;

            $result['second_image'] =  $frame->second_image;

            $result['face_count'] =  $frame->face_count;

            $result['size'] =  $frame->size;

            $result['size_inch'] =  $frame->size_inch;

            $result['type'] =  $frame->type;

            $result['status'] =  $frame->status;

            $result['created_at'] = $frame->created_at ;

            $result['updated_at'] = $frame->updated_at ;

            $results[] =  $result;

        }



        if( sizeof($frameList) ){

            return response()->json(['status'=>$this->successStatus, 

                'msg' => 'Frame list successfully',

                'response'=> ['frameList' => $results]

            ]);

            

        }else{

            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No frames found']); 

        }

    }




}