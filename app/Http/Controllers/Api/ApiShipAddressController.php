<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\ShipModel, App\Group_request, Hash; 
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Session;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Database\QueryException;

class ApiShipAddressController extends Controller 
{
    public $successStatus = true;
    public $failureStatus = false;
    
    /** 
    * create feed api 
    * 
    * @return \Illuminate\Http\Response 
    */ 

    public function getAllCountryList(){
        $countryList = DB::table('countries')->get();

        if( !empty($countryList) ){
            return response()->json(['status'=>$this->successStatus, 
                'msg' => 'Country list successfully',
                'response'=>['countryList' => $countryList]
            ]);
            
        }else{
            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No Country found']); 
        }
    }


     public function getUserAddressById(Request $request){

        $address_id = $request->address_id;

            $country_info = DB::table('shipping_address')->where('id',$address_id)->first();
            $country_name = $country_info->country;
            $day_info = DB::table('shipping_price')->where('country_id',$country_name)->first();

         if(!empty($day_info)){   
            $ship_price = $day_info->price;
            $delivery_days = $day_info->delivery_days;
            $current_date = date('Y-m-d');
            $delivery_date = date('Y-m-d',strtotime($current_date.'+ ' .$delivery_days. 'days'));

        //$address_info = DB::table('shipping_address')->where('id', $address_id)->where('status',1)->first();
        $address_info =  DB::table('shipping_address')
        ->select('shipping_address.id', 'shipping_address.address_type', 'shipping_address.fullname','shipping_address.street_address','shipping_address.city','shipping_address.state',
            'countries.name as country', 'shipping_address.country as country_id' ,'shipping_address.zip_code','shipping_address.contact_number','shipping_price.price as shipping_price' ,'shipping_price.currency as shipping_currency', 'shipping_price.delivery_days', 'shipping_address.status', 'shipping_address.created_at')
        ->leftjoin('shipping_price','shipping_price.country_id','=','shipping_address.country')
        ->leftjoin('countries','countries.id','=','shipping_address.country')
        ->where('shipping_address.id',$address_id)
        ->first();

        if(!empty($address_info)){

            return response()->json(['status'=>$this->successStatus, 
                'msg' => 'Address list successfully',
                'response'=>['addressList' => $address_info , 'delivery_date' => $delivery_date]
            ]);
            
        }else{

            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No Address found']);

        }
    }else{
         return response()->json(['status'=>$this->failureStatus, 'msg' => 'selected country is not valid for shipping']); 
    }
    
    }


    public function getUserAddressList(Request $request){
        $user_id = $request->user_id;

        $addressList =  DB::table('shipping_address')
        ->select('shipping_address.id', 'shipping_address.address_type', 'shipping_address.fullname','shipping_address.street_address','shipping_address.city','shipping_address.state',
            'countries.name as country', 'shipping_address.country as country_id' ,'shipping_address.zip_code','shipping_address.contact_number','shipping_price.price as shipping_price' ,'shipping_price.currency as shipping_currency', 'shipping_price.delivery_days', 'shipping_address.status', 'shipping_address.created_at')
        ->leftjoin('shipping_price','shipping_price.country_id','=','shipping_address.country')
        ->leftjoin('countries','countries.id','=','shipping_address.country')
        ->where('user_id',$user_id)
        ->get();

        //if(!empty($addressList)){
        if(count($addressList)>0){
            return response()->json(['status'=>$this->successStatus, 
                'msg' => 'Address list successfully', 'response'=>['addressList' => $addressList ]
            ]);
            
        }else{
            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No address found']); 
        }
    }

    public function createShipAddress(Request $request){
        //print_r($request->all()); die();
        $forminput =  $request->all();
        $validator = Validator::make($request->all(), [ 
            'user_id'  => 'required',
            'address_type'  => 'required',
            'fullname'  => 'required',
            'street_address'  => 'required',
            'city'  => 'required',
            'state'  => 'required',
            'country'  => 'required',
            'zip_code'  => 'required',
            'contact_number'  => 'required'
        ]);
        
        if ($validator->fails()) { 
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {   
                return response()->json(['status'=>$this->failureStatus,'msg'=>$message]);            
            }            
        }

        $price_info = DB::table('shipping_price')->where('country_id',$forminput['country'] )->first();
        

        if(!empty($price_info)){
            $price = !empty($price_info->price)? $price_info->price:'';
            $currency = !empty($price_info->currency)? $price_info->currency:'';
        $obj = new ShipModel;
        $obj->user_id = $forminput['user_id'];
        $obj->address_type = $forminput['address_type'];
        $obj->fullname = $forminput['fullname'];
        $obj->street_address = $forminput['street_address'];
        $obj->city = $forminput['city'];
        $obj->state = $forminput['state'];
        $obj->country = $forminput['country'];
        $obj->zip_code = $forminput['zip_code'];
        $obj->contact_number = $forminput['contact_number'];
        $obj->status = 1;
        $obj->created_at = date('Y-m-d H:i:s');
        $res = $obj->save();
        $address_id = $obj->id;

        if( $res ){
            if (!empty($forminput['user_id'])) {
                $credentials = array(
                    'ship_id' => $address_id,
                    'shipping_price' => $price,
                    'shipping_currency' => $currency
                ); 

                $shipData = DB::table('users')->where('id',$forminput['user_id'] )->update( $credentials );
            }
            return response()->json(['status'=>$this->successStatus, 'msg' => 'Record Inserted successfully','response'=>['address_id' =>$address_id ,
                'ship_price' =>$price, 'ship_currency' => $currency]]);
            
        }else{
            return response()->json(['status'=>$this->failureStatus, 'msg' => 'Something went wrong']); 
        }
        }
        else{
            return response()->json(['status'=>$this->failureStatus, 'msg' => 'selected country is not valid for shipping']); 
       }
    }


    public function editShipAddress(Request $request){
        $forminput =  $request->all();
        $validator = Validator::make($request->all(), [ 
            'ship_id'  => 'required',
            'user_id'  => 'required',
            'fullname'  => 'required',
            'street_address'  => 'required',
            'city'  => 'required',
            'state'  => 'required',
            'country'  => 'required',
            'zip_code'  => 'required',
            'contact_number'  => 'required'
        ]
    );
        
        if ($validator->fails()) { 
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {   
                return response()->json(['status'=>$this->failureStatus,'msg'=>$message]);            
            }            
        }

        $price_info = DB::table('shipping_price')->where('country_id',$forminput['country'] )->first();
        
       if(!empty($price_info)){
            $price = !empty($price_info->price) ? $price_info->price:'';
            $currency = !empty($price_info->currency) ? $price_info->currency:'';

            $arrayData = array(
            	'address_type' => $forminput['address_type'],
                'fullname'=>$forminput['fullname'],
                'street_address'=>$forminput['street_address'],
                'city'=>$forminput['city'],
                'state'=>$forminput['state'],
                'country'=>$forminput['country'],
                'zip_code'=>$forminput['zip_code'],
                'contact_number'=>$forminput['contact_number'],
                'updated_at'=>date('Y-m-d H:i:s')
            );

            $shipData = DB::table('shipping_address')->where( 'user_id',$forminput['user_id'] )->where('id',$forminput['ship_id'] )->update( $arrayData );

            if( $shipData ){
                    $credentials = array(
                        'shipping_price' => $price,
                        'shipping_currency' => $currency
                    ); 

                    $shipData = DB::table('users')->where('id',$forminput['user_id'] )->update( $credentials );
                return response()->json(['status'=>$this->successStatus, 'msg' => 'Address updated successfully', 'response'=>['ship_id' =>$request->ship_id,
                'ship_price' =>$price, 'ship_currency' => $currency ]]);
                
            }else{
                return response()->json(['status'=>$this->failureStatus, 'msg' => 'Something went wrong']); 
            }
        }
        else{
            return response()->json(['status'=>$this->failureStatus, 'msg' => 'selected country is not valid for shipping']); 
       }
   }

   public function createdefaultaddress(Request $request){
             //print_r($request->all()); die();
        $forminput =  $request->all();
        $validator = Validator::make($request->all(), [ 
            'user_id'  => 'required',
            'address_id'  => 'required',
        ]);
        
        if ($validator->fails()) { 
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {   
                return response()->json(['status'=>$this->failureStatus,'msg'=>$message]);            
            }            
        }

         $res = DB::table('shipping_address')->where('id',$forminput['address_id'])->where('user_id',$forminput['user_id'])->first();



        if( $res ){
            $price_info = DB::table('shipping_price')->where('country_id',$res->country )->first();  
        //     print_r($price_info);
        // die;

        if(!empty($price_info)){ 
         
             $price = !empty($price_info->price) ? $price_info->price:'';
             $currency = !empty($price_info->currency) ? $price_info->currency:'';

             $credentials = array(
                    'ship_id' => $forminput['address_id'],
                    'shipping_price' => $price,
                    'shipping_currency' => $currency
                ); 
            if (!empty($forminput['user_id']) && !empty($forminput['address_id'])) {

                $shipData = DB::table('users')->where('id',$forminput['user_id'] )->update( $credentials );
            }
            return response()->json(['status'=>$this->successStatus, 'msg' => 'Default Address has been updated successfully','response'=>['address_id' =>$forminput['address_id']]]);
        }else{

            $price = '';
             $currency = '';

             $credentials = array(
                    'ship_id' => $forminput['address_id'],
                    // 'shipping_price' => $price,
                    // 'shipping_currency' => $currency
                ); 
            if (!empty($forminput['user_id']) && !empty($forminput['address_id'])) {

                $shipData = DB::table('users')->where('id',$forminput['user_id'] )->update( $credentials );
            }
            return response()->json(['status'=>$this->successStatus, 'msg' => 'Default Address has been updated successfully','response'=>['address_id' =>$forminput['address_id']]]);
        }
            
        }else{
            return response()->json(['status'=>$this->failureStatus, 'msg' => 'User id and address id invailid']); 
        }
   }
    
}