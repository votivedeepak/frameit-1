<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Validator,DB;
use App\User;

class NotificationController extends HomeController
{
    public function __construct()
    {
        parent::__construct();

        $this->Post = new Post;
    }
   
    public function sendNotification(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required',
        ]);

        $input = array_except($request->all(),array('_token'));

        $this->Post->AddData($input);

        $notification = \DB::table('users')->get();

        print_r($notification);die;

        foreach ($notification as $key => $value) {
            $this->notification($value->token, $request->get('title'));
        }

        \Session::put('success','Send notification successfully!!');

        return redirect();
    }

    public function notification($token, $title)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token=$token;

        $notification = [
            'title' => $title,
            'sound' => true,
        ];
        
        $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=AAAASk5-u2g:APA91bHQE__pE-TXQszZb-TorN-8v9dbStYqnp_vIfVWitAXGrnfuVr4YZInoK53FZPEPIj6ej3izSvMwNkbWEb54dcrflqMMwAyjtnjpU1TwS4CyxP_9iHQRAYDlC25yI32Vhs6-4sZ',
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return true;
    } 

}