<?php

namespace App\Http\Controllers\Admin;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller; 

use App\FrameModel;

use Illuminate\Support\Facades\Auth; 

use Validator, DB;

use Illuminate\Validation\Rule;

use Twilio\Rest\Client;

use Session;



class FrameController extends Controller 

{

	public function frame_list() {

		$data['frame_list'] = DB::table('frames')->orderBy('id', 'desc')->get();
		$data['category']=DB::table('category')->where('cat_status','=',1)->get();

		return view('admin/frames/frame_list')->with($data);

	}



	public function add_frame(){
		$data['category']=DB::table('category')->where('cat_status','=',1)->get();
		return view('admin/frames/add_frame')->with($data); 

	}



	public function submit_frame(Request $request){

		//print_r($request->all());die;

		$validator = Validator::make($request->all(), [

			'title' => 'required',

			'category_id' => 'required',

			'quantity' => 'required',

			'price' => 'required',

			'type' => 'required',

			'size' => 'required', 

			'images' => 'required|mimes:jpeg,png,jpg,gif,svg'

		]);

		if ($validator->fails()) {

			session::flash('error', 'Validation error.');

			return redirect('/admin/add_frame')->withErrors($validator)->withInput(); 

		} else {



			$title = $request->title;

			$quantity = $request->quantity;

			$price = $request->price;

			$size = $request->size;

			$size_inch = $request->size_inch;

			$face_count = $request->face_count;

			$category_id = $request->category_id;

			$type = $request->type;

			if ($request->hasFile('images')){



				$frameImage = $request->file('images');

				$extension  = $frameImage->getClientOriginalName();

				$imgName = time().'_'.$extension;

				$destinationPath = public_path('/uploads/frame_images/');

				$frameImage->move($destinationPath, $imgName);



			}else{
				$imgName="";
			}

			if ($request->hasFile('second_image')){



				$frameImage1 = $request->file('second_image');

				$extension1  = $frameImage1->getClientOriginalName();

				$imgName1 = time().'_'.$extension1;

				$destinationPath1 = public_path('/uploads/frame_images/');

				$frameImage1->move($destinationPath1, $imgName1);



			}else{
				$imgName1="";
			}

			

			$obj = new FrameModel;

			$obj->title = $title;

			$obj->quantity = $quantity;

			$obj->price = $price;

			$obj->currency = 'MYR';

			$obj->images = $imgName;

			$obj->alignment ="vertical";

			$obj->second_image = $imgName1;

			$obj->type = $type;

			$obj->category_id = $category_id;

			$obj->size = $size;

			$obj->size_inch = $size_inch;

			$obj->face_count = $face_count;

			$obj->status = 1;

			$obj->created_at = date('Y-m-d H:i:s');

			$res = $obj->save();

			if($res){

				session::flash('message', 'Record Addeed Succesfully.');

				return redirect('admin/frame_list');

			}else{

				session::flash('error', 'Record not inserted.');

				return redirect('admin/frame_list');

			}

		}

	}





	public function edit_frame(Request $request){

		$frame_id = base64_decode($request->id);

		$data['frame_info'] = FrameModel::find($frame_id);

		$data['category']=DB::table('category')->where('cat_status','=',1)->get();

		return view('admin/frames/edit_frame')->with($data) ;

	}



	public function update_frame(Request $request){



		$frame_id = $request->input('frame_id');

		$title = $request->input('title');

		$quantity = $request->input('quantity');

		$price = $request->input('price');

		$type = $request->input('type');

		$size = $request->input('size');

		$size_inch = $request->size_inch;

		$face_count = $request->face_count;

		$category_id = $request->input('category_id');

		//$request->validate([

		//	'images' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5000'

		//]);





		$frameData = FrameModel::where('id', $frame_id)->first();



		if ($request->hasFile('images')){

			$image_path = public_path("/uploads/frame_images/".$frameData->images);

			if (file_exists($image_path)) {

				@unlink($image_path);

			}



			$frameImage = $request->file('images');

			$extension  = $frameImage->getClientOriginalName();

			$imgName = time().'_'.$extension;

			$destinationPath = public_path('/uploads/frame_images/');

			$frameImage->move($destinationPath, $imgName);



		} else {

			$imgName = $frameData->images;

		}

		if ($request->hasFile('second_image')){

			$image_path1 = public_path("/uploads/frame_images/".$frameData->second_image);

			if (file_exists($image_path1)) {

				@unlink($image_path1);

			}



			$frameImage1 = $request->file('second_image');

			$extension1  = $frameImage1->getClientOriginalName();

			$imgName1 = time().'_'.$extension1;

			$destinationPath1 = public_path('/uploads/frame_images/');

			$frameImage1->move($destinationPath1, $imgName1);



		} else {

			$imgName1 = $frameData->second_image;

		}



		$frameData->title = $title;

		$frameData->images = $imgName;

		$frameData->second_image = $imgName1;

		$frameData->quantity = $quantity;

		$frameData->price = $price;

		$frameData->currency = 'MYR';

		$frameData->type = $type;

		$frameData->category_id = $category_id;

		$frameData->size = $size;

		$frameData->size_inch = $size_inch;

		$frameData->face_count = $face_count;

		$frameData->updated_at = date('Y-m-d H:i:s');

		$res = $frameData->save();

		

		if($res){

			session::flash('message', 'Record updated succesfully.');

			return redirect('admin/frame_list');

		}else{

			session::flash('error', 'Somthing went wrong.');

			return redirect('admin/frame_list');

		} 

	} 





	public function change_frame_status(Request $request)

    {

        $frame = FrameModel::find($request->frame_id);

        $frame->status = $request->status;

        $res = $frame->save();



        if($res){



        	if($request->status == '1'){

        		$title ="New artice/anouncement published successfully";

        	}else{

        		$title ="New artice/anouncement unpublished";

        	}

        	



	    	$device_token ="ftkpkxjW37Q:APA91bE_27LnEi3ziGsQpVbZnvh6MlNxovsEVVQSGPDMlnTYt47IiQP25JuSwC0AqiGeGwHa3LAFZ9mw8IKmRkMD1lzKRSeMEBPNCpvsIkKA4AcNAieLbQ-OFlXq74jiCUo2xfPy2fi8";

	    

	    	$notification = \DB::table('users')->get();



	        foreach ($notification as $key => $value) {



	            $this->notification($value->device_token, $title);

	        }



        }

        

  

        return response()->json(['success'=>'User status change successfully.']);

    }



    public function notification($token, $title)

    {

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $token=$token;



        $notification = [

            'title' => 'FRAMEiT',

            'body' => $title,

            'tag' =>'feed_status',

            'sound' => true,

        ];

        

        $extraNotificationData = $notification;



        $fcmNotification = [

            //'registration_ids' => $tokenList, //multple token array

            'to'        => $token, //single token

            'notification' => $notification,

            'data' => $extraNotificationData

        ];



        $headers = [

            'Authorization: key=AAAASk5-u2g:APA91bHQE__pE-TXQszZb-TorN-8v9dbStYqnp_vIfVWitAXGrnfuVr4YZInoK53FZPEPIj6ej3izSvMwNkbWEb54dcrflqMMwAyjtnjpU1TwS4CyxP_9iHQRAYDlC25yI32Vhs6-4sZ',

            'Content-Type: application/json'

        ];





        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$fcmUrl);

        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));

        $result = curl_exec($ch);

        curl_close($ch);



        return true;

    }





	public function delete_frame(Request $request) {

		$frame_id = $request->frame_id;



		$frame_info = DB::table('frames')->where('id','=',$frame_id)->first();

		$media_url = $frame_info->images;



		$res = DB::table('frames')->where('id', '=', $frame_id)->delete();



		if ($res) {

			@unlink(url('/').'/public/uploads/frame_images/'.$media_url);

			return json_encode(array('status' => 'success','msg' => 'Image has been deleted successfully!'));

		} else {

			return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));

		}



	}



}

?>