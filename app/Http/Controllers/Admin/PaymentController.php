<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\PaymentModel;
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;

class PaymentController extends Controller 
{
	public function transaction_list() {
		$data['transaction_list'] =  DB::table('payment_transaction')
        ->select('payment_transaction.id','payment_transaction.order_id', 'payment_transaction.txn_id', 'payment_transaction.txn_amount', 'payment_transaction.payment_method',  'payment_transaction.txn_status', 'payment_transaction.txn_date',
        	'payment_transaction.user_id', 'users.first_name', 'users.last_name', 'payment_transaction.created_at')
        ->join('users','users.id','=','payment_transaction.user_id')
        ->orderBy('payment_transaction.id', 'DESC')
        ->get();

		return view('admin/coupons/transaction_list')->with($data);
	}

	public function change_transaction_status(Request $request)
    {
        $trans_info = PaymentModel::find($request->trans_id);
        $trans_info->status = $request->status;
        $trans_info->save();
  
        return response()->json(['success'=>'Transaction status change successfully.']);
    }


	public function delete_transaction_code(Request $request) {
		$trans_id = $request->trans_id;

		$trans_info = DB::table('payment_transaction')->where('id','=',$trans_id)->first();
		$res = DB::table('payment_transaction')->where('id', '=', $trans_id)->delete();

		if ($res) {
			return json_encode(array('status' => 'success','msg' => 'Transaction has been deleted successfully!'));
		} else {
			return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
		}

	}

	public function add_coupon(){
		return view('admin/coupons/add_coupon'); 
	}

	public function submit_coupon(Request $request){
		//print_r($request->all());die;
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'start_date' => 'required',
			'end_date' => 'required',
			'no_of_uses' => 'required',
			'uses_limit' => 'required',
			'code' => 'required',
			'type' => 'required',
			'value' => 'required'
		]);
		if ($validator->fails()) {
			session::flash('error', 'Validation error.');
			return redirect('/admin/add_coupon')->withErrors($validator)->withInput(); 
		} else {

			$name = $request->name;
			$start_date = $request->start_date;
			$end_date = $request->end_date;
			$no_of_uses = $request->no_of_uses;
			$uses_limit = $request->uses_limit;
			$code = $request->code;
			$type = $request->type;
			$value = $request->value;
			
			$obj = new CouponModel;
			$obj->name = $name;
			$obj->start_date = $start_date;
			$obj->end_date = $end_date;
			$obj->no_of_uses = $no_of_uses;
			$obj->uses_limit = $uses_limit;
			$obj->code = $code;
			$obj->type = $type;
			$obj->value = $value;
			$obj->status = 1;
			$obj->created_at = date('Y-m-d H:i:s');
			$res = $obj->save();
			if($res){
				session::flash('message', 'Record Addeed Succesfully.');
				return redirect('admin/coupon_list');
			}else{
				session::flash('error', 'Record not inserted.');
				return redirect('admin/coupon_list');
			}
		}
	}


	public function edit_coupon(Request $request){
		$coupon_id = base64_decode($request->id);
		$data['coupon_info'] = PaymentModel::find($coupon_id);

		return view('admin/coupons/edit_coupon')->with($data) ;
	}

	public function update_coupon(Request $request){

		$coupon_id = $request->input('coupon_id') ;
		$name = $request->input('name') ;
		$start_date = $request->input('start_date') ;
		$end_date = $request->input('end_date') ;
		$no_of_uses = $request->input('no_of_uses') ;
		$uses_limit = $request->input('uses_limit') ;
		$code = $request->input('code') ;
		$type = $request->input('type') ;
		$value = $request->input('value') ;


		$couponData = PaymentModel::where('id', $coupon_id)->first();

		$couponData->name = $name;
		$couponData->start_date = $start_date;
		$couponData->end_date = $end_date;
		$couponData->no_of_uses = $no_of_uses;
		$couponData->uses_limit = $uses_limit;
		$couponData->code = $code;
		$couponData->type = $type;
		$couponData->value = $value;
		$couponData->updated_at = date('Y-m-d H:i:s');
		$res = $couponData->save();
		
		if($res){
			session::flash('message', 'Record updated succesfully.');
			return redirect('admin/coupon_list');
		}else{
			session::flash('error', 'Somthing went wrong.');
			return redirect('admin/coupon_list');
		} 
	} 


}
?>