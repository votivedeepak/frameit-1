<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\PramotionModel;
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;

class PramotionController extends Controller 
{
	public function pramotion_video_list() {
		$data['video_list'] = DB::table('pramotion_videos')->get();
		return view('admin/pramotions/video_list')->with($data);
	}

	public function add_video(){
		return view('admin/pramotions/add_video'); 
	}

	public function submit_video(Request $request){
		//print_r($request->all());die;
		$validator = Validator::make($request->all(), [
			'title' => 'required',
			'sub_title' => 'required',
			'description' => 'required',
			'image' => 'required',
			'videos.*'  => 'mimes:mp4,3gp,avi,mov,qt',
			'button_name' => 'required'
		]);
		if ($validator->fails()) {
			session::flash('error', 'Validation error.');
			return redirect('/admin/add_video')->withErrors($validator)->withInput(); 
		} else {

			$title = $request->title;
			$sub_title = $request->sub_title;
			$description = $request->description;
			$button_name = $request->button_name;
			$image = $request->image;

			$image->move(public_path('/uploads/pramotion_videos/'),$image->getClientOriginalName());

			if ($request->hasFile('video')){

				$pramotionVideo = $request->file('video');
				$extension  = $pramotionVideo->getClientOriginalName();
				$vidName = time().'_'.$extension;
				$destinationPath = public_path('/uploads/pramotion_videos/');
				$pramotionVideo->move($destinationPath, $vidName);

			}

			$obj = new PramotionModel;
			$obj->title = $title;
			$obj->sub_title = $sub_title;
			$obj->description = $description;
			$obj->video_image = $image->getClientOriginalName();
			$obj->video = $vidName;
			$obj->button_name = $button_name;
			$obj->status = 1;
			$obj->created_at = date('Y-m-d H:i:s');
			$res = $obj->save();
			if($res){
				session::flash('message', 'Record Addeed Succesfully.');
				return redirect('admin/pramotion_video_list');
			}else{
				session::flash('error', 'Record not inserted.');
				return redirect('admin/pramotion_video_list');
			}
		}
	}


	public function edit_video(Request $request){
		$video_id = base64_decode($request->id);
		$data['video_info'] = PramotionModel::find($video_id);

		return view('admin/pramotions/edit_video')->with($data) ;
	}

	public function update_video(Request $request){

		$video_id = $request->input('video_id') ;
		$title = $request->input('title') ;
		$sub_title = $request->input('sub_title') ;
		$description = $request->input('description') ;
		$button_name = $request->input('button_name') ;
		$image = $request->image;

		//$request->validate([
			//'video' => 'mimes:mp4,3gp,avi,mov,qt'
		//]);


		$pramotionData = PramotionModel::where('id', $video_id)->first();

		if ($request->hasFile('image')){
			$img_name = $image->getClientOriginalName();
			$image->move(public_path('/uploads/pramotion_videos/'),$image->getClientOriginalName());
		}else{
			$img_name = $pramotionData->video_image;
		}

		

		if ($request->hasFile('video')){
			$image_path = public_path("/uploads/feed_images/".$pramotionData->video);
			if (file_exists($image_path)) {
				@unlink($image_path);
			}

			$pramotionVideo = $request->file('video');
			$extension  = $pramotionVideo->getClientOriginalName();
			$vidName = time().'_'.$extension;
			$destinationPath = public_path('/uploads/pramotion_videos/');
			$pramotionVideo->move($destinationPath, $vidName);
			
		} else {
			$vidName = $pramotionData->video;
		}

		$pramotionData->title = $title;
		$pramotionData->sub_title = $sub_title;
		$pramotionData->description = $description;
		$pramotionData->button_name = $button_name;
		$pramotionData->video = $vidName;
		$pramotionData->video_image = $img_name;
		$pramotionData->updated_at = date('Y-m-d H:i:s');
		$res = $pramotionData->save();
		
		if($res){
			session::flash('message', 'Record updated succesfully.');
			return redirect('admin/pramotion_video_list');
		}else{
			session::flash('error', 'Somthing went wrong.');
			return redirect('admin/pramotion_video_list');
		} 
	}

	public function change_video_status(Request $request)
    {
        $video = PramotionModel::find($request->video_id);
        $video->status = $request->status;
        $video->save();
  
        return response()->json(['success'=>'User status change successfully.']);
    }



	public function delete_pramote_video(Request $request) {
		$video_id = $request->video_id;

		$video_info = DB::table('pramotion_videos')->where('id','=',$video_id)->first();
		$media_url = $video_info->video;

		$res = DB::table('pramotion_videos')->where('id', '=', $video_id)->delete();

		if ($res) {
			@unlink(url('/').'/public/uploads/pramotion_videos/'.$media_url);
			return json_encode(array('status' => 'success','msg' => 'Image has been deleted successfully!'));
		} else {
			return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
		}

	} 


}
?>