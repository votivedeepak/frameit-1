<?php

namespace App\Http\Controllers\Admin;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller; 

use App\User;

use Illuminate\Support\Facades\Auth; 

use Validator, DB;

use Illuminate\Validation\Rule;

use Twilio\Rest\Client;

use Session;



class DashboardController extends Controller 

{

	public function index() {

		$data['orders'] = DB::table('orders')->where('order_status','pending')->get();

		$data['users'] = DB::table('users')->where('status',1)->get();

		$data['visitors'] = DB::table('users')->where('status',1)->get();

		return view('admin/dashboard')->with($data);

	}



	public function logout(){

        Auth::logout();

        return redirect('/admin_login');

    }

}

?>