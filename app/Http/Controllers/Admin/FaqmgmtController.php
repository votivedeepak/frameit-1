<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\FaqModel;
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;

class FaqmgmtController extends Controller 
{
	public function faq_mgmt_list() {
		$data['faq_list'] = DB::table('faqs_mgmt')->get();
		return view('admin/faqs/faq_mgmt_list')->with($data);
	}

	public function add_faq_mgmt(){
		return view('admin/faqs/add_faq_mgmt'); 
	}

	public function submit_faq_mgmt(Request $request){

		$validator = Validator::make($request->all(), [
			'question' => 'required',
			'answer' => 'required',
		]);
		if ($validator->fails()) {
			session::flash('error', 'Validation error.');
			return redirect('/admin/add_faq_mgmt')->withErrors($validator)->withInput(); 
		} else {

			$question = $request->question;
			$answer = $request->answer;
			
			$obj = new FaqModel;
			$obj->question = $question;
			$obj->answer = $answer;
			$obj->status = 1;
			$obj->created_at = date('Y-m-d H:i:s');
			$res = $obj->save();
			if($res){
				session::flash('message', 'Record Addeed Succesfully.');
				return redirect('admin/faq_mgmt_list');
			}else{
				session::flash('error', 'Record not inserted.');
				return redirect('admin/faq_mgmt_list');
			}
		}
	}


	public function edit_faq_mgmt(Request $request){
		$faq_id = base64_decode($request->id);
		$data['faq_info'] = FaqModel::find($faq_id);

		return view('admin/faqs/edit_faq_mgmt')->with($data) ;
	}

	public function update_faq_mgmt(Request $request){

		$faq_id = $request->input('faq_id') ;
		$question = $request->input('question') ;
		$answer = $request->input('answer') ;

		$faqData = FaqModel::where('id', $faq_id)->first();

		$faqData->question = $question;
		$faqData->answer = $answer;
		$faqData->updated_at = date('Y-m-d H:i:s');
		$res = $faqData->save();
		
		if($res){
			session::flash('message', 'Record updated succesfully.');
			return redirect('admin/faq_mgmt_list');
		}else{
			session::flash('error', 'Somthing went wrong.');
			return redirect('admin/faq_mgmt_list');
		} 
	} 


	public function change_faq_status(Request $request)
    {
        $faq_info = FaqModel::find($request->faq_id);
        $faq_info->status = $request->status;
        $faq_info->save();
  
        return response()->json(['success'=>'Faq mgmt status change successfully.']);
    }


	public function delete_faq_mgmt(Request $request) {
		$faq_id = $request->faq_id;

		$faq_info = DB::table('faqs_mgmt')->where('id','=',$faq_id)->first();

		$res = DB::table('faqs_mgmt')->where('id', '=', $faq_id)->delete();	

		if ($res) {
			return json_encode(array('status' => 'success','msg' => 'Data has been deleted successfully!'));
		} else {
			return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
		}

	}

	

}
?>