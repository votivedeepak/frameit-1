<?php
	namespace App\Http\Controllers\Admin;

	use Illuminate\Support\Facades\Hash;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller; 
	use App\User;
	use App\FeedTitleModel;
	use App\Helpers\Helper;
	use Illuminate\Support\Facades\Auth; 
	use Validator, DB;
	use Illuminate\Validation\Rule;
	use Twilio\Rest\Client;
	use Session;
	use Mail;

	class FeedTitle extends Controller 
	{
		public function show_feed_name(){
	        $feed_details = DB::select('select * from feed_title');
	        return view('admin/feeds/show_feed_title',['feed_details'=>$feed_details]);
	    }

		public function addFeedTitle(){
	        return view('admin/feeds/add_feed_title'); 
	    }

	    public function submit_feed_title(Request $request){
	        $validator = Validator::make($request->all(), [
	            'feed_title_name' => 'required',
	            'feed_description' => 'required'
	            
	            
	        ]);
	        if ($validator->fails()) {
	            session::flash('error', 'Validation error.');
	            return redirect('/admin/addFeedTitle')->withErrors($validator)->withInput(); 
	        } else {
	            
	            $feed_title = $request->input('feed_title_name');
	            $feed_description = $request->input('feed_description');

	            
	            DB::insert('insert into feed_title (feed_heading_name,feed_description,status,created_at) values(?,?,1,?)',[$feed_title,$feed_description,date('Y-m-d H:i:s')]);

	            session::flash('message', 'Record Added Succesfully.');

	            return redirect('admin/showFeedTitle');
	            
	        }
	    }

	    public function editFeed_title($id){
	        $feed_title_details = DB::select('select * from feed_title where feed_heading_id = ?',[$id]);
	        return view('admin/feeds/edit_feed_title',['feed_title_details'=>$feed_title_details]); 
	    }

	    public function updateFeedTitle(Request $request, $id){
            $feed = DB::select('select * from feed_title where feed_heading_id = ?',[$id]);
            
            $feed_image_data = (array)$feed[0]; 
            
            
            $feed_heading_name = $request->input('feed_title_name');
            $feed_title_description = $request->input('feed_title_description');

            
            
            DB::update('update feed_title set feed_heading_name = ?,feed_description = ? where feed_heading_id = ?',[$feed_heading_name, $feed_title_description, $id]);

            session::flash('message', 'Record Updated Succesfully.');

            return redirect('admin/showFeedTitle');
            
        

    	}

    	public function change_feedtitle_status(Request $request)

	    {
            
	        $feed = FeedTitleModel::find($request->feed_heading_id);
	        
	        $feed->status = $request->status;

	        $feed->save();

	  		

	        return response()->json(['success'=>'Feed Title status change successfully.']);

	    } 

    	public function deleteFeedTitle(Request $request) {
            $feed_id = $request->feed_id;
			$res = DB::table('feed_title')->where('feed_heading_id', '=', $feed_id)->delete();	

			if ($res) {
				return json_encode(array('status' => 'success','msg' => 'Data has been deleted successfully!'));
			} else {
				return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
			}
			
		}
	}

?>
