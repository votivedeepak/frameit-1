<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\User;
use App\BannerModel;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;
use Mail;

class Banner extends Controller 
{
	

    public function show_banner(){
        $banner_details = DB::select('select * from banner');
        return view('admin/banner/show_banner',['banner_details'=>$banner_details]);
    }

    public function addBanner(){
        return view('admin/banner/add_banner'); 
    }

    public function submit_banner(Request $request){
        $validator = Validator::make($request->all(), [
            'banner_image' => 'required',
            'mobile_banner_image' => 'required',
            'banner_title' => 'required',
            'banner_description' => 'required',
            'button_name' => 'required',
            'button_url' => 'required',
            
        ]);
        if ($validator->fails()) {
            session::flash('error', 'Validation error.');
            return redirect('/admin/addBanner')->withErrors($validator)->withInput(); 
        } else {
            $banner_image = $request->file('banner_image');
            $mobile_banner_image = $request->file('mobile_banner_image');
            $banner_description = $request->input('banner_description');
            $banner_title = $request->input('banner_title');
            $button_name = $request->input('button_name');
            $button_url = $request->input('button_url');

            
            $banner_image->move(public_path('/uploads/banner_images/'),$banner_image->getClientOriginalName());
            $mobile_banner_image->move(public_path('/uploads/banner_images/'),$mobile_banner_image->getClientOriginalName());
            
            DB::insert('insert into banner (banner_image,mobile_banner_image,banner_title,banner_description,button_name,button_url,status,created_at) values(?,?,?,?,?,?,1,?)',[$banner_image->getClientOriginalName(),$mobile_banner_image->getClientOriginalName(),$banner_title,$banner_description,$button_name,$button_url,date('Y-m-d H:i:s')]);

            session::flash('message', 'Record Addeed Succesfully.');

            return redirect('admin/showBanner');
            
        }
    }

    public function editBanner($id){
        $banner = DB::select('select * from banner where banner_id = ?',[$id]);
        return view('admin/banner/edit_banner',['banner_details'=>$banner]); 
    }

    public function updateBanner(Request $request, $id){
            $banner = DB::select('select * from banner where banner_id = ?',[$id]);
            
            $banner_image_data = (array)$banner[0]; 
            
            $banner_image_one = $request->file('banner_image');
            $mobile_banner_image = $request->file('mobile_banner_image');
            $banner_title = $request->input('banner_title');
            $banner_description = $request->input('banner_description');
            $button_name = $request->input('button_name');
            $button_url = $request->input('button_url');

            if($banner_image_one){
                $banner_image = $banner_image_one->getClientOriginalName();
                $banner_image_one->move(public_path('/uploads/banner_images/'),$banner_image_one->getClientOriginalName());
            }else{
                $banner_image = $banner_image_data['banner_image'];
            }

            if($mobile_banner_image){
                $banner_image_mobile = $mobile_banner_image->getClientOriginalName();
                $mobile_banner_image->move(public_path('/uploads/banner_images/'),$mobile_banner_image->getClientOriginalName());
            }else{
                $banner_image_mobile = $banner_image_data['mobile_banner_image'];
            }
            
            
            DB::update('update banner set banner_image = ?,mobile_banner_image = ?, banner_title=?, banner_description = ?, button_name=?,button_url=? where banner_id = ?',[$banner_image,$banner_image_mobile,$banner_title,$banner_description,$button_name,$button_url, $id]);

            session::flash('message', 'Record Updated Succesfully.');

            return redirect('admin/showBanner');
            
        

    }

    public function deleteBanner(Request $request) {

        $banner_id = $request->banner_id;



        $banner_info = DB::table('banner')->where('banner_id','=',$banner_id)->first();
        
        $media_url = $banner_info->banner_image;
        



        $res = DB::table('banner')->where('banner_id', '=', $banner_id)->delete();



        if ($res) {

            @unlink(url('/').'/public/uploads/banner_images/'.$media_url);

            return json_encode(array('status' => 'success','msg' => 'Image has been deleted successfully!'));

        } else {

            return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));

        }



    }

    public function change_banner_status(Request $request)

    {
        
        $feed = BannerModel::find($request->banner_id);
        
        $feed->status = $request->status;

        $feed->save();

        

        return response()->json(['success'=>'Feed Title status change successfully.']);

    } 


}
?>