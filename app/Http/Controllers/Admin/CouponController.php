<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\CouponModel;
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;

class CouponController extends Controller 
{
	public function coupon_list() {
		$data['coupon_list'] = DB::table('coupon')->get();
		return view('admin/coupons/coupon_list')->with($data);
	}

	public function add_coupon(){
		return view('admin/coupons/add_coupon'); 
	}

	public function submit_coupon(Request $request){
		//print_r($request->all());die;
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'start_date' => 'required',
			'end_date' => 'required',
			'no_of_uses' => 'required',
			'uses_limit' => 'required',
			'code' => 'required',
			'type' => 'required',
			'value' => 'required',
			'first_signup_only' => 'required'
		]);
		if ($validator->fails()) {
			session::flash('error', 'Validation error.');
			return redirect('/admin/add_coupon')->withErrors($validator)->withInput(); 
		} else {

			$name = $request->name;
			$start_date = $request->start_date;
			$end_date = $request->end_date;
			$no_of_uses = $request->no_of_uses;
			$uses_limit = $request->uses_limit;
			$code = $request->code;
			$type = $request->type;
			$value = $request->value;
			$first_signup_only = $request->first_signup_only;
			
			$obj = new CouponModel;
			$obj->name = $name;
			$obj->start_date = $start_date;
			$obj->end_date = $end_date;
			$obj->no_of_uses = $no_of_uses;
			$obj->uses_limit = $uses_limit;
			$obj->code = $code;
			$obj->type = $type;
			$obj->value = $value;
			$obj->first_signup_only = $first_signup_only;
			$obj->status = 1;
			$obj->created_at = date('Y-m-d H:i:s');
			$res = $obj->save();
			if($res){
				session::flash('message', 'Record Addeed Succesfully.');
				return redirect('admin/coupon_list');
			}else{
				session::flash('error', 'Record not inserted.');
				return redirect('admin/coupon_list');
			}
		}
	}


	public function edit_coupon(Request $request){
		$coupon_id = base64_decode($request->id);
		$data['coupon_info'] = CouponModel::find($coupon_id);

		return view('admin/coupons/edit_coupon')->with($data) ;
	}

	public function update_coupon(Request $request){

		$coupon_id = $request->input('coupon_id') ;
		$name = $request->input('name') ;
		$start_date = $request->input('start_date') ;
		$end_date = $request->input('end_date') ;
		$no_of_uses = $request->input('no_of_uses') ;
		$uses_limit = $request->input('uses_limit') ;
		$code = $request->input('code') ;
		$type = $request->input('type') ;
		$value = $request->input('value') ;
		$first_signup_only = $request->input('first_signup_only') ;


		$couponData = CouponModel::where('id', $coupon_id)->first();

		$couponData->name = $name;
		$couponData->start_date = $start_date;
		$couponData->end_date = $end_date;
		$couponData->no_of_uses = $no_of_uses;
		$couponData->uses_limit = $uses_limit;
		$couponData->code = $code;
		$couponData->type = $type;
		$couponData->value = $value;
		$couponData->first_signup_only = $first_signup_only;
		$couponData->updated_at = date('Y-m-d H:i:s');
		$res = $couponData->save();
		
		if($res){
			session::flash('message', 'Record updated succesfully.');
			return redirect('admin/coupon_list');
		}else{
			session::flash('error', 'Somthing went wrong.');
			return redirect('admin/coupon_list');
		} 
	} 


	public function change_coupon_status(Request $request)
    {
        $coupon_info = CouponModel::find($request->coupon_id);
        $coupon_info->status = $request->status;
        $coupon_info->save();
  
        return response()->json(['success'=>'Coupon status change successfully.']);
    }


	public function delete_coupon_code(Request $request) {
		$coupon_id = $request->coupon_id;

		$coupon_info = DB::table('coupon')->where('id','=',$coupon_id)->first();
		$res = DB::table('coupon')->where('id', '=', $coupon_id)->delete();

		if ($res) {
			return json_encode(array('status' => 'success','msg' => 'Coupon has been deleted successfully!'));
		} else {
			return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
		}

	}

}
?>