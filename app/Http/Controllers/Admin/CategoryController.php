<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\CheckoutModel;
use App\User;
use App\Category;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;
use Mail;

class CategoryController extends Controller 
{
	public function category_list() {
		$data['category_list'] = Category::orderby('cat_id','DESC')->get();
		return view('admin/category/category_list')->with($data);
	}

	public function add_category(){
		return view('admin/category/add_category'); 
	}


	public function submit_category(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'category_name' => 'required',
		
		]);
		if ($validator->fails()) {
			session::flash('error', 'Validation error.');
			return redirect('/admin/category/add_category')->withErrors($validator)->withInput(); 
		} else {
			$name = $request->category_name;

            $head_count = $request->head_count;


			$obj = new Category;
			$obj->category_name = $name;
			$obj->cat_status = 1;
            $obj->head_count = $head_count;
			$obj->created_at = date('Y-m-d H:i:s');
			$obj->updated_at = date('Y-m-d H:i:s');
			$res = $obj->save();

			if ($res) {

                session::flash('message', 'Category Added successfully!');
                return redirect('admin/category_list');

            } else {

            	session::flash('error', 'OOPs! Some internal issue occured.');
            	return redirect('admin/add_category');
            }
        } 

    }


    public function edit_category(Request $request){
    	$user_id = base64_decode($request->id);
    	$data['user_info'] = Category::where('cat_id', $user_id)->first();

    	return view('admin/category/edit_category')->with($data) ;
    }

    public function update_category(Request $request){

    	$cat_id = $request->input('cat_id') ;
    	$category_name = $request->input('category_name') ;
    	

    	$userData = Category::where('cat_id', $cat_id)->first();
    
        $userData->category_name = $request->category_name;
        $userData->head_count = $request->head_count;
        $userData->updated_at = date('Y-m-d H:i:s');

    	$res = $userData->save();

    	if($res){
    		session::flash('message', 'Category updated succesfully.');
    		return redirect('admin/category_list');
    	}else{
    		session::flash('error', 'Somthing went wrong.');
    		return redirect('admin/category_list');
    	} 
    }

    public function user_profile(Request $request) {
    	$user_id = base64_decode($request->id);
    	$data['user_info'] = Vendor::find($user_id);

    	$data['followers'] = DB::table('follower')->where('user_id','=',$user_id)->where('request_status','=',1)->count();

    	$data['following'] = DB::table('follower')->where('follower_id','=',$user_id)->where('request_status','=',1)->count();

    	return view('admin/vendor/user_profile')->with($data);
    }

    public function change_user_status(Request $request)
    {
    	$user = Vendor::find($request->user_id);
    	$user->status = $request->status;
    	$user->save();

    	return response()->json(['success'=>'User status change successfully.']);
    }

    public function delete_user(Request $request) {
        $user_id = $request->user_id;

        $frame_info = DB::table('vendor')->where('vendor_id','=',$user_id)->first();

        $res = DB::table('vendor')->where('vendor_id', '=', $user_id)->delete();

        if ($res) {
            return json_encode(array('status' => 'success','msg' => 'User has been deleted successfully!'));
        } else {
            return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
        }

    }


    public function change_user_password(Request $request){

        $user_id = base64_decode($request->id);
        $data['user_info'] = Vendor::where('vendor_id','=',$user_id)->first();
        return view('admin/vendor/change_user_password')->with($data);

    }

    public function update_user_password(Request $request) {
        
        $user_id = $request->user_id;
        $user_info = Vendor::find($user_id);

        /*if (!(Hash::check($request->get('current-password'), $user_info->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }*/

        $validatedData = $request->validate([
            //'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user_info->password = bcrypt($request->get('new-password'));
        $user_info->save();

        return redirect()->back()->with("message","Password changed successfully !");
    }



    public function video_list(Request $request) {
    	$user_id = base64_decode($request->id);

    	$data['user_info'] = User::find($user_id);

    	/*$data['video_list'] = DB::table('post_media')->where('media_type','=','video')->get();*/

    	$data['video_list'] = DB::table('post_media')
    	->join('posts', 'post_media.post_id', '=', 'posts.post_id')
    	->where('posts.user_id','=',$user_id)
    	->where('post_media.media_type','=','video')
    	->select('post_media.*')
    	->get();

    	return view('admin/video_list')->with($data);
    }

    public function image_list(Request $request) {
    	$user_id = base64_decode($request->id);

    	$data['user_info'] = User::find($user_id);

    	/*$data['image_list'] = DB::table('post_media')->where('media_type','=','image')->get();*/

    	$data['image_list'] = DB::table('post_media')
    	->join('posts', 'post_media.post_id', '=', 'posts.post_id')
    	->where('posts.user_id','=',$user_id)
    	->where('post_media.media_type','=','image')
    	->select('post_media.*')
    	->get();

    	return view('admin/image_list')->with($data);
    }	

    public function delete_image(Request $request) {
    	$image_id = $request->image_id;

    	$image_info = DB::table('post_media')->where('id','=',$image_id)->first();
    	$media_url = $image_info->media_url;

    	$res = DB::table('post_media')->where('id', '=', $image_id)->delete();

    	if ($res) {
    		@unlink(url('/').'/public/uploads/feed_images/'.$media_url);
    		return json_encode(array('status' => 'succ','msg' => 'Image has been deleted successfully!'));
    	} else {
    		return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
    	}

    }

    public function delete_video(Request $request) {
    	$video_id = $request->video_id;

    	$image_info = DB::table('post_media')->where('id','=',$video_id)->first();
    	$media_url = $image_info->media_url;

    	$res = DB::table('post_media')->where('id', '=', $video_id)->delete();

    	if ($res) {
    		@unlink(url('/').'/public/uploads/feed_videos/'.$media_url);
    		return json_encode(array('status' => 'succ','msg' => 'Video has been deleted successfully!'));
    	} else {
    		return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
    	}

    }

    public function vendorlogin() {

        return view('vendor/login');

    }

    public function submit_login(Request $request) {

        $request->email;

        $request->password;



        $validator = Validator::make($request->all(), [

            'email' => 'required',

            'password' => 'required|min:6'

        ]);

        if ($validator->fails()) {

            return redirect('/login')

            ->withErrors($validator)

            ->withInput();

        } else {



            $inputVal = $request->all(); 



            $credentials = array(

                'email' => $inputVal['email'],

                'password' => $inputVal['password'],

                'status' => 1

            ); 

            

            
            if (Auth::guard('vendor')->attempt(['email' => $request->email, 'password' => $request->password])) {
                        $details = Auth::guard('vendor')->user();
                        $user = $details['original'];
                        return redirect('/vendor/dashboard');
                        //echo "ghhhhgh";
                       // return $user;
            }else{ 

                $user = DB::table('users')->where('email',$inputVal['email'])->first();

                if(!empty($user)){

                    //if ($user->user_status != 1) {
                    if ($user->status != 1) {

                        Auth::logout();

                        session::flash('error', 'Your acount is inactive.');

                        return redirect('/admin_login');

                    } else {  

                        Auth::logout();

                        session::flash('error', 'Email or Password is incorrect.' );

                        return redirect('/admin_login');

                        

                    } 

                }else{  

                    session::flash('error', 'Your account does not exist.' );

                    return redirect('/admin_login');

                }  

            

        }

    }

}

public function dashboard() {

        $id = Auth::guard('vendor')->user()->vendor_id;

        if(!empty($id)){
                  $data['orders'] = DB::table('orders')->where('vendor_id', $id)->get();

                    $data['users'] = DB::table('users')->where('status',1)->get();

                    $data['visitors'] = DB::table('users')->where('status',1)->get();

                    return view('vendor/dashboard')->with($data);
        }else{
                return redirect('/vendorlogin');
        }
    }

    public function logout(){

        Auth::logout();

        return redirect('/vendorlogin');

    }

    public function order_list() {

         $id = Auth::guard('vendor')->user()->vendor_id;

         //print_r($id);

        $data['order_list'] =  DB::table('orders')

        ->select('frames.title','frames.images','frames.currency','users.first_name', 'users.last_name', 'orders.id', 'orders.total_tile', 'orders.total_price', 

            'orders.shipping_price', 'orders.coupon_discount', 'orders.wallet_discount', 'orders.address_id','orders.frame_id', 'orders.order_status', 'orders.delivery_date', 'orders.status', 'orders.vendor_id', 'orders.created_at')

        ->join('users','users.id','=','orders.user_id')

        ->join('frames','frames.id','=','orders.frame_id')

        ->where('orders.vendor_id',$id)

        ->where('orders.status', 1)

        ->where('users.status', 1)

        ->orderBy('orders.id', 'DESC')

        ->get();

        return view('vendor/order_list')->with($data);

    }



    public function view_order_details(Request $request){

        $order_id = base64_decode($request->id);

        $data['order_info'] =  DB::table('orders')

        ->select('frames.images', 'frames.type' ,'frames.currency','users.first_name', 'users.last_name', 'orders.id', 'orders.total_price', 'orders.shipping_price', 'shipping_price.currency as shipping_currency', 'orders.address_id', 'users.email', 'users.contact_number as contact', 'users.address',

            'orders.order_status', 'orders.delivery_date', 'orders.created_at', 'shipping_address.fullname','shipping_address.street_address','shipping_address.city', 'orders.vendor_id',

            'shipping_address.state','countries.name as country','shipping_address.zip_code','shipping_address.contact_number')

        ->join('users','users.id','=','orders.user_id')

        ->join('frames','frames.id','=','orders.frame_id')

        ->join('shipping_address','shipping_address.id','=','orders.address_id')

        ->join('countries','countries.id','=','shipping_address.country')

        ->join('shipping_price','shipping_price.country_id','=','shipping_address.country')

        ->where('orders.id', $order_id )

        ->where('users.status', 1)

        ->first();



        $data['order_details'] =  DB::table('order_detail')

        ->select('frames.images', 'frames.price','frames.currency', 'order_detail.order_id', 'order_detail.tile_image', 'order_detail.rotation', 'order_detail.quantity',  'orders.address_id', 

          'orders.frame_id', 'orders.shipping_price', 'shipping_price.currency as shipping_currency', 'orders.order_status', 

            'orders.total_price', 'orders.coupon_discount', 'orders.wallet_discount' , 'orders.delivery_date')

        ->join('orders','orders.id','=','order_detail.order_id')

        ->join('frames','frames.id','=','orders.frame_id')

        ->join('shipping_address','shipping_address.id','=','orders.address_id')

        ->join('shipping_price','shipping_price.country_id','=','shipping_address.country')

        ->where('order_detail.order_id',$order_id)

        ->get();


        $data['vendor'] =  DB::table('vendor')

        ->where('status',1)

        ->get();



        return view('vendor/view_order_details')->with($data) ;

    }


    public function change_order_status(Request $request)

    {

        $order_info = CheckoutModel::find($request->order_id);

        $date = $order_info->created_at;

        $order_info->order_status = $request->status;

        $res = $order_info->save();



        if ($res) {



            $users = DB::table('users')->where('id','=',$order_info->user_id)->first(); 



            $username = $users->first_name." ".$users->last_name;



            $title ="Hey ". $username ." your order status is ". $request->status;



            //$device_token ="ftkpkxjW37Q:APA91bE_27LnEi3ziGsQpVbZnvh6MlNxovsEVVQSGPDMlnTYt47IiQP25JuSwC0AqiGeGwHa3LAFZ9mw8IKmRkMD1lzKRSeMEBPNCpvsIkKA4AcNAieLbQ-OFlXq74jiCUo2xfPy2fi8";

            

            $this->notification($users->device_token, $title, $request->order_id, $date);

          

                      

            if ($_SERVER['SERVER_NAME'] != 'localhost') {

                $data['url'] = url('/');

                $data['status'] = $request->status;

                $data['first_name'] = $users->first_name;

                $data['last_name'] = $users->last_name;

                $data['status'] = $request->status;

                $data['order_id'] = $request->order_id;





                $fromEmail = Helper::getFromEmail();

                $inData['from_email'] = $fromEmail;

                $inData['email'] = $users->email;

                Mail::send('emails.invoice.order_status_template',$data, function ($message) use ($inData) {

                    $message->from($inData['from_email'],'FRAMEiT');

                    $message->to($inData['email']);

                    $message->subject('FRAMEiT - Order Status Mail');

                });



            }



            return response()->json(['success'=>'Order status change successfully.']);



        }   

    }

     public function notification($token, $title, $order_id, $date)

    {

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $token=$token;



        $notification = [

            'id' => $order_id,

            'title' => 'FRAMEiT',

            'body' => $title,

            'tag' =>'order_status',

            'date' =>$date,

            'sound' => true,

        ];

        

        $extraNotificationData = $notification;



        $fcmNotification = [

            //'registration_ids' => $tokenList, //multple token array

            'to'        => $token, //single token

            'notification' => $notification,

            'data' => $extraNotificationData

        ];



        $headers = [

            'Authorization: key=AAAASk5-u2g:APA91bHQE__pE-TXQszZb-TorN-8v9dbStYqnp_vIfVWitAXGrnfuVr4YZInoK53FZPEPIj6ej3izSvMwNkbWEb54dcrflqMMwAyjtnjpU1TwS4CyxP_9iHQRAYDlC25yI32Vhs6-4sZ',

            'Content-Type: application/json'

        ];





        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$fcmUrl);

        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));

        $result = curl_exec($ch);

        curl_close($ch);



        return true;

    }

}
