<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedsModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'feeds';
    protected $fillable = [
        'title', 'description', 'picture',
    ];
    //public $timestamps = false;
}
