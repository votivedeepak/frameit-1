<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferralModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'referral_commission_settings';
    protected $fillable = [
        'no_of_orders', 'order_amount', 'referral_amount'
    ];
    //public $timestamps = false;
}
