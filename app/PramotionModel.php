<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class PramotionModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'pramotion_videos';
    protected $fillable = [
        'title','sub_title','description', 'video',
    ];
    //public $timestamps = false;
}
