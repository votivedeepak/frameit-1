<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'banner';
    protected $primaryKey = 'banner_id';
    protected $fillable = [
        'banner_description',
    ];
    //public $timestamps = false;
}
