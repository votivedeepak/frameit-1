<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Braintree_Configuration;
class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    
    // public function boot()
    // {

    //     \Braintree_Configuration::environment(config('services.braintree.environment'));
    //     \Braintree_Configuration::merchantId(config('services.braintree.merchant_id'));
    //     \Braintree_Configuration::publicKey(config('services.braintree.public_key'));
    //     \Braintree_Configuration::privateKey(config('services.braintree.private_key'));

    // }

    public function boot()
    {
        \Braintree_Configuration::environment(env('BRAINTREE_ENV'));
        \Braintree_Configuration::merchantId(env('BRAINTREE_MERCHANT_ID'));
        \Braintree_Configuration::publicKey(env('BRAINTREE_PUBLIC_KEY'));
        \Braintree_Configuration::privateKey(env('BRAINTREE_PRIVATE_KEY'));
    }

   /* public function boot()
    {
        
         //video duration validation  'video:25' 
        Validator::extend('video_length', function($attribute, $value, $parameters, $validator) {
            // validate the file extension
            if(!empty($value->getClientOriginalExtension()) && ($value->getClientOriginalExtension() == 'mp4' || $value->getClientOriginalExtension() == '3gp' || $value->getClientOriginalExtension() == 'avi')){

                $ffprobe = FFMpeg\FFProbe::create(
                    array('ffmpeg.binaries'  => 'C:/Program Files/ffmpeg/bin/ffmpeg.exe', 
                        'ffprobe.binaries' => 'C:/Program Files/ffmpeg/bin/ffprobe.exe'
                    )
                );
                $duration = $ffprobe
                    ->format($value->getRealPath()) // extracts file information
                    ->get('duration');
                return(round($duration) > $parameters[0]) ?false:true;
            }else{
                return false;
            }
        });
    }*/


}
